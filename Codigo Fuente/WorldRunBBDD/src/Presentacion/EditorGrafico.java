package Presentacion;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JToolBar;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.ButtonGroup;

import Presentacion.Index;

import java.awt.SystemColor;

import javax.swing.UIManager;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;

import java.awt.Component;

import javax.swing.SwingConstants;

public class EditorGrafico {

	private JFrame frame;
	private JToolBar tbBarraDibujo;
	private JButton btnComida;
	private JButton btnBebida;
	private JButton btnRectangulo;
	private JButton btnAnotacion;
	private JScrollPane scrollPane;
	
	//Area de dibujo personalizada (creada extendiendo de JLabel)
	private MiAreaDibujo miAreaDibujo;
	//Imagen en la que se cargará el fichero seleccionado por el usuario
	private ImageIcon imagen;

	//Variable que almacena el modo de dibujado seleccionado por el usuario
	int modo = -1;
	
	private final int COMIDA = 1;
	private final int BEBIDA = 2;
	private final int SALIDA = 3;
	private final int META = 4;
	private final int SECCION = 5;
	private final int RECTANGULO = 6;
	private final int TEXTO = 7;
	

	private Toolkit toolkit;

	private Image imagComida;
	private Image imagBebida;
	private Image imagSalida;
	private Image imagMeta;
	private Image imagSeccion;
	private Image imagRectangulo;
	private Image imagTexto;

	private Cursor cursorComida;
	private Cursor cursorBebida;
	private Cursor cursorSalida;
	private Cursor cursorMeta;
	private Cursor cursorSeccion;
	private Cursor cursorRectangulo;
	private Cursor cursorTexto;

	private int x, y;

	//Componente de entrada de texto que permitirá añadir anotaciones (dibujar texto) sobre la imagen
	private JTextField txtTexto = new JTextField();
	private JButton btnMeta;
	private JButton btnSalida;
	private JButton btnSeccion;
	private JButton btnBorrar;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JLabel lblSeparador;
	private JButton btnAyuda;
	private JLabel lblSeparador2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					EditorGrafico window = new EditorGrafico();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EditorGrafico() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(EditorGrafico.class.getResource("/Recursos/FavIcon.png")));
		frame.setBounds(100, 100, 700, 700);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		{
			tbBarraDibujo = new JToolBar();
			tbBarraDibujo.setBorder(UIManager.getBorder("ToolBar.border"));
			frame.getContentPane().add(tbBarraDibujo, BorderLayout.NORTH);
			{
				btnComida = new JButton("");
				btnComida.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
				btnComida.addActionListener(new BtnComidaActionListener());
				buttonGroup.add(btnComida);
				btnComida.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/comida.png")));
				tbBarraDibujo.add(btnComida);
			}
			{
				btnBebida = new JButton("");
				btnBebida.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
				btnBebida.addActionListener(new BtnBebidaActionListener());
				buttonGroup.add(btnBebida);
				btnBebida.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/bebida.png")));
				tbBarraDibujo.add(btnBebida);
			}
			{
				btnSalida = new JButton("");
				btnSalida.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
				btnSalida.addActionListener(new BtnSalidaActionListener());
				buttonGroup.add(btnSalida);
				btnSalida.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/salida.png")));
				tbBarraDibujo.add(btnSalida);
			}
			{
				{
					btnMeta = new JButton("");
					btnMeta.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
					btnMeta.addActionListener(new BtnMetaActionListener());
					{
						btnSeccion = new JButton("");
						btnSeccion.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
						btnSeccion.addActionListener(new BtnSeccionActionListener());
						buttonGroup.add(btnSeccion);
						btnSeccion.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/seccion.png")));
						tbBarraDibujo.add(btnSeccion);
					}
					buttonGroup.add(btnMeta);
					btnMeta.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/meta.png")));
					tbBarraDibujo.add(btnMeta);
				}
			}
			{
				btnAnotacion = new JButton("");
				btnAnotacion.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
				btnAnotacion.addActionListener(new BtnAnotacionActionListener());
				buttonGroup.add(btnAnotacion);
				btnRectangulo = new JButton("");
				btnRectangulo.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
				btnRectangulo.addActionListener(new BtnRectanguloActionListener());
				{
					lblSeparador = new JLabel("");
					lblSeparador.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/separador.png")));
					tbBarraDibujo.add(lblSeparador);
				}
				buttonGroup.add(btnRectangulo);
				btnRectangulo.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/rectangulo.png")));
				tbBarraDibujo.add(btnRectangulo);
				btnAnotacion.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/anotacion.png")));
				tbBarraDibujo.add(btnAnotacion);
			}
			{
				btnBorrar = new JButton("");
				btnBorrar.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
				btnBorrar.addActionListener(new BtnBorrarActionListener());
				buttonGroup.add(btnBorrar);
				btnBorrar.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/borrar.png")));
				tbBarraDibujo.add(btnBorrar);
			}
			{
				lblSeparador2 = new JLabel("");
				lblSeparador2.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/separador2.png")));
				tbBarraDibujo.add(lblSeparador2);
			}
			{
				btnAyuda = new JButton("");
				btnAyuda.addActionListener(new BtnAyudaActionListener());
				btnAyuda.addMouseListener(new MouseAdapter() {
					
				});
				btnAyuda.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
				btnAyuda.setIcon(new ImageIcon(EditorGrafico.class.getResource("/Recursos/ayuda.png")));
				tbBarraDibujo.add(btnAyuda);
			}
		}
		{
			scrollPane = new JScrollPane();
			frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
			//Creación del área de dibujo personalizada
			miAreaDibujo = new MiAreaDibujo();
			miAreaDibujo.setHorizontalTextPosition(SwingConstants.CENTER);
			miAreaDibujo.setHorizontalAlignment(SwingConstants.CENTER);
			miAreaDibujo.setAlignmentX(Component.CENTER_ALIGNMENT);
			miAreaDibujo.setBackground(SystemColor.textHighlightText);
			miAreaDibujo.addMouseListener(new MiAreaDibujoMouseListener());
			miAreaDibujo.addMouseMotionListener(new MiAreaDibujoMouseMotionListener());//T 19
			miAreaDibujo.addMouseListener(new MiAreaDibujoMouseListener());//T 19
			miAreaDibujo.setIcon(null);
			scrollPane.setViewportView(miAreaDibujo);

			//Creación de imágenes y cursores
			toolkit = Toolkit.getDefaultToolkit();
			
			imagComida = toolkit.getImage(getClass().getClassLoader().getResource("Recursos/comida.png"));
			imagBebida = toolkit.getImage(getClass().getClassLoader().getResource("Recursos/bebida.png"));
			imagSalida = toolkit.getImage(getClass().getClassLoader().getResource("Recursos/salida.png"));
			imagMeta = toolkit.getImage(getClass().getClassLoader().getResource("Recursos/meta.png"));
			imagSeccion = toolkit.getImage(getClass().getClassLoader().getResource("Recursos/seccion.png"));
			imagRectangulo = toolkit.getImage(getClass().getClassLoader().getResource("Recursos/rectangulo.png"));
			imagTexto =  toolkit.getImage(getClass().getClassLoader().getResource("Recursos/anotacion.png"));
			
			//Creación de los cursores
			
			cursorComida= toolkit.createCustomCursor(imagComida,new Point(0,0),"CURSOR_COMIDA");
			cursorBebida = toolkit.createCustomCursor(imagBebida,new Point(0,0),"CURSOR_BEBIDA");
			cursorSalida= toolkit.createCustomCursor(imagSalida,new Point(0,0),"CURSOR_SALIDA");
			cursorMeta = toolkit.createCustomCursor(imagMeta,new Point(0,0),"CURSOR_META");
			cursorSeccion= toolkit.createCustomCursor(imagSeccion,new Point(0,0),"CURSOR_SECCION");
			cursorRectangulo = toolkit.createCustomCursor(imagRectangulo,new Point(0,0),"CURSOR_RECTANGULO");
			cursorTexto= toolkit.createCustomCursor(imagTexto,new Point(0,0),"CURSOR_TEXTO");
			
			//Subida de fotografia
			imagen = new ImageIcon(EditorGrafico.class.getResource("/Recursos/mapa.png"));
			miAreaDibujo.setIcon(imagen);
			miAreaDibujo.eliminarObjetoGrafico();
		}
	}
	
	private class BtnComidaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = COMIDA;
			frame.setCursor(cursorComida);
		}
	}
	
	private class BtnBebidaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = BEBIDA;
			frame.setCursor(cursorBebida);
		}
	}
	
	private class BtnSalidaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = SALIDA;
			frame.setCursor(cursorSalida);
		}
	}
	
	private class BtnMetaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = META;
			frame.setCursor(cursorMeta);
		}
	}
	
	private class BtnSeccionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = SECCION;
			frame.setCursor(cursorSeccion);
		}
	}
	
	private class BtnRectanguloActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = RECTANGULO;
			frame.setCursor(cursorRectangulo);
		}
	}
	
	private class BtnAnotacionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = TEXTO;
			frame.setCursor(cursorTexto);
		}
	}
	
	private class BtnBorrarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			miAreaDibujo.borrar();
		}
	}
	
	
	
	private class MiAreaDibujoMouseMotionListener extends MouseMotionAdapter {

		public void mouseDragged(MouseEvent e) {       
			if (modo == RECTANGULO && imagen!=null) {
			    ((RectanguloGrafico)miAreaDibujo.getUltimoObjetoGrafico()).setX1(e.getX());
			    ((RectanguloGrafico)miAreaDibujo.getUltimoObjetoGrafico()).setY1(e.getY());
			    miAreaDibujo.repaint();
			}
		}
	}
	private class MiAreaDibujoMouseListener extends MouseAdapter {
		public void mousePressed(MouseEvent arg0) {
				x = arg0.getX();
				y = arg0.getY();
				//toolkit = Toolkit.getDefaultToolkit();
				if (imagen != null){
					switch (modo){
					case COMIDA:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagComida));
						miAreaDibujo.repaint();
						break;
					case BEBIDA:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagBebida));
						miAreaDibujo.repaint();
						break;
					case SALIDA:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagSalida));
						miAreaDibujo.repaint();
						break;
					case META:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagMeta));
						miAreaDibujo.repaint();
						break;
					case SECCION:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagSeccion));
						miAreaDibujo.repaint();
						break;
					case RECTANGULO:
						miAreaDibujo.addObjetoGrafico(new RectanguloGrafico(x,y,x,y, Color.RED));
						break;
					case TEXTO:
						txtTexto.setBounds(x, y, 200,30);
						txtTexto.setVisible(true);
						txtTexto.requestFocus();
						txtTexto.addActionListener(new ActionListener() {
		                			public void actionPerformed(ActionEvent arg) {
		                				if(!txtTexto.getText().equals("")) 
		                				miAreaDibujo.addObjetoGrafico(new TextoGrafico(x, y+15, txtTexto.getText(), Color.BLUE));
		                				txtTexto.setText("");
		                				txtTexto.setVisible(false);
		                				miAreaDibujo.repaint();
		                			}
		        			});
						miAreaDibujo.add(txtTexto);
					}
				}	
		}
	}
	private class BtnAyudaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			LeyendaEditorGrafico leyendaGraf=new LeyendaEditorGrafico();
			leyendaGraf.getFrame().setVisible(true);
			leyendaGraf.getFrame().setLocationRelativeTo(frame);
		}
	}
	
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}		
}