package Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.Toolkit;

import javax.swing.JLabel;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;

public class LeyendaEditorGrafico {

	private JFrame frmLeyendaEditor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LeyendaEditorGrafico window = new LeyendaEditorGrafico();
					window.frmLeyendaEditor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LeyendaEditorGrafico() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLeyendaEditor = new JFrame();
		frmLeyendaEditor.setIconImage(Toolkit.getDefaultToolkit().getImage(LeyendaEditorGrafico.class.getResource("/Recursos/FavIcon.png"))); //$NON-NLS-1$
		frmLeyendaEditor.setBounds(100, 100, 254, 450);
		frmLeyendaEditor.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JLabel lblLeyenda = new JLabel(MessagesLeyendaEditorGrafico.getString("LeyendaEditorGrafico.lblLeyenda.text")); //$NON-NLS-1$ //$NON-NLS-1$
		lblLeyenda.setIcon(new ImageIcon(LeyendaEditorGrafico.class.getResource(MessagesLeyendaEditorGrafico.getString("LeyendaEditorGrafico.1")))); //$NON-NLS-1$
		frmLeyendaEditor.getContentPane().add(lblLeyenda, BorderLayout.CENTER);
	}
	
	public JFrame getFrame() {
		return frmLeyendaEditor;
	}

	public void setFrame(JFrame frame) {
		this.frmLeyendaEditor = frame;
	}

}
