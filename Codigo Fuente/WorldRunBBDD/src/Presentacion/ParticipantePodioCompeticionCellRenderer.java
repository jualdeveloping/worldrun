package Presentacion;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

class ParticipantePodioCompeticionCellRenderer extends DefaultListCellRenderer {
	
	protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
	
	ImageIcon iconoPrimero;
	ImageIcon iconoSegundo;
	ImageIcon iconoTercero;
	ImageIcon iconoResto;
	ImageIcon iconoParticipante;
	
	public ParticipantePodioCompeticionCellRenderer (){
		iconoPrimero = new ImageIcon(ParticipantePodioCompeticionCellRenderer.class.getResource("/Recursos/oro.png"));
		iconoSegundo = new ImageIcon(ParticipantePodioCompeticionCellRenderer.class.getResource("/Recursos/plata.png"));
		iconoTercero = new ImageIcon(ParticipantePodioCompeticionCellRenderer.class.getResource("/Recursos/bronce.png"));
		iconoResto = new ImageIcon(ParticipantePodioCompeticionCellRenderer.class.getResource("/Recursos/fuerapodio.png"));
		iconoParticipante = new ImageIcon(ParticipantePodioCompeticionCellRenderer.class.getResource("/Recursos/participante.png"));
	}
	
	public Component getListCellRendererComponent(JList list, Object value, int index,boolean isSelected, boolean hasFocus) {
		
		JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, hasFocus);
		String posicion = list.getModel().getElementAt(index).toString();
		
		String[] pos = posicion.split(" ");
		

		String aux = pos[0];
		
		
		if (aux.compareTo("1") == 0) {
		renderer.setIcon(iconoPrimero);
		}
		if (aux.compareTo("2") == 0) {
			renderer.setIcon(iconoSegundo);
		}
		if (aux.compareTo("3") == 0) {
			renderer.setIcon(iconoTercero);
		}
		
		
		else if (aux.compareTo("1") != 0 && aux.compareTo("2") != 0 && aux.compareTo("3") != 0) {
			renderer.setIcon(iconoParticipante);
		}
		
		return renderer;
	}
}
