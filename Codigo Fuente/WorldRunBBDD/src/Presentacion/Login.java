package Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.GridBagLayout;
import java.awt.FlowLayout;
import java.awt.Window;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.List;
import java.awt.Choice;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;

import Dominio.Admin;
import Dominio.Competicion;
import Dominio.Participante;
import Presentacion.Index;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.SwingConstants;

import java.awt.SystemColor;

public class Login {

	private JFrame frmLogin;
	private JPanel panel;
	private JLabel lblLogo;
	private JLabel lblUsuario;
	private JLabel lblContrasena;
	private JTextField txtUsuario;
	private JCheckBox chckbxRecordarContrasena;
	private JButton btnEntrar;
	private JButton btnCancelar;
	private JPasswordField pwdfContrasena;
	private JPanel panelAcceso;
	private JButton btnAyuda;
	public static int numeroArray;
	public static ArrayList<String> admin=new ArrayList<String>();

	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					Login window = new Login();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	Connection connection=null;
	private JButton btnIdiomaLogin;

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public Login() throws Exception {
		initialize();
		connection=Persistencia.AgenteDB.conectar();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		java.util.Date fechaUltimoAcceso = new java.util.Date();
		
		
		frmLogin = new JFrame();
		frmLogin.setIconImage(Toolkit.getDefaultToolkit().getImage(Login.class.getResource("/Recursos/FavIcon.png"))); //$NON-NLS-1$
		frmLogin.setBounds(100, 100, 482, 375);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.setResizable(false);
		
		{
			panel = new JPanel();
			panel.setBackground(SystemColor.control);
			frmLogin.getContentPane().add(panel, BorderLayout.CENTER);
			panel.setLayout(null);
			{
				lblLogo = new JLabel(""); //$NON-NLS-1$
				lblLogo.setIcon(new ImageIcon(Login.class.getResource("/Recursos/logoLogin.png"))); //$NON-NLS-1$
				lblLogo.setBounds(17, 11, 313, 97);
				panel.add(lblLogo);
			}
			{
				panelAcceso = new JPanel();
				panelAcceso.setBackground(SystemColor.control);
				panelAcceso.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
				panelAcceso.setBounds(102, 134, 282, 154);
				panel.add(panelAcceso);
				panelAcceso.setLayout(null);
				{
					lblContrasena = new JLabel(MessagesLogin.getString("Login.lblContrasena.text")); //$NON-NLS-1$
					lblContrasena.setFont(new Font("Tahoma", Font.PLAIN, 13)); //$NON-NLS-1$
					lblContrasena.setBounds(10, 42, 77, 21);
					panelAcceso.add(lblContrasena);
				}
				{
					txtUsuario = new JTextField();
					txtUsuario.addActionListener(new TxtUsuarioActionListener());
					txtUsuario.setBounds(86, 11, 184, 20);
					panelAcceso.add(txtUsuario);
					txtUsuario.setColumns(10);
				}
				{
					chckbxRecordarContrasena = new JCheckBox(MessagesLogin.getString("Login.chckbxRecordarContrasena.text")); //$NON-NLS-1$
					chckbxRecordarContrasena.setBackground(SystemColor.control);
					chckbxRecordarContrasena.setBounds(86, 70, 184, 23);
					panelAcceso.add(chckbxRecordarContrasena);
				}
				{
					pwdfContrasena = new JPasswordField();
					pwdfContrasena.setEnabled(false);
					pwdfContrasena.addActionListener(new MiEntrarActionListener());
					pwdfContrasena.setBounds(86, 43, 184, 20);
					panelAcceso.add(pwdfContrasena);
				}
				{
					btnCancelar = new JButton(MessagesLogin.getString("Login.btnCancelar.text")); //$NON-NLS-1$
					btnCancelar.addActionListener(new BtnCancelarActionListener());
					btnCancelar.setEnabled(false);
					btnCancelar.setBounds(179, 104, 91, 23);
					panelAcceso.add(btnCancelar);
				}
				{
					btnEntrar = new JButton(MessagesLogin.getString("Login.btnEntrar.text")); //$NON-NLS-1$
					btnEntrar.setEnabled(false);
					btnEntrar.addActionListener(new MiEntrarActionListener());
					btnEntrar.setBounds(86, 104, 83, 23);
					panelAcceso.add(btnEntrar);
				}
				{
					lblUsuario = new JLabel(MessagesLogin.getString("Login.lblUsuario.text")); //$NON-NLS-1$
					lblUsuario.setBounds(33, 12, 43, 19);
					panelAcceso.add(lblUsuario);
					lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 13)); //$NON-NLS-1$
				}
			}
			{
				btnAyuda = new JButton(""); //$NON-NLS-1$
				btnAyuda.addActionListener(new BtnAyudaActionListener());
				btnAyuda.setIcon(new ImageIcon(Login.class.getResource("/Recursos/ayuda.png"))); //$NON-NLS-1$
				btnAyuda.setBounds(406, 11, 60, 37);
				panel.add(btnAyuda);
			}
			{
				btnIdiomaLogin = new JButton(MessagesLogin.getString("")); //$NON-NLS-1$
				btnIdiomaLogin.setIcon(new ImageIcon(Login.class.getResource("/Recursos/idioma.png"))); //$NON-NLS-1$
				btnIdiomaLogin.addActionListener(new BtnNewButtonActionListener());
				btnIdiomaLogin.setBounds(325, 11, 71, 37);
				panel.add(btnIdiomaLogin);
			}
			
			JLabel lblFondo = new JLabel(""); //$NON-NLS-1$
			lblFondo.setBackground(Color.RED);
			lblFondo.setIcon(new ImageIcon(Login.class.getResource("/Recursos/fondologin.png"))); //$NON-NLS-1$
			lblFondo.setBounds(0, 0, 476, 347);
			panel.add(lblFondo);
		}
	}
	public JFrame getFrameLogin() {
		return frmLogin;
	}
	
	public JFrame getFrame() {
		return frmLogin;
	}

	public void setFrame(JFrame frame) {
		this.frmLogin = frame;
	}
	
	private class TxtUsuarioActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			//Activamos los de la contraseña
			lblContrasena.setEnabled(true);
			pwdfContrasena.setEnabled(true);
			//Pasamos el foco (el cursor) al campo de la contraseña
			pwdfContrasena.requestFocus();
			//Desactivamos los de usuario
			lblUsuario.setEnabled(false);
			//txtUsuario.setEnabled(false);
			//Activamos el boton de entrar y cancelar
			btnEntrar.setEnabled(true);
			btnCancelar.setEnabled(true);
		}
	}
	
	private class BtnAyudaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			if (Desktop.isDesktopSupported()) {
			    try {
			        File myFile = new File("Manual.pdf");  //$NON-NLS-1$
			        Desktop.getDesktop().open(myFile);
			    } catch (IOException ex) {
			    	JOptionPane.showMessageDialog(frmLogin,MessagesLogin.getString("Login.8"), "Error", JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			    }
			}
		}
	}
	
	
	private class BtnCancelarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			txtUsuario.setEnabled(true);
			txtUsuario.setText(null);
			pwdfContrasena.setEnabled(false);
			pwdfContrasena.setText(null);
		}
	}
	private class MiEntrarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			try{
				String query = "select * from Admin where usuario =? and password=? "; //$NON-NLS-1$
				PreparedStatement pst = connection.prepareStatement(query);
				pst.setString(1, txtUsuario.getText());
				pst.setString(2, pwdfContrasena.getText());
				ResultSet rs=pst.executeQuery();
				int count =0;
				while(rs.next()){
					count=count+1;
				}
				if(count == 1){
					JOptionPane.showMessageDialog(null, MessagesLogin.getString("Login.11")); //$NON-NLS-1$
					admin.add(txtUsuario.getText());
					admin.add(pwdfContrasena.getText());
					new Index().getFrame().setVisible(true);
					frmLogin.setVisible(false);
				}
				else if(count >1){
					JOptionPane.showMessageDialog(null, MessagesLogin.getString("Login.12")); //$NON-NLS-1$
				}
				else{
					JOptionPane.showMessageDialog(null, MessagesLogin.getString("Login.13")); //$NON-NLS-1$
				}
				rs.close();
				pst.close();
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, e);
				System.out.println(e);
				e.printStackTrace();
			}
		
		}
	}
	private class BtnNewButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			SeleccionIdiomaLogin select = new SeleccionIdiomaLogin();
			select.main(null);
		    frmLogin.dispose();
		}
	}
}
