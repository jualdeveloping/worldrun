package Presentacion;

import java.awt.EventQueue;

import javax.swing.text.MaskFormatter;

import java.text.ParseException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.JMenu;
import javax.swing.SwingConstants;
import javax.swing.JMenuBar;
import javax.swing.SwingUtilities;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.UIManager;

import java.awt.Color;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

import java.awt.Dimension;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;

import javax.swing.JList;
import javax.swing.border.LineBorder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.swing.JMenuItem;

import java.awt.Font;

import javax.swing.JRadioButtonMenuItem;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.SystemColor;

import javax.swing.JTextArea;

import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;

















import Dominio.Competicion;
import Dominio.Participante;
import Dominio.Persona;

import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JPasswordField;

public class Index {

	private JFrame frmIndex;
	private JMenuBar menuBar;
	private JPanel panelMenu;
	private JPanel panelIndex;
	private JButton btnCerrarSesion;
	private JLabel lblConectado;
	private JLabel lblLogotipo;
	private JMenu mnArchivo;
	private JMenu mnEditar;
	private JMenu mnVer;
	private JMenu mnAyuda;
	private JButton btnAtras;
	private JButton btnHome;
	private JPanel panelAtras;
	private JPanel panelHome;
	private JLabel lblCompeticionesIndex;
	private JPanel panelBuscarCompeticion;
	private JTextField txtBuscarCompeticion;
	private JButton btnCrearCompeticion;
	private JButton btnModificarCompeticion;
	private JButton btnEliminarCompeticion;
	private JButton btnVerCompeticion;
	private JPanel panelBuscarParticipante;
	private JTextField txtBuscarParticipante;
	private JButton btnCrearParticipante;
	private JButton btnModificarParticipante;
	private JButton btnEliminarParticipante;
	private JButton btnVerParticipante;
	private JPanel panelCompeticion;
	private JScrollPane spLogoEntidadCompeticion;
	private JLabel lblNombreCompeticion;
	private JTextField txtNombreCompeticion;
	private JLabel lblLimiteInscritosCompeticion;
	private JTextField txtLimiteInscritosCompeticion;
	private JLabel lblLocalidadCompeticion;
	private JTextField txtLocalidadCompeticion;
	private JLabel lblFechaLimiteInscritosCompeticion;
	private JComboBox cbFechaLimiteInscripcionDiaCompeticion;
	private JComboBox cbFechaLimiteInscripcionMesCompeticion;
	private JComboBox cbFechaLimiteInscripcionAnoCompeticion;
	private JLabel lblFechaCompeticion;
	private JComboBox cbFechaDiaCompeticion;
	private JComboBox cbFechaMesCompeticion;
	private JComboBox cbFechaAnoCompeticion;
	private JLabel lblPrecioInscripcionCompeticion;
	private JTextField txtPrecioInscripcionCompeticion;
	private JScrollPane spCartelCompeticion;
	private JLabel lblModalidadCompeticion;
	private JComboBox cbModalidadCompeticion;
	private JLabel lblDificultadCompeticion;
	private JComboBox cbDificultadCompeticion;
	private JLabel lblDistanciaCompeticion;
	private JTextField txtDistanciaCompeticion;
	private JLabel lblDescripcionCompeticion;
	private JTextField txtDescripcionCompeticion;
	private JLabel lblTelefonoCompeticion;
	private JFormattedTextField txtTelefonoCompeticion;
	private JLabel lblCorreoElectronicoCompeticion;
	private JTextField txtCorreoElectronicoCompeticion;
	private JLabel lblWebCompeticion;
	private JTextField txtWebCompeticion;
	private JScrollPane spMapaCarreraCompeticion;
	private JScrollPane spLogoEntidadesCooperativasCompeticion;
	private JPanel panelParticipante;
	private JScrollPane spFotoParticipante;
	private JLabel lblNombreParticipante;
	private JTextField txtNombreParticipante;
	private JLabel lblTallaParticipante;
	private JComboBox cbTallaParticipante;
	private JLabel lblApellidosParticipante;
	private JTextField txtApellidosParticipante;
	private JLabel lblTipoParticipante;
	private JComboBox cbTipoParticipante;
	private JLabel lblDNIParticipante;
	private JFormattedTextField txtDNIParticipante;
	private JScrollPane spClubDeportivoParticipante;
	private JLabel lblFechaNacimientoParticipante;
	private JComboBox cbFechaNacimientoDiaParticipante;
	private JComboBox cbFechaNacimientoMesParticipante;
	private JComboBox cbFechaNacimientoAnoParticipante;
	private JLabel lblSexoParticipante;
	private JComboBox cbSexoParticipante;
	private JLabel lblDireccionParticipante;
	private JComboBox cbDireccionParticipante;
	private JTextField txtDireccionParticipante;
	private JLabel lblNumeroParticipante;
	private JTextField txtNumeroParticipante;
	private JLabel lblEscaleraParticipante;
	private JComboBox cbEscaleraParticipante;
	private JLabel lblPisoParticipante;
	private JTextField txtPisoParticipante;
	private JLabel lblPuertaParticipante;
	private JTextField txtPuertaParticipante;
	private JLabel lblCodigoPostalParticipante;
	private JTextField txtCodigoPostalParticipante;
	private JLabel lblLocalidadParticipante;
	private JTextField txtLocalidadParticipante;
	private JLabel lblProvinciaParticipante;
	private JTextField txtProvinciaParticipante;
	private JScrollPane spSponsorParticipante;
	private JLabel lblPaisParticipante;
	private JTextField txtPaisParticipante;
	private JLabel lblTelefonoParticipante;
	private JFormattedTextField txtTelefonoParticipante;
	private JLabel lblCorreoElectronicoParticipante;
	private JTextField txtCorreoElectronicoParticipante;
	private JPanel panelAdmin;
	private JScrollPane spFotoAdmin;
	private JLabel lblNombreAdmin;
	private JTextField txtNombreAdmin;
	private JLabel lblApellidosAdmin;
	private JTextField txtApellidosAdmin;
	private JLabel lblDNIAdmin;
	private JFormattedTextField txtDNIAdmin;
	private JLabel lblFechaNacimientoAdmin;
	private JComboBox cbFechaNacimientoDiaAdmin;
	private JComboBox cbFechaNacimientoMesAdmin;
	private JComboBox cbFechaNacimientoAnoAdmin;
	private JLabel lblSexoAdmin;
	private JComboBox cbSexoAdmin;
	private JLabel lblDireccionAdmin;
	private JComboBox cbDireccionAdmin;
	private JTextField txtDireccionAdmin;
	private JLabel lblNumeroAdmin;
	private JTextField txtNumeroAdmin;
	private JLabel lblEscaleraAdmin;
	private JComboBox cbEscaleraAdmin;
	private JLabel lblPisoAdmin;
	private JTextField txtPisoAdmin;
	private JLabel lblPuertaAdmin;
	private JTextField txtPuertaAdmin;
	private JLabel lblCodigoPostalAdmin;
	private JTextField txtCodigoPostalAdmin;
	private JLabel lblLocalidadAdmin;
	private JTextField txtLocalidadAdmin;
	private JLabel lblProvinciaAdmin;
	private JTextField txtProvinciaAdmin;
	private JLabel lblPaisAdmin;
	private JTextField txtPaisAdmin;
	private JLabel lblTelefonoAdmin;
	private JFormattedTextField txtTelefonoAdmin;
	private JLabel lblCorreoElectronicoAdmin;
	private JFormattedTextField txtCorreoElectronicoAdmin;
	private JLabel lblFechaUltimoAccesoAdmin;
	private JPanel panelContenedor;
	private JLabel lblParticipantesIndex;
	private JButton btnUsuario;
	private JMenuItem mntmCerrarSesion;
	private JMenuItem mntmSalir;
	private JMenu mnTamanoLetra;
	private JRadioButtonMenuItem rdbtnmntmPequena;
	private JRadioButtonMenuItem rdbtnmntmNormal;
	private JRadioButtonMenuItem rdbtnmntmGrande;
	private JMenuItem mntmIdioma;
	private JMenuItem mntmManualUsuario;
	private JMenu mnIrA;
	private JMenuItem mntmIrAInicio;
	private JMenuItem mntmIrAParticipantes;
	private JMenuItem mntmIrACompeticion;
	private JMenuItem mntmIrAPerfil;
	private JPanel panelAnterior;
	private JPanel panelActual = panelIndex;
	private JScrollPane spParticipante;
	private JList listParticipante;
	private JScrollPane spCompeticion;
	private JList listCompeticion;
	private JLabel lblFechaUltimoAccesoMostrarAdmin;
	private JScrollPane spInformacionGeneralCompeticion;
	private JScrollPane spEnlacesInteresCompeticion;
	private JTextArea taInformacionGeneralCompeticion;
	private JTextArea taEnlacesInteresCompeticion;
	private JLabel lblMapaImagenCompeticion;
	private JLabel lblLogoEntidadCompeticion;
	private JLabel lblCartelCompeticion;
	private JLabel lblLogoEntidadesCooperativas;
	private JButton btnGuardarCompeticionCompeticion;
	private JButton btnModificarCompeticionCompeticion;
	private JButton btnEliminarCompeticionCompeticion;
	private JPanel panelDatosParticipante;
	private JButton btnGuardarParticipanteParticipante;
	private JButton btnModificarParticipanteParticipante;
	private JButton btnEliminarParticipanteParticipante;
	private JPanel panelCompeticionParticipante;
	private JScrollPane spCompeticionParticipante;
	private JList listCompeticionParticipante;
	private JPanel panelResumenCompeticionParticipante;
	private JTextArea taResumenCompeticionParticipante;
	private JButton btnBorrarCompeticionParticipante;
	private JButton btnAnadirCompeticionParticipante;
	private JPanel panelDatosCompeticion;
	private JPanel panelInscritosCompeticion;
	private JPanel panelInformacionGeneralCompeticion;
	private JPanel panelLogosCompeticion;
	private JPanel panelEnlacesInteresCompeticion;
	private JPanel panelMapaCompeticion;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JLabel lblTituloBuscarParticipante;
	private JLabel lblTituloBuscarCompeticion;
	private JPanel panelDatosAdmin;
	private JLabel lblFotoAdmin;
	private JMenuItem mntmAcercaDe;
	private JLabel lblCompe;
	private JLabel lblFoto;
	private JButton btnGuardarAdminAdmin;
	private JButton btnEliminarAdminAdmin;
	private JButton btnModificarAdminAdmin;
	private JLabel lblClubDeportivoParticipante;
	private JLabel lblSponsorParticipante;
	
	private JLabel lblFondoBuscarCompeticion;
	private JLabel lblFondoBuscarParticipante;
	private JLabel lblEstadoCompeticion;
	private JComboBox cbEstadoCompeticion;
	private JButton btnVerInformacionCompeticion;
	private JButton btnCrearAdminAdmin;
	private JPanel panelPodioCompeticion;
	private JPanel panelParticipantesPodioCompeticion;
	private JScrollPane spParticipantesPodioCompeticion;
	private JList listParticipantesPodioCompeticion;
	private JLabel lblSeccion1PodioCompeticion;
	private JLabel lblSeccion2PodioCompeticion;
	private JLabel lblSeccion3PodioCompeticion;
	private JLabel lblSeccion4PodioCompeticion;
	private JLabel lblSeccion5PodioCompeticion;
	private JTextField txtSeccion1PodioCompeticion;
	private JTextField txtSeccion2PodioCompeticion;
	private JTextField txtSeccion3PodioCompeticion;
	private JTextField txtSeccion4PodioCompeticion;
	private JTextField txtSeccion5PodioCompeticion;
	private JPanel panelTiemposPodioCompeticion;
	private JLabel lblTiempoTotalPodioCompeticion;
	private JTextField txtTiempoTotalPodioCompeticion;
	private JPanel panel;
	private JLabel lblTiempos;
	private JLabel lblParticipantes;
	private JLabel lblUsuarioAdmin;
	private JLabel lblContraseñaAdmin;
	private JLabel lblRepetirContraseñaAdmin;
	private JTextField txtUsuarioAdmin;
	private JPasswordField txtContraseñaAdmin;
	private JPasswordField txtRepetirContraseñaAdmin;
	private JButton btnAñadirPodioCompeticion;
	private JButton btnBorrarPodioCompeticion;
	private JButton btnEditarImagen;
	private JPanel panelNuevaCompeticionParticipante;
	private JPanel panelCompeticionesNuevaCompeticionParticipante;
	private JScrollPane spCompeticionNuevaCompeticionParticipante;
	private JList listCompeticionNuevaCompeticionParticipante;
	private JLabel lblTiempoTotalCP;
	private JLabel lblSeccion1CP;
	private JLabel lblSeccion2CP;
	private JPanel panelTiemposNuevaCompeticionParticipante;
	private JLabel lblSeccion3CP;
	private JLabel lblSeccion4CP;
	private JLabel lblSeccion5CP;
	private JTextField txtTiempoTotalCP;
	private JTextField txtSeccion1CP;
	private JTextField txtSeccion2CP;
	private JTextField txtSeccion3CP;
	private JTextField txtSeccion4CP;
	private JTextField txtSeccion5CP;
	private JLabel lblPosicionCP;
	private JTextField txtPosicionCP;
	private JLabel label;
	private JLabel lblDatosCP;
	private JButton btnGuardarNuevaCompeticionParticipante;
	private JPanel panelParticipantesNuevaCompeticionParticipante;
	private JScrollPane spParticipanteNuevaCompeticionParticipante;
	private JList listParticipanteNuevaCompeticionParticipante;
	private JLabel lblParticipanteCP;
	private JLabel lblCompeticionCP;
	private JTextField txtParticipanteCP;
	private JTextField txtCompeticionCP;
	private JLabel lblImagenCompeticiones;
	
	JLabel lblPodio;
	JLabel lblClasificacion;
	
	private DefaultListModel modeloListaParticipante;
	private DefaultListModel modeloListaCompeticion;
	private DefaultListModel modeloListaCompeticionParticipante;
	private DefaultListModel modeloListaParticipantePodioCompeticion;
	private DefaultListModel modeloListaCompeticionNuevaCompeticionParticipante;
	private DefaultListModel modeloListaParticipanteNuevaCompeticionParticipante;
	
	private String idCompeticion;
	private String dniParticipante;
	
	private JLabel lblIdCompeticion;
	private JTextField txtIdCompeticion;
	
	//private String Usuario = ;
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//Para que todas las ventanas tengan el aspecto que tienen en Test/Preview
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					Index window = new Index();
					window.frmIndex.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	static Connection connection=null;
	private JLabel lblFondoIndex;
	private JButton btnGaleriaPodioCompeticion;
	private JLabel lblSeleccioneUnParticipante;
	private JLabel lblSeleccioneUnaCompeticin;
	
	
	public Index() throws Exception {
		initialize();
		connection=Persistencia.AgenteDB.conectar();
	}

	private void initialize() {
		frmIndex = new JFrame();
		frmIndex.setIconImage(Toolkit.getDefaultToolkit().getImage(Index.class.getResource("/Recursos/FavIcon.png"))); //$NON-NLS-1$
		frmIndex.setBounds(100, 100, 1243, 706);
		frmIndex.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmIndex.setResizable(false);
		{
			menuBar = new JMenuBar();
			menuBar.setBackground(SystemColor.textHighlightText);
			frmIndex.setJMenuBar(menuBar);
			{
				mnArchivo = new JMenu(MessagesIndex.getString("Index.mnArchivo.text")); //$NON-NLS-1$
				menuBar.add(mnArchivo);
				{
					mntmCerrarSesion = new JMenuItem(MessagesIndex.getString("Index.mntmCerrarSesion.text")); //$NON-NLS-1$
					mntmCerrarSesion.setIcon(new ImageIcon(Index.class.getResource("/Recursos/cerrarSesion.png"))); //$NON-NLS-1$
					mntmCerrarSesion.addActionListener(new MntmCerrarSesionActionListener());
					mnArchivo.add(mntmCerrarSesion);
				}
				{
					mntmSalir = new JMenuItem(MessagesIndex.getString("Index.mntmSalir.text")); //$NON-NLS-1$
					mntmSalir.setIcon(new ImageIcon(Index.class.getResource("/Recursos/salir.png"))); //$NON-NLS-1$
					mntmSalir.addActionListener(new MntmSalirActionListener());
					mntmSalir.setMnemonic('S');
					mntmSalir.setFont(new Font("Segoe UI", Font.PLAIN, 12)); //$NON-NLS-1$
					mnArchivo.add(mntmSalir);
				}
			}
			{
				mnEditar = new JMenu(MessagesIndex.getString("Index.mnEditar.text")); //$NON-NLS-1$
				menuBar.add(mnEditar);
				{
					mntmIdioma = new JMenuItem(MessagesIndex.getString("Index.mntmIdioma.text")); //$NON-NLS-1$
					mntmIdioma.setIcon(new ImageIcon(Index.class.getResource("/Recursos/idioma.png"))); //$NON-NLS-1$
					mntmIdioma.addActionListener(new MntmIdiomaActionListener());
					mnEditar.add(mntmIdioma);
				}
			}
			{
				mnVer = new JMenu(MessagesIndex.getString("Index.mnVer.text")); //$NON-NLS-1$
				menuBar.add(mnVer);
				{
					mnTamanoLetra = new JMenu(MessagesIndex.getString("Index.mnTamanoLetra.text")); //$NON-NLS-1$
					mnTamanoLetra.setIcon(new ImageIcon(Index.class.getResource("/Recursos/tamanoletra.png"))); //$NON-NLS-1$
					mnVer.add(mnTamanoLetra);
					mnTamanoLetra.setMnemonic('T');
					{
						rdbtnmntmPequena = new JRadioButtonMenuItem(MessagesIndex.getString("Index.rdbtnmntmPequena.text")); //$NON-NLS-1$
						buttonGroup.add(rdbtnmntmPequena);
						rdbtnmntmPequena.addActionListener(new RdbtnmntmPequenaActionListener());
						mnTamanoLetra.add(rdbtnmntmPequena);
					}
					{
						rdbtnmntmNormal = new JRadioButtonMenuItem(MessagesIndex.getString("Index.rdbtnmntmNormal.text")); //$NON-NLS-1$
						buttonGroup.add(rdbtnmntmNormal);
						rdbtnmntmNormal.addActionListener(new RdbtnmntmNormalActionListener());
						mnTamanoLetra.add(rdbtnmntmNormal);
					}
					{
						rdbtnmntmGrande = new JRadioButtonMenuItem(MessagesIndex.getString("Index.rdbtnmntmGrande.text")); //$NON-NLS-1$
						buttonGroup.add(rdbtnmntmGrande);
						rdbtnmntmGrande.addActionListener(new RdbtnmntmGrandeActionListener());
						mnTamanoLetra.add(rdbtnmntmGrande);
					}
				}
			}
			{
				mnIrA = new JMenu(MessagesIndex.getString("Index.mnIrA.text")); //$NON-NLS-1$
				menuBar.add(mnIrA);
				{
					mntmIrAInicio = new JMenuItem(MessagesIndex.getString("Index.mntmIrAInicio.text")); //$NON-NLS-1$
					mntmIrAInicio.setIcon(new ImageIcon(Index.class.getResource("/Recursos/home-26.png"))); //$NON-NLS-1$
					mntmIrAInicio.addActionListener(new MiIndexActionListener());
					mnIrA.add(mntmIrAInicio);
				}
				{
					mntmIrAParticipantes = new JMenuItem(MessagesIndex.getString("Index.mntmIrAParticipantes.text")); //$NON-NLS-1$
					mntmIrAParticipantes.setIcon(new ImageIcon(Index.class.getResource("/Recursos/participante.png"))); //$NON-NLS-1$
					mntmIrAParticipantes.addActionListener(new MiParticipanteActionListener());
					mnIrA.add(mntmIrAParticipantes);
				}
				{
					mntmIrACompeticion = new JMenuItem(MessagesIndex.getString("Index.mntmIrACompeticion.text")); //$NON-NLS-1$
					mntmIrACompeticion.setIcon(new ImageIcon(Index.class.getResource("/Recursos/competicion.png"))); //$NON-NLS-1$
					mntmIrACompeticion.addActionListener(new MiCompeticionActionListener());
					mnIrA.add(mntmIrACompeticion);
				}
				{
					mntmIrAPerfil = new JMenuItem(MessagesIndex.getString("Index.mntmIrAPerfil.text")); //$NON-NLS-1$
					mntmIrAPerfil.setIcon(new ImageIcon(Index.class.getResource("/Recursos/admin.png"))); //$NON-NLS-1$
					mntmIrAPerfil.addActionListener(new MiAdminActionListener());
					mntmIrAPerfil.setActionCommand(MessagesIndex.getString("Index.mntmIrAPerfil.actionCommand")); //$NON-NLS-1$
					mnIrA.add(mntmIrAPerfil);
				}
			}
			{
				mnAyuda = new JMenu(MessagesIndex.getString("Index.mnAyuda.text")); //$NON-NLS-1$
				menuBar.add(mnAyuda);
				{
					mntmManualUsuario = new JMenuItem(MessagesIndex.getString("Index.mntmManualUsuario.text")); //$NON-NLS-1$
					mntmManualUsuario.setIcon(new ImageIcon(Index.class.getResource("/Recursos/manual.png"))); //$NON-NLS-1$
					mntmManualUsuario.addActionListener(new MntmManualUsuarioActionListener());
					mnAyuda.add(mntmManualUsuario);
				}
				{
					mntmAcercaDe = new JMenuItem(MessagesIndex.getString("Index.mntmAcercaDe.text")); //$NON-NLS-1$
					mntmAcercaDe.addActionListener(new MntmAcercaDeActionListener());
					mntmAcercaDe.setIcon(new ImageIcon(Index.class.getResource("/Recursos/acercade.png"))); //$NON-NLS-1$
					mnAyuda.add(mntmAcercaDe);
				}
			}
		}
		frmIndex.getContentPane().setLayout(new BorderLayout(0, 0));
		{
			panelContenedor = new JPanel();
			frmIndex.getContentPane().add(panelContenedor);
			panelContenedor.setLayout(new CardLayout(0, 0));
			{
				panelIndex = new JPanel();
				panelIndex.setBackground(SystemColor.control);
				panelContenedor.add(panelIndex, "Index"); //$NON-NLS-1$
				{
					lblCompeticionesIndex = new JLabel(""); //$NON-NLS-1$
					lblCompeticionesIndex.setBounds(725, 205, 400, 250);
					{
						lblParticipantesIndex = new JLabel(""); //$NON-NLS-1$
						lblParticipantesIndex.setBounds(95, 205, 400, 250);
						panelIndex.setLayout(null);
						lblParticipantesIndex.setIcon(new ImageIcon(Index.class.getResource("/Recursos/participantes.png"))); //$NON-NLS-1$
						panelIndex.add(lblParticipantesIndex);
					}
					lblCompeticionesIndex.setIcon(new ImageIcon(Index.class.getResource("/Recursos/competiciones.png"))); //$NON-NLS-1$
					panelIndex.add(lblCompeticionesIndex);
				}
				
				JLabel lblMenu = new JLabel(""); //$NON-NLS-1$
				lblMenu.setAlignmentX(Component.CENTER_ALIGNMENT);
				lblMenu.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.0")))); //$NON-NLS-1$
				lblMenu.setBounds(295, 22, 636, 151);
				panelIndex.add(lblMenu);
				
				JButton btnParticipantes = new JButton(""); //$NON-NLS-1$
				btnParticipantes.addActionListener(new MiParticipanteActionListener());
				btnParticipantes.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.16")))); //$NON-NLS-1$
				btnParticipantes.setBounds(155, 466, 283, 65);
				panelIndex.add(btnParticipantes);
				
				JButton btnCompeticiones = new JButton(""); //$NON-NLS-1$
				btnCompeticiones.addActionListener(new MiCompeticionActionListener());
				btnCompeticiones.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.17")))); //$NON-NLS-1$
				btnCompeticiones.setBounds(783, 466, 283, 65);
				panelIndex.add(btnCompeticiones);
				{
					lblFondoIndex = new JLabel(""); //$NON-NLS-1$
					lblFondoIndex.setBackground(SystemColor.control);
					lblFondoIndex.setIcon(new ImageIcon(Index.class.getResource("/Recursos/fondoindex.png"))); //$NON-NLS-1$
					lblFondoIndex.setBounds(0, 0, 1237, 608);
					panelIndex.add(lblFondoIndex);
				}
			}
			{
				panelBuscarParticipante = new JPanel();
				panelBuscarParticipante.setBackground(SystemColor.control);
				panelBuscarParticipante.setName(MessagesIndex.getString("Index.panelBuscarParticipante.name")); //$NON-NLS-1$
				panelContenedor.add(panelBuscarParticipante, "BuscarParticipante"); //$NON-NLS-1$
				GridBagLayout gbl_panelBuscarParticipante = new GridBagLayout();
				gbl_panelBuscarParticipante.columnWidths = new int[]{75, 449, 122, 0, 0, 0, 0};
				gbl_panelBuscarParticipante.rowHeights = new int[]{0, 69, -33, 62, 83, 86, 0, 0, 0};
				gbl_panelBuscarParticipante.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
				gbl_panelBuscarParticipante.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
				panelBuscarParticipante.setLayout(gbl_panelBuscarParticipante);
				{
					{
						btnCrearParticipante = new JButton(""); //$NON-NLS-1$
						btnCrearParticipante.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.20")))); //$NON-NLS-1$
						btnCrearParticipante.addActionListener(new BtnCrearParticipanteActionListener());
						btnCrearParticipante.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
						btnCrearParticipante.setMaximumSize(new Dimension(200, 50));
						btnCrearParticipante.setMinimumSize(new Dimension(200, 50));
						btnCrearParticipante.setAlignmentX(Component.CENTER_ALIGNMENT);
						{
							lblTituloBuscarParticipante = new JLabel(""); //$NON-NLS-1$
							lblTituloBuscarParticipante.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.22")))); //$NON-NLS-1$
							lblTituloBuscarParticipante.setAlignmentX(Component.CENTER_ALIGNMENT);
							GridBagConstraints gbc_lblTituloBuscarParticipante = new GridBagConstraints();
							gbc_lblTituloBuscarParticipante.gridwidth = 2;
							gbc_lblTituloBuscarParticipante.insets = new Insets(0, 0, 5, 5);
							gbc_lblTituloBuscarParticipante.gridx = 1;
							gbc_lblTituloBuscarParticipante.gridy = 1;
							panelBuscarParticipante.add(lblTituloBuscarParticipante, gbc_lblTituloBuscarParticipante);
						}
						{
							lblFondoBuscarParticipante = new JLabel(""); //$NON-NLS-1$
							lblFondoBuscarParticipante.setIcon(new ImageIcon(Index.class.getResource("/Recursos/fondobuscarparticipante.png"))); //$NON-NLS-1$
							GridBagConstraints gbc_lblFondoBuscarParticipante = new GridBagConstraints();
							gbc_lblFondoBuscarParticipante.gridheight = 2;
							gbc_lblFondoBuscarParticipante.insets = new Insets(0, 0, 5, 5);
							gbc_lblFondoBuscarParticipante.gridx = 4;
							gbc_lblFondoBuscarParticipante.gridy = 1;
							panelBuscarParticipante.add(lblFondoBuscarParticipante, gbc_lblFondoBuscarParticipante);
						}
						{
							txtBuscarParticipante = new JTextField();
							txtBuscarParticipante.setBorder(new LineBorder(SystemColor.textText));
							txtBuscarParticipante.setText(MessagesIndex.getString("Index.txtBuscarParticipante.text")); //$NON-NLS-1$
							txtBuscarParticipante.setColumns(10);
							GridBagConstraints gbc_txtBuscarParticipante = new GridBagConstraints();
							gbc_txtBuscarParticipante.gridwidth = 2;
							gbc_txtBuscarParticipante.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtBuscarParticipante.insets = new Insets(0, 0, 5, 5);
							gbc_txtBuscarParticipante.gridx = 1;
							gbc_txtBuscarParticipante.gridy = 2;
							panelBuscarParticipante.add(txtBuscarParticipante, gbc_txtBuscarParticipante);
						}
						{
							spParticipante = new JScrollPane();
							GridBagConstraints gbc_spParticipante = new GridBagConstraints();
							gbc_spParticipante.gridwidth = 2;
							gbc_spParticipante.gridheight = 4;
							gbc_spParticipante.insets = new Insets(0, 0, 5, 5);
							gbc_spParticipante.fill = GridBagConstraints.BOTH;
							gbc_spParticipante.gridx = 1;
							gbc_spParticipante.gridy = 3;
							panelBuscarParticipante.add(spParticipante, gbc_spParticipante);
							{
								listParticipante = new JList();								
								/*listParticipante.setModel(new AbstractListModel() {
									String[] values = new String[] {"Juan Manuel Caballero Gil", "Álvaro Fernández Villa"};
									public int getSize() {
										return values.length;
									}
									public Object getElementAt(int index) {
										return values[index];
									}
								});*/
								modeloListaParticipante = new DefaultListModel();
								listParticipante.setModel(modeloListaParticipante);
								listParticipante.setSelectedIndex(0);
								listParticipante.setCellRenderer(new ParticipanteCellRenderer());
								spParticipante.setViewportView(listParticipante);
							}
						}
						GridBagConstraints gbc_btnCrearParticipante = new GridBagConstraints();
						gbc_btnCrearParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_btnCrearParticipante.gridx = 4;
						gbc_btnCrearParticipante.gridy = 3;
						panelBuscarParticipante.add(btnCrearParticipante, gbc_btnCrearParticipante);
					}
				}
				{
					btnModificarParticipante = new JButton(""); //$NON-NLS-1$
					btnModificarParticipante.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.24")))); //$NON-NLS-1$
					btnModificarParticipante.addActionListener(new BtnModificarParticipanteActionListener());
					btnModificarParticipante.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
					btnModificarParticipante.setMinimumSize(new Dimension(200, 50));
					btnModificarParticipante.setMaximumSize(new Dimension(200, 50));
					btnModificarParticipante.setAlignmentX(Component.CENTER_ALIGNMENT);
					GridBagConstraints gbc_btnModificarParticipante = new GridBagConstraints();
					gbc_btnModificarParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_btnModificarParticipante.gridx = 4;
					gbc_btnModificarParticipante.gridy = 4;
					panelBuscarParticipante.add(btnModificarParticipante, gbc_btnModificarParticipante);
				}
				{
					btnEliminarParticipante = new JButton(""); //$NON-NLS-1$
					btnEliminarParticipante.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.26")))); //$NON-NLS-1$
					btnEliminarParticipante.addActionListener(new BtnEliminarParticipanteActionListener());
					btnEliminarParticipante.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
					btnEliminarParticipante.setMaximumSize(new Dimension(200, 50));
					btnEliminarParticipante.setMinimumSize(new Dimension(200, 50));
					btnEliminarParticipante.setAlignmentX(Component.CENTER_ALIGNMENT);
					GridBagConstraints gbc_btnEliminarParticipante = new GridBagConstraints();
					gbc_btnEliminarParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_btnEliminarParticipante.gridx = 4;
					gbc_btnEliminarParticipante.gridy = 5;
					panelBuscarParticipante.add(btnEliminarParticipante, gbc_btnEliminarParticipante);
				}
				btnVerParticipante = new JButton(""); //$NON-NLS-1$
				btnVerParticipante.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.28")))); //$NON-NLS-1$
				btnVerParticipante.addActionListener(new BtnVerParticipanteActionListener());
				btnVerParticipante.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
				btnVerParticipante.setMinimumSize(new Dimension(200, 50));
				btnVerParticipante.setMaximumSize(new Dimension(200, 50));
				btnVerParticipante.setAlignmentX(Component.CENTER_ALIGNMENT);
				GridBagConstraints gbc_btnVerParticipante = new GridBagConstraints();
				gbc_btnVerParticipante.insets = new Insets(0, 0, 5, 5);
				gbc_btnVerParticipante.gridx = 4;
				gbc_btnVerParticipante.gridy = 6;
				panelBuscarParticipante.add(btnVerParticipante, gbc_btnVerParticipante);
			}
			panelBuscarCompeticion = new JPanel();
			panelBuscarCompeticion.setBackground(SystemColor.control);
			panelContenedor.add(panelBuscarCompeticion, "BuscarCompeticion"); //$NON-NLS-1$
			GridBagLayout gbl_panelBuscarCompeticion = new GridBagLayout();
			gbl_panelBuscarCompeticion.columnWidths = new int[]{75, 449, 122, 0, 0, 0, 0};
			gbl_panelBuscarCompeticion.rowHeights = new int[]{0, 69, -33, 62, 0, 83, 86, 0, 0};
			gbl_panelBuscarCompeticion.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
			gbl_panelBuscarCompeticion.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
			panelBuscarCompeticion.setLayout(gbl_panelBuscarCompeticion);
			{
				lblTituloBuscarCompeticion = new JLabel(""); //$NON-NLS-1$
				lblTituloBuscarCompeticion.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.31")))); //$NON-NLS-1$
				GridBagConstraints gbc_lblTituloBuscarCompeticion = new GridBagConstraints();
				gbc_lblTituloBuscarCompeticion.gridwidth = 2;
				gbc_lblTituloBuscarCompeticion.insets = new Insets(0, 0, 5, 5);
				gbc_lblTituloBuscarCompeticion.gridx = 1;
				gbc_lblTituloBuscarCompeticion.gridy = 1;
				panelBuscarCompeticion.add(lblTituloBuscarCompeticion, gbc_lblTituloBuscarCompeticion);
			}
			{
				lblFondoBuscarCompeticion = new JLabel(""); //$NON-NLS-1$
				lblFondoBuscarCompeticion.setIcon(new ImageIcon(Index.class.getResource("/Recursos/fondobuscarcompeticion.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_lblFondoBuscarCompeticion = new GridBagConstraints();
				gbc_lblFondoBuscarCompeticion.gridheight = 2;
				gbc_lblFondoBuscarCompeticion.insets = new Insets(0, 0, 5, 5);
				gbc_lblFondoBuscarCompeticion.gridx = 4;
				gbc_lblFondoBuscarCompeticion.gridy = 1;
				panelBuscarCompeticion.add(lblFondoBuscarCompeticion, gbc_lblFondoBuscarCompeticion);
			}
			{
				txtBuscarCompeticion = new JTextField();
				txtBuscarCompeticion.setBorder(new LineBorder(new Color(0, 0, 0)));
				txtBuscarCompeticion.setText(MessagesIndex.getString("Index.txtBuscarCompeticion.text")); //$NON-NLS-1$
				txtBuscarCompeticion.setColumns(10);
				GridBagConstraints gbc_txtBuscarCompeticion = new GridBagConstraints();
				gbc_txtBuscarCompeticion.gridwidth = 2;
				gbc_txtBuscarCompeticion.fill = GridBagConstraints.HORIZONTAL;
				gbc_txtBuscarCompeticion.insets = new Insets(0, 0, 5, 5);
				gbc_txtBuscarCompeticion.gridx = 1;
				gbc_txtBuscarCompeticion.gridy = 2;
				panelBuscarCompeticion.add(txtBuscarCompeticion, gbc_txtBuscarCompeticion);
			}
			{
				spCompeticion = new JScrollPane();
				GridBagConstraints gbc_spCompeticion = new GridBagConstraints();
				gbc_spCompeticion.gridwidth = 2;
				gbc_spCompeticion.gridheight = 4;
				gbc_spCompeticion.insets = new Insets(0, 0, 5, 5);
				gbc_spCompeticion.fill = GridBagConstraints.BOTH;
				gbc_spCompeticion.gridx = 1;
				gbc_spCompeticion.gridy = 3;
				panelBuscarCompeticion.add(spCompeticion, gbc_spCompeticion);
				{
					listCompeticion = new JList();
					/*listCompeticion.setModel(new AbstractListModel() {
						String[] values = new String[] {"Ultraman 2014 Hawaii", "Ironman 2014 Lanzarote", "VII Carrera Urbana de Ciudad Real"};
						public int getSize() {
							return values.length;
						}
						public Object getElementAt(int index) {
							return values[index];
						}
					});*/
					modeloListaCompeticion = new DefaultListModel();
					listCompeticion.setModel(modeloListaCompeticion);
					listCompeticion.setSelectedIndex(0);
					listCompeticion.setCellRenderer(new CompeticionCellRenderer());
					spCompeticion.setViewportView(listCompeticion);
				}
			}
			{
				btnCrearCompeticion = new JButton(""); //$NON-NLS-1$
				btnCrearCompeticion.setAlignmentX(Component.CENTER_ALIGNMENT);
				btnCrearCompeticion.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.33")))); //$NON-NLS-1$
				btnCrearCompeticion.addActionListener(new BtnCrearCompeticionActionListener());
				btnCrearCompeticion.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
				btnCrearCompeticion.setMaximumSize(new Dimension(200, 50));
				btnCrearCompeticion.setMinimumSize(new Dimension(200, 50));
				GridBagConstraints gbc_btnCrearCompeticion = new GridBagConstraints();
				gbc_btnCrearCompeticion.insets = new Insets(0, 0, 5, 5);
				gbc_btnCrearCompeticion.gridx = 4;
				gbc_btnCrearCompeticion.gridy = 3;
				panelBuscarCompeticion.add(btnCrearCompeticion, gbc_btnCrearCompeticion);
			}
			{
				btnModificarCompeticion = new JButton(""); //$NON-NLS-1$
				btnModificarCompeticion.setAlignmentX(Component.CENTER_ALIGNMENT);
				btnModificarCompeticion.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.35")))); //$NON-NLS-1$
				btnModificarCompeticion.addActionListener(new BtnModificarCompeticionActionListener());
				btnModificarCompeticion.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
				btnModificarCompeticion.setMaximumSize(new Dimension(200, 50));
				btnModificarCompeticion.setMinimumSize(new Dimension(200, 50));
				GridBagConstraints gbc_btnModificarCompeticion = new GridBagConstraints();
				gbc_btnModificarCompeticion.insets = new Insets(0, 0, 5, 5);
				gbc_btnModificarCompeticion.gridx = 4;
				gbc_btnModificarCompeticion.gridy = 4;
				panelBuscarCompeticion.add(btnModificarCompeticion, gbc_btnModificarCompeticion);
			}
			btnEliminarCompeticion = new JButton(""); //$NON-NLS-1$
			btnEliminarCompeticion.setAlignmentX(Component.CENTER_ALIGNMENT);
			btnEliminarCompeticion.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.37")))); //$NON-NLS-1$
			btnEliminarCompeticion.addActionListener(new BtnEliminarCompeticionActionListener());
			btnEliminarCompeticion.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
			btnEliminarCompeticion.setMinimumSize(new Dimension(200, 50));
			btnEliminarCompeticion.setMaximumSize(new Dimension(200, 50));
			GridBagConstraints gbc_btnEliminarCompeticion = new GridBagConstraints();
			gbc_btnEliminarCompeticion.insets = new Insets(0, 0, 5, 5);
			gbc_btnEliminarCompeticion.gridx = 4;
			gbc_btnEliminarCompeticion.gridy = 5;
			panelBuscarCompeticion.add(btnEliminarCompeticion, gbc_btnEliminarCompeticion);
			btnVerCompeticion = new JButton(""); //$NON-NLS-1$
			btnVerCompeticion.setAlignmentX(Component.CENTER_ALIGNMENT);
			btnVerCompeticion.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.39")))); //$NON-NLS-1$
			btnVerCompeticion.addActionListener(new BtnVerCompeticionActionListener());
			btnVerCompeticion.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
			btnVerCompeticion.setMaximumSize(new Dimension(200, 50));
			btnVerCompeticion.setMinimumSize(new Dimension(200, 50));
			GridBagConstraints gbc_btnVerCompeticion = new GridBagConstraints();
			gbc_btnVerCompeticion.insets = new Insets(0, 0, 5, 5);
			gbc_btnVerCompeticion.gridx = 4;
			gbc_btnVerCompeticion.gridy = 6;
			panelBuscarCompeticion.add(btnVerCompeticion, gbc_btnVerCompeticion);
			{
				panelParticipante = new JPanel();
				panelParticipante.setBackground(SystemColor.inactiveCaptionBorder);
				panelContenedor.add(panelParticipante, "Participante"); //$NON-NLS-1$
				GridBagLayout gbl_panelParticipante = new GridBagLayout();
				gbl_panelParticipante.columnWidths = new int[]{0, 200, 122, 123, 0, 0, 104, 32, 63, 0, 10, 0, 0};
				gbl_panelParticipante.rowHeights = new int[]{43, 16, 0, 0, 0, 0, 0, 0, 0, 0, 61, 0, 0, 0, 0, 53, 34, 0};
				gbl_panelParticipante.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
				gbl_panelParticipante.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				panelParticipante.setLayout(gbl_panelParticipante);
				{
					lblCompe = new JLabel(""); //$NON-NLS-1$
					lblCompe.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.42")))); //$NON-NLS-1$
					GridBagConstraints gbc_lblCompe = new GridBagConstraints();
					gbc_lblCompe.insets = new Insets(0, 0, 5, 5);
					gbc_lblCompe.gridx = 9;
					gbc_lblCompe.gridy = 1;
					panelParticipante.add(lblCompe, gbc_lblCompe);
				}
				{
					panelDatosParticipante = new JPanel();
					panelDatosParticipante.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					panelDatosParticipante.setBackground(SystemColor.controlHighlight);
					GridBagConstraints gbc_panelDatosParticipante = new GridBagConstraints();
					gbc_panelDatosParticipante.gridwidth = 6;
					gbc_panelDatosParticipante.gridheight = 12;
					gbc_panelDatosParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_panelDatosParticipante.fill = GridBagConstraints.VERTICAL;
					gbc_panelDatosParticipante.gridx = 1;
					gbc_panelDatosParticipante.gridy = 1;
					panelParticipante.add(panelDatosParticipante, gbc_panelDatosParticipante);
					GridBagLayout gbl_panelDatosParticipante = new GridBagLayout();
					gbl_panelDatosParticipante.columnWidths = new int[]{0, 115, 109, 104, 0, 0, 85, 0, 57, 0, 0};
					gbl_panelDatosParticipante.rowHeights = new int[]{0, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 53, 24, 0, 37, 0};
					gbl_panelDatosParticipante.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					gbl_panelDatosParticipante.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					panelDatosParticipante.setLayout(gbl_panelDatosParticipante);
					{
						spFotoParticipante = new JScrollPane();
						GridBagConstraints gbc_spFotoParticipante = new GridBagConstraints();
						gbc_spFotoParticipante.gridheight = 5;
						gbc_spFotoParticipante.fill = GridBagConstraints.BOTH;
						gbc_spFotoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_spFotoParticipante.gridx = 1;
						gbc_spFotoParticipante.gridy = 1;
						panelDatosParticipante.add(spFotoParticipante, gbc_spFotoParticipante);
						{
							lblFoto = new JLabel(""); //$NON-NLS-1$
							lblFoto.setHorizontalAlignment(SwingConstants.CENTER);
							lblFoto.setIcon(new ImageIcon(Index.class.getResource("/Recursos/av1.png"))); //$NON-NLS-1$
							spFotoParticipante.setViewportView(lblFoto);
						}
					}
					{
						lblNombreParticipante = new JLabel(MessagesIndex.getString("Index.lblNombreParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblNombreParticipante = new GridBagConstraints();
						gbc_lblNombreParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblNombreParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblNombreParticipante.gridx = 2;
						gbc_lblNombreParticipante.gridy = 1;
						panelDatosParticipante.add(lblNombreParticipante, gbc_lblNombreParticipante);
					}
					{
						txtNombreParticipante = new JTextField();
						GridBagConstraints gbc_txtNombreParticipante = new GridBagConstraints();
						gbc_txtNombreParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtNombreParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtNombreParticipante.gridx = 3;
						gbc_txtNombreParticipante.gridy = 1;
						panelDatosParticipante.add(txtNombreParticipante, gbc_txtNombreParticipante);
					}
					{
						lblApellidosParticipante = new JLabel(MessagesIndex.getString("Index.lblApellidosParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblApellidosParticipante = new GridBagConstraints();
						gbc_lblApellidosParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblApellidosParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblApellidosParticipante.gridx = 2;
						gbc_lblApellidosParticipante.gridy = 2;
						panelDatosParticipante.add(lblApellidosParticipante, gbc_lblApellidosParticipante);
					}
					{
						txtApellidosParticipante = new JTextField();
						GridBagConstraints gbc_txtApellidosParticipante = new GridBagConstraints();
						gbc_txtApellidosParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtApellidosParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtApellidosParticipante.gridx = 3;
						gbc_txtApellidosParticipante.gridy = 2;
						panelDatosParticipante.add(txtApellidosParticipante, gbc_txtApellidosParticipante);
					}
					{
						
						lblDNIParticipante = new JLabel(MessagesIndex.getString("Index.lblDNIParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblDNIParticipante = new GridBagConstraints();
						gbc_lblDNIParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblDNIParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblDNIParticipante.gridx = 2;
						gbc_lblDNIParticipante.gridy = 3;
						panelDatosParticipante.add(lblDNIParticipante, gbc_lblDNIParticipante);
						
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("########-U"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtDNIParticipante = new JFormattedTextField(mascara);
							GridBagConstraints gbc_txtDNIParticipante = new GridBagConstraints();
							gbc_txtDNIParticipante.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtDNIParticipante.insets = new Insets(0, 0, 5, 5);
							gbc_txtDNIParticipante.gridx = 3;
							gbc_txtDNIParticipante.gridy = 3;
							panelDatosParticipante.add(txtDNIParticipante, gbc_txtDNIParticipante);
						}
						catch(Exception ex){
							
						}
					}
					{
						lblFechaNacimientoParticipante = new JLabel(MessagesIndex.getString("Index.lblFechaNacimientoParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblFechaNacimientoParticipante = new GridBagConstraints();
						gbc_lblFechaNacimientoParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblFechaNacimientoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblFechaNacimientoParticipante.gridx = 2;
						gbc_lblFechaNacimientoParticipante.gridy = 4;
						panelDatosParticipante.add(lblFechaNacimientoParticipante, gbc_lblFechaNacimientoParticipante);
					}
					{
										
						cbFechaNacimientoDiaParticipante = new JComboBox();
						cbFechaNacimientoDiaParticipante.setModel(new DefaultComboBoxModel(new String[] {" ", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$ //$NON-NLS-32$
						
						
						GridBagConstraints gbc_cbFechaNacimientoDiaParticipante = new GridBagConstraints();
						gbc_cbFechaNacimientoDiaParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbFechaNacimientoDiaParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaNacimientoDiaParticipante.gridx = 3;
						gbc_cbFechaNacimientoDiaParticipante.gridy = 4;
						panelDatosParticipante.add(cbFechaNacimientoDiaParticipante, gbc_cbFechaNacimientoDiaParticipante);
					}
					{
						cbFechaNacimientoMesParticipante = new JComboBox();
						cbFechaNacimientoMesParticipante.setModel(new DefaultComboBoxModel(new String[] {" ","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$
						GridBagConstraints gbc_cbFechaNacimientoMesParticipante = new GridBagConstraints();
						gbc_cbFechaNacimientoMesParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbFechaNacimientoMesParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaNacimientoMesParticipante.gridx = 4;
						gbc_cbFechaNacimientoMesParticipante.gridy = 4;
						panelDatosParticipante.add(cbFechaNacimientoMesParticipante, gbc_cbFechaNacimientoMesParticipante);
					}
					{
						cbFechaNacimientoAnoParticipante = new JComboBox();
						cbFechaNacimientoAnoParticipante.setModel(new DefaultComboBoxModel(new String[] {" ","1920", "1921", "1922", "1923", "1924", "1925", "1926", "1927", "1928", "1929", "1930", "1931", "1932", "1933", "1934", "1935", "1936", "1937", "1938", "1939", "1940", "1941", "1942", "1943", "1944", "1945", "1946", "1947", "1948", "1949", "1950", "1951", "1952", "1953", "1954", "1955", "1956", "1957", "1958", "1959", "1960", "1961", "1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$ //$NON-NLS-32$ //$NON-NLS-33$ //$NON-NLS-34$ //$NON-NLS-35$ //$NON-NLS-36$ //$NON-NLS-37$ //$NON-NLS-38$ //$NON-NLS-39$ //$NON-NLS-40$ //$NON-NLS-41$ //$NON-NLS-42$ //$NON-NLS-43$ //$NON-NLS-44$ //$NON-NLS-45$ //$NON-NLS-46$ //$NON-NLS-47$ //$NON-NLS-48$ //$NON-NLS-49$ //$NON-NLS-50$ //$NON-NLS-51$ //$NON-NLS-52$ //$NON-NLS-53$ //$NON-NLS-54$ //$NON-NLS-55$ //$NON-NLS-56$ //$NON-NLS-57$ //$NON-NLS-58$ //$NON-NLS-59$ //$NON-NLS-60$ //$NON-NLS-61$ //$NON-NLS-62$ //$NON-NLS-63$ //$NON-NLS-64$ //$NON-NLS-65$ //$NON-NLS-66$ //$NON-NLS-67$ //$NON-NLS-68$ //$NON-NLS-69$ //$NON-NLS-70$ //$NON-NLS-71$ //$NON-NLS-72$ //$NON-NLS-73$ //$NON-NLS-74$ //$NON-NLS-75$ //$NON-NLS-76$ //$NON-NLS-77$ //$NON-NLS-78$ //$NON-NLS-79$ //$NON-NLS-80$ //$NON-NLS-81$ //$NON-NLS-82$ //$NON-NLS-83$ //$NON-NLS-84$ //$NON-NLS-85$ //$NON-NLS-86$ //$NON-NLS-87$ //$NON-NLS-88$ //$NON-NLS-89$ //$NON-NLS-90$ //$NON-NLS-91$ //$NON-NLS-92$ //$NON-NLS-93$ //$NON-NLS-94$ //$NON-NLS-95$ //$NON-NLS-96$
						GridBagConstraints gbc_cbFechaNacimientoAnoParticipante = new GridBagConstraints();
						gbc_cbFechaNacimientoAnoParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbFechaNacimientoAnoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaNacimientoAnoParticipante.gridx = 5;
						gbc_cbFechaNacimientoAnoParticipante.gridy = 4;
						panelDatosParticipante.add(cbFechaNacimientoAnoParticipante, gbc_cbFechaNacimientoAnoParticipante);
					}
					{
						lblSexoParticipante = new JLabel(MessagesIndex.getString("Index.lblSexoParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblSexoParticipante = new GridBagConstraints();
						gbc_lblSexoParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblSexoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblSexoParticipante.gridx = 2;
						gbc_lblSexoParticipante.gridy = 5;
						panelDatosParticipante.add(lblSexoParticipante, gbc_lblSexoParticipante);
					}
					{
						cbSexoParticipante = new JComboBox();
						cbSexoParticipante.setModel(new DefaultComboBoxModel(new String[] {" ","Masculino", "Femenino"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						GridBagConstraints gbc_cbSexoParticipante = new GridBagConstraints();
						gbc_cbSexoParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbSexoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_cbSexoParticipante.gridx = 3;
						gbc_cbSexoParticipante.gridy = 5;
						panelDatosParticipante.add(cbSexoParticipante, gbc_cbSexoParticipante);
					}
					{
						lblDireccionParticipante = new JLabel(MessagesIndex.getString("Index.lblDireccionParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblDireccionParticipante = new GridBagConstraints();
						gbc_lblDireccionParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblDireccionParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblDireccionParticipante.gridx = 1;
						gbc_lblDireccionParticipante.gridy = 6;
						panelDatosParticipante.add(lblDireccionParticipante, gbc_lblDireccionParticipante);
					}
					{
						cbDireccionParticipante = new JComboBox();
						cbDireccionParticipante.setModel(new DefaultComboBoxModel(new String[] {" ","Calle", "Avenida", "Camino", "Cuesta", "Paseo", "Ronda", "Travesía", "Pasaje"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
						GridBagConstraints gbc_cbDireccionParticipante = new GridBagConstraints();
						gbc_cbDireccionParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbDireccionParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_cbDireccionParticipante.gridx = 2;
						gbc_cbDireccionParticipante.gridy = 6;
						panelDatosParticipante.add(cbDireccionParticipante, gbc_cbDireccionParticipante);
					}
					{
						txtDireccionParticipante = new JTextField();
						GridBagConstraints gbc_txtDireccionParticipante = new GridBagConstraints();
						gbc_txtDireccionParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtDireccionParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtDireccionParticipante.gridx = 3;
						gbc_txtDireccionParticipante.gridy = 6;
						panelDatosParticipante.add(txtDireccionParticipante, gbc_txtDireccionParticipante);
					}
					{
						lblNumeroParticipante = new JLabel(MessagesIndex.getString("Index.lblNumeroParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblNumeroParticipante = new GridBagConstraints();
						gbc_lblNumeroParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblNumeroParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblNumeroParticipante.gridx = 4;
						gbc_lblNumeroParticipante.gridy = 6;
						panelDatosParticipante.add(lblNumeroParticipante, gbc_lblNumeroParticipante);
					}
					{
						try{
						MaskFormatter mascara=new MaskFormatter("##");	 //$NON-NLS-1$
						mascara.setPlaceholderCharacter('X');
						txtNumeroParticipante = new JFormattedTextField(mascara);
						GridBagConstraints gbc_txtNumeroParticipante = new GridBagConstraints();
						gbc_txtNumeroParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtNumeroParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtNumeroParticipante.gridx = 5;
						gbc_txtNumeroParticipante.gridy = 6;
						panelDatosParticipante.add(txtNumeroParticipante, gbc_txtNumeroParticipante);
						}catch(Exception ex){
							
						}
					}
					{
						lblEscaleraParticipante = new JLabel(MessagesIndex.getString("Index.lblEscaleraParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblEscaleraParticipante = new GridBagConstraints();
						gbc_lblEscaleraParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblEscaleraParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblEscaleraParticipante.gridx = 1;
						gbc_lblEscaleraParticipante.gridy = 7;
						panelDatosParticipante.add(lblEscaleraParticipante, gbc_lblEscaleraParticipante);
					}
					{
						cbEscaleraParticipante = new JComboBox();
						cbEscaleraParticipante.setModel(new DefaultComboBoxModel(new String[] {" ","Derecha", "Izquierda"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						GridBagConstraints gbc_cbEscaleraParticipante = new GridBagConstraints();
						gbc_cbEscaleraParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbEscaleraParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_cbEscaleraParticipante.gridx = 2;
						gbc_cbEscaleraParticipante.gridy = 7;
						panelDatosParticipante.add(cbEscaleraParticipante, gbc_cbEscaleraParticipante);
					}
					{
						lblPisoParticipante = new JLabel(MessagesIndex.getString("Index.lblPisoParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblPisoParticipante = new GridBagConstraints();
						gbc_lblPisoParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblPisoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblPisoParticipante.gridx = 3;
						gbc_lblPisoParticipante.gridy = 7;
						panelDatosParticipante.add(lblPisoParticipante, gbc_lblPisoParticipante);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("#"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtPisoParticipante = new JFormattedTextField(mascara);
							GridBagConstraints gbc_txtPisoParticipante = new GridBagConstraints();
							gbc_txtPisoParticipante.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtPisoParticipante.insets = new Insets(0, 0, 5, 5);
							gbc_txtPisoParticipante.gridx = 4;
							gbc_txtPisoParticipante.gridy = 7;
							panelDatosParticipante.add(txtPisoParticipante, gbc_txtPisoParticipante);
						}
						catch(Exception ex){

						}
					}
					{
						lblPuertaParticipante = new JLabel(MessagesIndex.getString("Index.lblPuertaParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblPuertaParticipante = new GridBagConstraints();
						gbc_lblPuertaParticipante.fill = GridBagConstraints.VERTICAL;
						gbc_lblPuertaParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblPuertaParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblPuertaParticipante.gridx = 5;
						gbc_lblPuertaParticipante.gridy = 7;
						panelDatosParticipante.add(lblPuertaParticipante, gbc_lblPuertaParticipante);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("U"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtPuertaParticipante = new JFormattedTextField(mascara);
						
						GridBagConstraints gbc_txtPuertaParticipante = new GridBagConstraints();
						gbc_txtPuertaParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtPuertaParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtPuertaParticipante.gridx = 6;
						gbc_txtPuertaParticipante.gridy = 7;
						panelDatosParticipante.add(txtPuertaParticipante, gbc_txtPuertaParticipante);
						}
						catch(Exception ex){
							
						}
					}
					{
						lblCodigoPostalParticipante = new JLabel(MessagesIndex.getString("Index.lblCodigoPostalParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblCodigoPostalParticipante = new GridBagConstraints();
						gbc_lblCodigoPostalParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblCodigoPostalParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblCodigoPostalParticipante.gridx = 1;
						gbc_lblCodigoPostalParticipante.gridy = 8;
						panelDatosParticipante.add(lblCodigoPostalParticipante, gbc_lblCodigoPostalParticipante);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("#####"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtCodigoPostalParticipante = new JFormattedTextField(mascara);
						
						GridBagConstraints gbc_txtCodigoPostalParticipante = new GridBagConstraints();
						gbc_txtCodigoPostalParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtCodigoPostalParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtCodigoPostalParticipante.gridx = 2;
						gbc_txtCodigoPostalParticipante.gridy = 8;
						panelDatosParticipante.add(txtCodigoPostalParticipante, gbc_txtCodigoPostalParticipante);
						}
						catch(Exception ex){
							
						}
					}
					{
						lblProvinciaParticipante = new JLabel(MessagesIndex.getString("Index.lblProvinciaParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblProvinciaParticipante = new GridBagConstraints();
						gbc_lblProvinciaParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblProvinciaParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblProvinciaParticipante.gridx = 3;
						gbc_lblProvinciaParticipante.gridy = 8;
						panelDatosParticipante.add(lblProvinciaParticipante, gbc_lblProvinciaParticipante);
					}
					{
						txtProvinciaParticipante = new JTextField();
						GridBagConstraints gbc_txtProvinciaParticipante = new GridBagConstraints();
						gbc_txtProvinciaParticipante.gridwidth = 2;
						gbc_txtProvinciaParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtProvinciaParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtProvinciaParticipante.gridx = 4;
						gbc_txtProvinciaParticipante.gridy = 8;
						panelDatosParticipante.add(txtProvinciaParticipante, gbc_txtProvinciaParticipante);
					}
					{
						lblLocalidadParticipante = new JLabel(MessagesIndex.getString("Index.lblLocalidadParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblLocalidadParticipante = new GridBagConstraints();
						gbc_lblLocalidadParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblLocalidadParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblLocalidadParticipante.gridx = 1;
						gbc_lblLocalidadParticipante.gridy = 9;
						panelDatosParticipante.add(lblLocalidadParticipante, gbc_lblLocalidadParticipante);
					}
					{
						txtLocalidadParticipante = new JTextField();
						GridBagConstraints gbc_txtLocalidadParticipante = new GridBagConstraints();
						gbc_txtLocalidadParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtLocalidadParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtLocalidadParticipante.gridx = 2;
						gbc_txtLocalidadParticipante.gridy = 9;
						panelDatosParticipante.add(txtLocalidadParticipante, gbc_txtLocalidadParticipante);
					}
					{
						lblPaisParticipante = new JLabel(MessagesIndex.getString("Index.lblPaisParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblPaisParticipante = new GridBagConstraints();
						gbc_lblPaisParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblPaisParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblPaisParticipante.gridx = 3;
						gbc_lblPaisParticipante.gridy = 9;
						panelDatosParticipante.add(lblPaisParticipante, gbc_lblPaisParticipante);
					}
					{
						txtPaisParticipante = new JTextField();
						GridBagConstraints gbc_txtPaisParticipante = new GridBagConstraints();
						gbc_txtPaisParticipante.gridwidth = 2;
						gbc_txtPaisParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtPaisParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtPaisParticipante.gridx = 4;
						gbc_txtPaisParticipante.gridy = 9;
						panelDatosParticipante.add(txtPaisParticipante, gbc_txtPaisParticipante);
					}
					{
						lblTelefonoParticipante = new JLabel(MessagesIndex.getString("Index.lblTelefonoParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblTelefonoParticipante = new GridBagConstraints();
						gbc_lblTelefonoParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblTelefonoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblTelefonoParticipante.gridx = 1;
						gbc_lblTelefonoParticipante.gridy = 10;
						panelDatosParticipante.add(lblTelefonoParticipante, gbc_lblTelefonoParticipante);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("#########"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtTelefonoParticipante= new JFormattedTextField(mascara);
						
							GridBagConstraints gbc_txtTelefonoParticipante = new GridBagConstraints();
							gbc_txtTelefonoParticipante.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtTelefonoParticipante.insets = new Insets(0, 0, 5, 5);
							gbc_txtTelefonoParticipante.gridx = 2;
							gbc_txtTelefonoParticipante.gridy = 10;
							panelDatosParticipante.add(txtTelefonoParticipante, gbc_txtTelefonoParticipante);
						}
						catch(Exception ex){
							
						}
					}
					{
						lblCorreoElectronicoParticipante = new JLabel(MessagesIndex.getString("Index.lblCorreoElectronicoParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblCorreoElectronicoParticipante = new GridBagConstraints();
						gbc_lblCorreoElectronicoParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblCorreoElectronicoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblCorreoElectronicoParticipante.gridx = 4;
						gbc_lblCorreoElectronicoParticipante.gridy = 10;
						panelDatosParticipante.add(lblCorreoElectronicoParticipante, gbc_lblCorreoElectronicoParticipante);
					}
					{
						txtCorreoElectronicoParticipante = new JTextField();
						GridBagConstraints gbc_txtCorreoElectronicoParticipante = new GridBagConstraints();
						gbc_txtCorreoElectronicoParticipante.gridwidth = 2;
						gbc_txtCorreoElectronicoParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtCorreoElectronicoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_txtCorreoElectronicoParticipante.gridx = 5;
						gbc_txtCorreoElectronicoParticipante.gridy = 10;
						panelDatosParticipante.add(txtCorreoElectronicoParticipante, gbc_txtCorreoElectronicoParticipante);
					}
					{
						lblTallaParticipante = new JLabel(MessagesIndex.getString("Index.lblTallaParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblTallaParticipante = new GridBagConstraints();
						gbc_lblTallaParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblTallaParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblTallaParticipante.gridx = 1;
						gbc_lblTallaParticipante.gridy = 11;
						panelDatosParticipante.add(lblTallaParticipante, gbc_lblTallaParticipante);
					}
					{
						cbTallaParticipante = new JComboBox();
						cbTallaParticipante.setModel(new DefaultComboBoxModel(new String[] {" ","XS", "S", "L", "XL", "XLL"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
						GridBagConstraints gbc_cbTallaParticipante = new GridBagConstraints();
						gbc_cbTallaParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbTallaParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_cbTallaParticipante.gridx = 2;
						gbc_cbTallaParticipante.gridy = 11;
						panelDatosParticipante.add(cbTallaParticipante, gbc_cbTallaParticipante);
					}
					{
						lblTipoParticipante = new JLabel(MessagesIndex.getString("Index.lblTipoParticipante.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblTipoParticipante = new GridBagConstraints();
						gbc_lblTipoParticipante.anchor = GridBagConstraints.EAST;
						gbc_lblTipoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_lblTipoParticipante.gridx = 4;
						gbc_lblTipoParticipante.gridy = 11;
						panelDatosParticipante.add(lblTipoParticipante, gbc_lblTipoParticipante);
					}
					{
						cbTipoParticipante = new JComboBox();
						cbTipoParticipante.setModel(new DefaultComboBoxModel(new String[] {" ","Junior", "Novato", "Intermedio", "Confirmado", "Experto", "Élite"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
						GridBagConstraints gbc_cbTipoParticipante = new GridBagConstraints();
						gbc_cbTipoParticipante.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbTipoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_cbTipoParticipante.gridx = 5;
						gbc_cbTipoParticipante.gridy = 11;
						panelDatosParticipante.add(cbTipoParticipante, gbc_cbTipoParticipante);
					}
					{
						spClubDeportivoParticipante =new JScrollPane();
						GridBagConstraints gbc_spClubDeportivoParticipante = new GridBagConstraints();
						gbc_spClubDeportivoParticipante.fill = GridBagConstraints.BOTH;
						gbc_spClubDeportivoParticipante.gridwidth = 2;
						gbc_spClubDeportivoParticipante.gridheight = 4;
						gbc_spClubDeportivoParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_spClubDeportivoParticipante.gridx = 2;
						gbc_spClubDeportivoParticipante.gridy = 12;
						panelDatosParticipante.add(spClubDeportivoParticipante, gbc_spClubDeportivoParticipante);
						{
							lblClubDeportivoParticipante = new JLabel(""); //$NON-NLS-1$
							lblClubDeportivoParticipante.setHorizontalAlignment(SwingConstants.CENTER);
							lblClubDeportivoParticipante.setIcon(new ImageIcon(Index.class.getResource("/Recursos/logoentidad.png"))); //$NON-NLS-1$
							spClubDeportivoParticipante.setViewportView(lblClubDeportivoParticipante);
						}
					}
					{
						spSponsorParticipante = new JScrollPane();
						GridBagConstraints gbc_spSponsorParticipante = new GridBagConstraints();
						gbc_spSponsorParticipante.fill = GridBagConstraints.BOTH;
						gbc_spSponsorParticipante.gridheight = 5;
						gbc_spSponsorParticipante.gridwidth = 4;
						gbc_spSponsorParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_spSponsorParticipante.gridx = 5;
						gbc_spSponsorParticipante.gridy = 12;
						panelDatosParticipante.add(spSponsorParticipante, gbc_spSponsorParticipante);
						{
							lblSponsorParticipante = new JLabel(""); //$NON-NLS-1$
							lblSponsorParticipante.setHorizontalAlignment(SwingConstants.CENTER);
							lblSponsorParticipante.setIcon(new ImageIcon(Index.class.getResource("/Recursos/logoentidadcoop.png"))); //$NON-NLS-1$
							spSponsorParticipante.setViewportView(lblSponsorParticipante);
						}
					}
					{
						btnGuardarParticipanteParticipante = new JButton(MessagesIndex.getString("Index.btnGuardarParticipanteParticipante.text")); //$NON-NLS-1$
						btnGuardarParticipanteParticipante.addActionListener(new BtnGuardarParticipanteParticipanteActionListener());
						btnGuardarParticipanteParticipante.setFont(new Font("Tahoma", Font.PLAIN, 13)); //$NON-NLS-1$
						GridBagConstraints gbc_btnGuardarParticipanteParticipante = new GridBagConstraints();
						gbc_btnGuardarParticipanteParticipante.fill = GridBagConstraints.VERTICAL;
						gbc_btnGuardarParticipanteParticipante.insets = new Insets(0, 0, 0, 5);
						gbc_btnGuardarParticipanteParticipante.gridx = 1;
						gbc_btnGuardarParticipanteParticipante.gridy = 16;
						panelDatosParticipante.add(btnGuardarParticipanteParticipante, gbc_btnGuardarParticipanteParticipante);
					}
					{
						btnModificarParticipanteParticipante = new JButton(MessagesIndex.getString("Index.btnModificarParticipanteParticipante.text")); //$NON-NLS-1$
						btnModificarParticipanteParticipante.addActionListener(new BtnModificarParticipanteParticipanteActionListener());
						btnModificarParticipanteParticipante.setFont(new Font("Tahoma", Font.PLAIN, 13)); //$NON-NLS-1$
						GridBagConstraints gbc_btnModificarParticipanteParticipante = new GridBagConstraints();
						gbc_btnModificarParticipanteParticipante.fill = GridBagConstraints.VERTICAL;
						gbc_btnModificarParticipanteParticipante.insets = new Insets(0, 0, 0, 5);
						gbc_btnModificarParticipanteParticipante.gridx = 2;
						gbc_btnModificarParticipanteParticipante.gridy = 16;
						panelDatosParticipante.add(btnModificarParticipanteParticipante, gbc_btnModificarParticipanteParticipante);
					}
					{
						btnEliminarParticipanteParticipante = new JButton(MessagesIndex.getString("Index.btnEliminarParticipanteParticipante.text")); //$NON-NLS-1$
						btnEliminarParticipanteParticipante.addActionListener(new BtnEliminarParticipanteParticipanteActionListener());
						btnEliminarParticipanteParticipante.setFont(new Font("Tahoma", Font.PLAIN, 13)); //$NON-NLS-1$
						GridBagConstraints gbc_btnEliminarParticipanteParticipante = new GridBagConstraints();
						gbc_btnEliminarParticipanteParticipante.fill = GridBagConstraints.VERTICAL;
						gbc_btnEliminarParticipanteParticipante.insets = new Insets(0, 0, 0, 5);
						gbc_btnEliminarParticipanteParticipante.gridx = 3;
						gbc_btnEliminarParticipanteParticipante.gridy = 16;
						panelDatosParticipante.add(btnEliminarParticipanteParticipante, gbc_btnEliminarParticipanteParticipante);
					}
				}
				{
					panelResumenCompeticionParticipante = new JPanel();
					panelResumenCompeticionParticipante.setBorder(new LineBorder(new Color(0, 0, 0), 2));
					GridBagConstraints gbc_panelResumenCompeticionParticipante = new GridBagConstraints();
					gbc_panelResumenCompeticionParticipante.gridwidth = 3;
					gbc_panelResumenCompeticionParticipante.gridheight = 11;
					gbc_panelResumenCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_panelResumenCompeticionParticipante.fill = GridBagConstraints.BOTH;
					gbc_panelResumenCompeticionParticipante.gridx = 8;
					gbc_panelResumenCompeticionParticipante.gridy = 2;
					panelParticipante.add(panelResumenCompeticionParticipante, gbc_panelResumenCompeticionParticipante);
					GridBagLayout gbl_panelResumenCompeticionParticipante = new GridBagLayout();
					gbl_panelResumenCompeticionParticipante.columnWidths = new int[]{0, 0};
					gbl_panelResumenCompeticionParticipante.rowHeights = new int[]{0, 0};
					gbl_panelResumenCompeticionParticipante.columnWeights = new double[]{1.0, Double.MIN_VALUE};
					gbl_panelResumenCompeticionParticipante.rowWeights = new double[]{1.0, Double.MIN_VALUE};
					panelResumenCompeticionParticipante.setLayout(gbl_panelResumenCompeticionParticipante);
					{
						taResumenCompeticionParticipante = new JTextArea();
						taResumenCompeticionParticipante.setEditable(false);
						GridBagConstraints gbc_taResumenCompeticionParticipante = new GridBagConstraints();
						gbc_taResumenCompeticionParticipante.fill = GridBagConstraints.BOTH;
						gbc_taResumenCompeticionParticipante.gridx = 0;
						gbc_taResumenCompeticionParticipante.gridy = 0;
						panelResumenCompeticionParticipante.add(taResumenCompeticionParticipante, gbc_taResumenCompeticionParticipante);
					}
				}
				{
					btnBorrarCompeticionParticipante = new JButton(MessagesIndex.getString("Index.btnBorrarCompeticionParticipante.text")); //$NON-NLS-1$
					btnBorrarCompeticionParticipante.addActionListener(new BtnBorrarCompeticionParticipanteActionListener());
					{
						btnAnadirCompeticionParticipante = new JButton(MessagesIndex.getString("Index.btnAnadirCompeticionParticipante.text")); //$NON-NLS-1$
						btnAnadirCompeticionParticipante.addActionListener(new BtnAnadirCompeticionParticipanteActionListener());
						btnAnadirCompeticionParticipante.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
						GridBagConstraints gbc_btnAnadirCompeticionParticipante = new GridBagConstraints();
						gbc_btnAnadirCompeticionParticipante.gridheight = 3;
						gbc_btnAnadirCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_btnAnadirCompeticionParticipante.gridx = 8;
						gbc_btnAnadirCompeticionParticipante.gridy = 13;
						panelParticipante.add(btnAnadirCompeticionParticipante, gbc_btnAnadirCompeticionParticipante);
					}
					btnBorrarCompeticionParticipante.setFont(new Font("Tahoma", Font.PLAIN, 16)); //$NON-NLS-1$
					GridBagConstraints gbc_btnBorrarCompeticionParticipante = new GridBagConstraints();
					gbc_btnBorrarCompeticionParticipante.fill = GridBagConstraints.HORIZONTAL;
					gbc_btnBorrarCompeticionParticipante.gridheight = 3;
					gbc_btnBorrarCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_btnBorrarCompeticionParticipante.gridx = 10;
					gbc_btnBorrarCompeticionParticipante.gridy = 13;
					panelParticipante.add(btnBorrarCompeticionParticipante, gbc_btnBorrarCompeticionParticipante);
				}
				{
					panelCompeticionParticipante = new JPanel();
					panelCompeticionParticipante.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), MessagesIndex.getString("Index.panelCompeticionParticipante.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));  //$NON-NLS-1$//$NON-NLS-2$
					GridBagConstraints gbc_panelCompeticionParticipante = new GridBagConstraints();
					gbc_panelCompeticionParticipante.gridwidth = 6;
					gbc_panelCompeticionParticipante.gridheight = 3;
					gbc_panelCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_panelCompeticionParticipante.fill = GridBagConstraints.BOTH;
					gbc_panelCompeticionParticipante.gridx = 1;
					gbc_panelCompeticionParticipante.gridy = 13;
					panelParticipante.add(panelCompeticionParticipante, gbc_panelCompeticionParticipante);
					GridBagLayout gbl_panelCompeticionParticipante = new GridBagLayout();
					gbl_panelCompeticionParticipante.columnWidths = new int[]{0, 0};
					gbl_panelCompeticionParticipante.rowHeights = new int[]{0, 0};
					gbl_panelCompeticionParticipante.columnWeights = new double[]{1.0, Double.MIN_VALUE};
					gbl_panelCompeticionParticipante.rowWeights = new double[]{1.0, Double.MIN_VALUE};
					panelCompeticionParticipante.setLayout(gbl_panelCompeticionParticipante);
					{
						spCompeticionParticipante = new JScrollPane();
						GridBagConstraints gbc_spCompeticionParticipante = new GridBagConstraints();
						gbc_spCompeticionParticipante.fill = GridBagConstraints.BOTH;
						gbc_spCompeticionParticipante.gridx = 0;
						gbc_spCompeticionParticipante.gridy = 0;
						panelCompeticionParticipante.add(spCompeticionParticipante, gbc_spCompeticionParticipante);
						{
							listCompeticionParticipante = new JList();
							listCompeticionParticipante.addMouseListener(new ListCompeticionParticipanteMouseListener());
							modeloListaCompeticionParticipante = new DefaultListModel();
							listCompeticionParticipante.setModel(modeloListaCompeticionParticipante);
							listCompeticionParticipante.setSelectedIndex(0);
							listCompeticionParticipante.setCellRenderer(new CompeticionParticipanteCellRenderer());
							listCompeticionParticipante.addListSelectionListener(new mostrarCompeticionParticipanteListSelectionListener());
							spCompeticionParticipante.setViewportView(listCompeticionParticipante);
						}
					}
				}
			}
			{
				panelCompeticion = new JPanel();
				panelCompeticion.setBackground(SystemColor.inactiveCaptionBorder);
				panelContenedor.add(panelCompeticion, "Competicion"); //$NON-NLS-1$
				GridBagLayout gbl_panelCompeticion = new GridBagLayout();
				gbl_panelCompeticion.columnWidths = new int[]{22, 107, 122, 123, 0, 13, 0, 104, 32, 9, 0};
				gbl_panelCompeticion.rowHeights = new int[]{42, 60, 47, 0, 52, 0, 21, 109, 0, 0, 37, 68, 65, 0, 0, 51, 0};
				gbl_panelCompeticion.columnWeights = new double[]{1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
				gbl_panelCompeticion.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				panelCompeticion.setLayout(gbl_panelCompeticion);
				{
					panelDatosCompeticion = new JPanel();
					panelDatosCompeticion.setBackground(SystemColor.controlHighlight);
					panelDatosCompeticion.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					GridBagConstraints gbc_panelDatosCompeticion = new GridBagConstraints();
					gbc_panelDatosCompeticion.gridwidth = 3;
					gbc_panelDatosCompeticion.gridheight = 5;
					gbc_panelDatosCompeticion.insets = new Insets(0, 0, 5, 5);
					gbc_panelDatosCompeticion.fill = GridBagConstraints.BOTH;
					gbc_panelDatosCompeticion.gridx = 1;
					gbc_panelDatosCompeticion.gridy = 1;
					panelCompeticion.add(panelDatosCompeticion, gbc_panelDatosCompeticion);
					GridBagLayout gbl_panelDatosCompeticion = new GridBagLayout();
					gbl_panelDatosCompeticion.columnWidths = new int[]{0, 100, 0, 127, 0, 40, 19, 0, 53, 0, 0};
					gbl_panelDatosCompeticion.rowHeights = new int[]{0, 33, 117, 34, 0, 0, 0, 0, 0, 0, 0, 0};
					gbl_panelDatosCompeticion.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
					gbl_panelDatosCompeticion.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					panelDatosCompeticion.setLayout(gbl_panelDatosCompeticion);
					{
						spCartelCompeticion = new JScrollPane();
						GridBagConstraints gbc_spCartelCompeticion = new GridBagConstraints();
						gbc_spCartelCompeticion.gridheight = 5;
						gbc_spCartelCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_spCartelCompeticion.fill = GridBagConstraints.BOTH;
						gbc_spCartelCompeticion.gridx = 1;
						gbc_spCartelCompeticion.gridy = 2;
						panelDatosCompeticion.add(spCartelCompeticion, gbc_spCartelCompeticion);
						{
							lblCartelCompeticion = new JLabel(""); //$NON-NLS-1$
							lblCartelCompeticion.setHorizontalAlignment(SwingConstants.CENTER);
							lblCartelCompeticion.setIcon(new ImageIcon(Index.class.getResource("/Recursos/logocompeticion.png"))); //$NON-NLS-1$
							spCartelCompeticion.setViewportView(lblCartelCompeticion);
						}
					}
					{
						lblNombreCompeticion = new JLabel(MessagesIndex.getString("Index.lblNombreCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblNombreCompeticion = new GridBagConstraints();
						gbc_lblNombreCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblNombreCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblNombreCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblNombreCompeticion.gridx = 2;
						gbc_lblNombreCompeticion.gridy = 2;
						panelDatosCompeticion.add(lblNombreCompeticion, gbc_lblNombreCompeticion);
					}
					{
						txtNombreCompeticion = new JTextField();
						GridBagConstraints gbc_txtNombreCompeticion = new GridBagConstraints();
						gbc_txtNombreCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtNombreCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtNombreCompeticion.gridx = 3;
						gbc_txtNombreCompeticion.gridy = 2;
						panelDatosCompeticion.add(txtNombreCompeticion, gbc_txtNombreCompeticion);
					}
					{
						lblIdCompeticion = new JLabel(MessagesIndex.getString("Index.lblIdCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblIdCompeticion = new GridBagConstraints();
						gbc_lblIdCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblIdCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblIdCompeticion.gridx = 7;
						gbc_lblIdCompeticion.gridy = 2;
						panelDatosCompeticion.add(lblIdCompeticion, gbc_lblIdCompeticion);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("#####"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtIdCompeticion = new JFormattedTextField(mascara);
						GridBagConstraints gbc_txtIdCompeticion = new GridBagConstraints();
						gbc_txtIdCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtIdCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtIdCompeticion.gridx = 8;
						gbc_txtIdCompeticion.gridy = 2;
						panelDatosCompeticion.add(txtIdCompeticion, gbc_txtIdCompeticion);
						txtIdCompeticion.setColumns(10);
						}
						catch(Exception e){
							
						}
					}
					{
						lblFechaCompeticion = new JLabel(MessagesIndex.getString("Index.lblFechaCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblFechaCompeticion = new GridBagConstraints();
						gbc_lblFechaCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblFechaCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblFechaCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblFechaCompeticion.gridx = 2;
						gbc_lblFechaCompeticion.gridy = 3;
						panelDatosCompeticion.add(lblFechaCompeticion, gbc_lblFechaCompeticion);
					}
					{
						cbFechaDiaCompeticion = new JComboBox();
						cbFechaDiaCompeticion.setModel(new DefaultComboBoxModel(new String[] {" ","01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$ //$NON-NLS-32$
						GridBagConstraints gbc_cbFechaDiaCompeticion = new GridBagConstraints();
						gbc_cbFechaDiaCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbFechaDiaCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaDiaCompeticion.gridx = 3;
						gbc_cbFechaDiaCompeticion.gridy = 3;
						panelDatosCompeticion.add(cbFechaDiaCompeticion, gbc_cbFechaDiaCompeticion);
					}
					{
						cbFechaMesCompeticion = new JComboBox();
						cbFechaMesCompeticion.setModel(new DefaultComboBoxModel(new String[] {" ","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$
						GridBagConstraints gbc_cbFechaMesCompeticion = new GridBagConstraints();
						gbc_cbFechaMesCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbFechaMesCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaMesCompeticion.gridx = 4;
						gbc_cbFechaMesCompeticion.gridy = 3;
						panelDatosCompeticion.add(cbFechaMesCompeticion, gbc_cbFechaMesCompeticion);
					}
					{
						cbFechaAnoCompeticion = new JComboBox();
						cbFechaAnoCompeticion.setModel(new DefaultComboBoxModel(new String[] {" ","2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$
						GridBagConstraints gbc_cbFechaAnoCompeticion = new GridBagConstraints();
						gbc_cbFechaAnoCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbFechaAnoCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaAnoCompeticion.gridx = 5;
						gbc_cbFechaAnoCompeticion.gridy = 3;
						panelDatosCompeticion.add(cbFechaAnoCompeticion, gbc_cbFechaAnoCompeticion);
					}
					{
						lblModalidadCompeticion = new JLabel(MessagesIndex.getString("Index.lblModalidadCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblModalidadCompeticion = new GridBagConstraints();
						gbc_lblModalidadCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblModalidadCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblModalidadCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblModalidadCompeticion.gridx = 7;
						gbc_lblModalidadCompeticion.gridy = 3;
						panelDatosCompeticion.add(lblModalidadCompeticion, gbc_lblModalidadCompeticion);
					}
					{
						cbModalidadCompeticion = new JComboBox();
						cbModalidadCompeticion.setModel(new DefaultComboBoxModel(new String[] {" ","Maratón", "Media Maratón", "Triatlón", "Montaña", "Orientación"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
						GridBagConstraints gbc_cbModalidadCompeticion = new GridBagConstraints();
						gbc_cbModalidadCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbModalidadCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_cbModalidadCompeticion.gridx = 8;
						gbc_cbModalidadCompeticion.gridy = 3;
						panelDatosCompeticion.add(cbModalidadCompeticion, gbc_cbModalidadCompeticion);
					}
					{
						lblLocalidadCompeticion = new JLabel(MessagesIndex.getString("Index.lblLocalidadCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblLocalidadCompeticion = new GridBagConstraints();
						gbc_lblLocalidadCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblLocalidadCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblLocalidadCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblLocalidadCompeticion.gridx = 2;
						gbc_lblLocalidadCompeticion.gridy = 4;
						panelDatosCompeticion.add(lblLocalidadCompeticion, gbc_lblLocalidadCompeticion);
					}
					{
						txtLocalidadCompeticion = new JTextField();
						GridBagConstraints gbc_txtLocalidadCompeticion = new GridBagConstraints();
						gbc_txtLocalidadCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtLocalidadCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtLocalidadCompeticion.gridx = 3;
						gbc_txtLocalidadCompeticion.gridy = 4;
						panelDatosCompeticion.add(txtLocalidadCompeticion, gbc_txtLocalidadCompeticion);
					}
					{
						lblDificultadCompeticion = new JLabel(MessagesIndex.getString("Index.lblDificultadCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblDificultadCompeticion = new GridBagConstraints();
						gbc_lblDificultadCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblDificultadCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblDificultadCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblDificultadCompeticion.gridx = 7;
						gbc_lblDificultadCompeticion.gridy = 4;
						panelDatosCompeticion.add(lblDificultadCompeticion, gbc_lblDificultadCompeticion);
					}
					{
						cbDificultadCompeticion = new JComboBox();
						cbDificultadCompeticion.setModel(new DefaultComboBoxModel(new String[] {" ","Facil", "Intermedia", "Dificil"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						GridBagConstraints gbc_cbDificultadCompeticion = new GridBagConstraints();
						gbc_cbDificultadCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbDificultadCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_cbDificultadCompeticion.gridx = 8;
						gbc_cbDificultadCompeticion.gridy = 4;
						panelDatosCompeticion.add(cbDificultadCompeticion, gbc_cbDificultadCompeticion);
					}
					{
						lblTelefonoCompeticion = new JLabel(MessagesIndex.getString("Index.lblTelefonoCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblTelefonoCompeticion = new GridBagConstraints();
						gbc_lblTelefonoCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblTelefonoCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblTelefonoCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblTelefonoCompeticion.gridx = 2;
						gbc_lblTelefonoCompeticion.gridy = 5;
						panelDatosCompeticion.add(lblTelefonoCompeticion, gbc_lblTelefonoCompeticion);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("#########"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtTelefonoCompeticion= new JFormattedTextField(mascara);
						
						GridBagConstraints gbc_txtTelefonoCompeticion = new GridBagConstraints();
						gbc_txtTelefonoCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtTelefonoCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtTelefonoCompeticion.gridx = 3;
						gbc_txtTelefonoCompeticion.gridy = 5;
						panelDatosCompeticion.add(txtTelefonoCompeticion, gbc_txtTelefonoCompeticion);
						}
						catch(Exception ex){
							
						}
					}
					{
						lblDistanciaCompeticion = new JLabel(MessagesIndex.getString("Index.lblDistanciaCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblDistanciaCompeticion = new GridBagConstraints();
						gbc_lblDistanciaCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblDistanciaCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblDistanciaCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblDistanciaCompeticion.gridx = 7;
						gbc_lblDistanciaCompeticion.gridy = 5;
						panelDatosCompeticion.add(lblDistanciaCompeticion, gbc_lblDistanciaCompeticion);
					}
					{
						try{
							
							txtDescripcionCompeticion = new JFormattedTextField();
							txtDescripcionCompeticion.addKeyListener(new KeyAdapter() {
								@Override
								public void keyTyped(java.awt.event.KeyEvent evt) {
									char c=evt.getKeyChar();
									if(!(Character.isDigit(c) || (c==KeyEvent.VK_BACK_SPACE) || c==KeyEvent.VK_DELETE)){
										evt.consume();
									}
								}
							});
							GridBagConstraints gbc_txtDescripcionCompeticion = new GridBagConstraints();
							gbc_txtDescripcionCompeticion.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtDescripcionCompeticion.insets = new Insets(0, 0, 5, 5);
							gbc_txtDescripcionCompeticion.gridx = 8;
							gbc_txtDescripcionCompeticion.gridy = 5;
							panelDatosCompeticion.add(txtDescripcionCompeticion, gbc_txtDescripcionCompeticion);
						}catch(Exception ex){

						}
					}
					{
						lblCorreoElectronicoCompeticion = new JLabel(MessagesIndex.getString("Index.lblCorreoElectronicoCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblCorreoElectronicoCompeticion = new GridBagConstraints();
						gbc_lblCorreoElectronicoCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblCorreoElectronicoCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblCorreoElectronicoCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblCorreoElectronicoCompeticion.gridx = 2;
						gbc_lblCorreoElectronicoCompeticion.gridy = 6;
						panelDatosCompeticion.add(lblCorreoElectronicoCompeticion, gbc_lblCorreoElectronicoCompeticion);
					}
					{
						txtCorreoElectronicoCompeticion = new JTextField();
						GridBagConstraints gbc_txtCorreoElectronicoCompeticion = new GridBagConstraints();
						gbc_txtCorreoElectronicoCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtCorreoElectronicoCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtCorreoElectronicoCompeticion.gridx = 3;
						gbc_txtCorreoElectronicoCompeticion.gridy = 6;
						panelDatosCompeticion.add(txtCorreoElectronicoCompeticion, gbc_txtCorreoElectronicoCompeticion);
					}
					{
						lblDescripcionCompeticion = new JLabel(MessagesIndex.getString("Index.lblDescripcionCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblDescripcionCompeticion = new GridBagConstraints();
						gbc_lblDescripcionCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblDescripcionCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblDescripcionCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblDescripcionCompeticion.gridx = 7;
						gbc_lblDescripcionCompeticion.gridy = 6;
						panelDatosCompeticion.add(lblDescripcionCompeticion, gbc_lblDescripcionCompeticion);
					}
					{
						txtDistanciaCompeticion = new JTextField();
						GridBagConstraints gbc_txtDistanciaCompeticion = new GridBagConstraints();
						gbc_txtDistanciaCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtDistanciaCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtDistanciaCompeticion.gridx = 8;
						gbc_txtDistanciaCompeticion.gridy = 6;
						panelDatosCompeticion.add(txtDistanciaCompeticion, gbc_txtDistanciaCompeticion);
					}
					{
						lblWebCompeticion = new JLabel(MessagesIndex.getString("Index.lblWebCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblWebCompeticion = new GridBagConstraints();
						gbc_lblWebCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblWebCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblWebCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblWebCompeticion.gridx = 2;
						gbc_lblWebCompeticion.gridy = 7;
						panelDatosCompeticion.add(lblWebCompeticion, gbc_lblWebCompeticion);
					}
					{
						txtWebCompeticion = new JTextField();
						GridBagConstraints gbc_txtWebCompeticion = new GridBagConstraints();
						gbc_txtWebCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtWebCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtWebCompeticion.gridx = 3;
						gbc_txtWebCompeticion.gridy = 7;
						panelDatosCompeticion.add(txtWebCompeticion, gbc_txtWebCompeticion);
					}
					{
						lblEstadoCompeticion = new JLabel(MessagesIndex.getString("Index.lblEstadoCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblEstadoCompeticion = new GridBagConstraints();
						gbc_lblEstadoCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblEstadoCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblEstadoCompeticion.gridx = 7;
						gbc_lblEstadoCompeticion.gridy = 7;
						panelDatosCompeticion.add(lblEstadoCompeticion, gbc_lblEstadoCompeticion);
					}
					{
						cbEstadoCompeticion = new JComboBox();
						cbEstadoCompeticion.setModel(new DefaultComboBoxModel(new String[] {" ","En curso", "Finalizada"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						GridBagConstraints gbc_cbEstadoCompeticion = new GridBagConstraints();
						gbc_cbEstadoCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_cbEstadoCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbEstadoCompeticion.gridx = 8;
						gbc_cbEstadoCompeticion.gridy = 7;
						panelDatosCompeticion.add(cbEstadoCompeticion, gbc_cbEstadoCompeticion);
						
					}
					{
						btnVerInformacionCompeticion = new JButton(MessagesIndex.getString("Index.btnVerInformacionCompeticion.text")); //$NON-NLS-1$
						btnVerInformacionCompeticion.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						btnVerInformacionCompeticion.addActionListener(new BtnVerInformacionCompeticionActionListener());
						GridBagConstraints gbc_btnVerInformacionCompeticion = new GridBagConstraints();
						gbc_btnVerInformacionCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_btnVerInformacionCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_btnVerInformacionCompeticion.gridx = 8;
						gbc_btnVerInformacionCompeticion.gridy = 8;
						panelDatosCompeticion.add(btnVerInformacionCompeticion, gbc_btnVerInformacionCompeticion);
					}
					{
						btnGuardarCompeticionCompeticion = new JButton(MessagesIndex.getString("Index.btnGuardarCompeticionCompeticion.text")); //$NON-NLS-1$
						btnGuardarCompeticionCompeticion.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_btnGuardarCompeticionCompeticion = new GridBagConstraints();
						gbc_btnGuardarCompeticionCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_btnGuardarCompeticionCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_btnGuardarCompeticionCompeticion.gridx = 1;
						gbc_btnGuardarCompeticionCompeticion.gridy = 9;
						panelDatosCompeticion.add(btnGuardarCompeticionCompeticion, gbc_btnGuardarCompeticionCompeticion);
						btnGuardarCompeticionCompeticion.addActionListener(new BtnGuardarCompeticionCompeticionActionListener());
						{
							btnModificarCompeticionCompeticion = new JButton(MessagesIndex.getString("Index.btnModificarCompeticionCompeticion.text")); //$NON-NLS-1$
							btnModificarCompeticionCompeticion.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
							GridBagConstraints gbc_btnModificarCompeticionCompeticion = new GridBagConstraints();
							gbc_btnModificarCompeticionCompeticion.fill = GridBagConstraints.HORIZONTAL;
							gbc_btnModificarCompeticionCompeticion.insets = new Insets(0, 0, 5, 5);
							gbc_btnModificarCompeticionCompeticion.gridx = 2;
							gbc_btnModificarCompeticionCompeticion.gridy = 9;
							panelDatosCompeticion.add(btnModificarCompeticionCompeticion, gbc_btnModificarCompeticionCompeticion);
							{
								btnEliminarCompeticionCompeticion = new JButton(MessagesIndex.getString("Index.btnEliminarCompeticionCompeticion.text")); //$NON-NLS-1$
								btnEliminarCompeticionCompeticion.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
								GridBagConstraints gbc_btnEliminarCompeticionCompeticion = new GridBagConstraints();
								gbc_btnEliminarCompeticionCompeticion.fill = GridBagConstraints.HORIZONTAL;
								gbc_btnEliminarCompeticionCompeticion.insets = new Insets(0, 0, 5, 5);
								gbc_btnEliminarCompeticionCompeticion.gridx = 3;
								gbc_btnEliminarCompeticionCompeticion.gridy = 9;
								panelDatosCompeticion.add(btnEliminarCompeticionCompeticion, gbc_btnEliminarCompeticionCompeticion);
								btnEliminarCompeticionCompeticion.addActionListener(new BtnEliminarCompeticionCompeticionActionListener());
							}
							btnModificarCompeticionCompeticion.addActionListener(new BtnModificarCompeticionCompeticionActionListener());
						}
					}
				}
				{
					panelInscritosCompeticion = new JPanel();
					panelInscritosCompeticion.setBackground(SystemColor.controlHighlight);
					panelInscritosCompeticion.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					GridBagConstraints gbc_panelInscritosCompeticion = new GridBagConstraints();
					gbc_panelInscritosCompeticion.gridheight = 3;
					gbc_panelInscritosCompeticion.gridwidth = 3;
					gbc_panelInscritosCompeticion.insets = new Insets(0, 0, 5, 5);
					gbc_panelInscritosCompeticion.fill = GridBagConstraints.BOTH;
					gbc_panelInscritosCompeticion.gridx = 6;
					gbc_panelInscritosCompeticion.gridy = 1;
					panelCompeticion.add(panelInscritosCompeticion, gbc_panelInscritosCompeticion);
					GridBagLayout gbl_panelInscritosCompeticion = new GridBagLayout();
					gbl_panelInscritosCompeticion.columnWidths = new int[]{0, 0, 48, 0, 96, 55, 0, 0};
					gbl_panelInscritosCompeticion.rowHeights = new int[]{41, 29, 29, 37, 0};
					gbl_panelInscritosCompeticion.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					gbl_panelInscritosCompeticion.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					panelInscritosCompeticion.setLayout(gbl_panelInscritosCompeticion);
					{
						lblLimiteInscritosCompeticion = new JLabel(MessagesIndex.getString("Index.lblLimiteInscritosCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblLimiteInscritosCompeticion = new GridBagConstraints();
						gbc_lblLimiteInscritosCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblLimiteInscritosCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblLimiteInscritosCompeticion.gridx = 2;
						gbc_lblLimiteInscritosCompeticion.gridy = 1;
						panelInscritosCompeticion.add(lblLimiteInscritosCompeticion, gbc_lblLimiteInscritosCompeticion);
					}
					{
						try{
							
							txtLimiteInscritosCompeticion = new JFormattedTextField();
							txtLimiteInscritosCompeticion.addKeyListener(new KeyAdapter() {
								@Override
								public void keyTyped(java.awt.event.KeyEvent evt) {
									char c=evt.getKeyChar();
									if(!(Character.isDigit(c) || (c==KeyEvent.VK_BACK_SPACE) || c==KeyEvent.VK_DELETE)){
										evt.consume();
									}
								}
							});
							GridBagConstraints gbc_txtLimiteInscritosCompeticion = new GridBagConstraints();
							gbc_txtLimiteInscritosCompeticion.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtLimiteInscritosCompeticion.insets = new Insets(0, 0, 5, 5);
							gbc_txtLimiteInscritosCompeticion.gridx = 3;
							gbc_txtLimiteInscritosCompeticion.gridy = 1;
							panelInscritosCompeticion.add(txtLimiteInscritosCompeticion, gbc_txtLimiteInscritosCompeticion);
						}catch(Exception ex){

						}
					}
					{
						lblPrecioInscripcionCompeticion = new JLabel(MessagesIndex.getString("Index.lblPrecioInscripcionCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblPrecioInscripcionCompeticion = new GridBagConstraints();
						gbc_lblPrecioInscripcionCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblPrecioInscripcionCompeticion.anchor = GridBagConstraints.EAST;
						gbc_lblPrecioInscripcionCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblPrecioInscripcionCompeticion.gridx = 4;
						gbc_lblPrecioInscripcionCompeticion.gridy = 1;
						panelInscritosCompeticion.add(lblPrecioInscripcionCompeticion, gbc_lblPrecioInscripcionCompeticion);
					}
					{
						try{
								
							txtPrecioInscripcionCompeticion = new JFormattedTextField();
							txtPrecioInscripcionCompeticion.addKeyListener(new KeyAdapter() {
								@Override
								public void keyTyped(java.awt.event.KeyEvent evt) {
									char c=evt.getKeyChar();
									if(!(Character.isDigit(c) || (c==KeyEvent.VK_BACK_SPACE) || c==KeyEvent.VK_DELETE)){
										evt.consume();
									}
								}
							});
							GridBagConstraints gbc_txtPrecioInscripcionCompeticion = new GridBagConstraints();
							gbc_txtPrecioInscripcionCompeticion.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtPrecioInscripcionCompeticion.insets = new Insets(0, 0, 5, 5);
							gbc_txtPrecioInscripcionCompeticion.gridx = 5;
							gbc_txtPrecioInscripcionCompeticion.gridy = 1;
							panelInscritosCompeticion.add(txtPrecioInscripcionCompeticion, gbc_txtPrecioInscripcionCompeticion);
						}catch(Exception ex){

						}
					}
					{
						lblFechaLimiteInscritosCompeticion = new JLabel(MessagesIndex.getString("Index.lblFechaLimiteInscritosCompeticion.text")); //$NON-NLS-1$
						lblFechaLimiteInscritosCompeticion.setFont(new Font("Tahoma", Font.PLAIN, 11)); //$NON-NLS-1$
						GridBagConstraints gbc_lblFechaLimiteInscritosCompeticion = new GridBagConstraints();
						gbc_lblFechaLimiteInscritosCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblFechaLimiteInscritosCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblFechaLimiteInscritosCompeticion.gridx = 2;
						gbc_lblFechaLimiteInscritosCompeticion.gridy = 2;
						panelInscritosCompeticion.add(lblFechaLimiteInscritosCompeticion, gbc_lblFechaLimiteInscritosCompeticion);
					}
					{
						cbFechaLimiteInscripcionDiaCompeticion = new JComboBox();
						cbFechaLimiteInscripcionDiaCompeticion.setModel(new DefaultComboBoxModel(new String[] {" ","01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$ //$NON-NLS-32$
						GridBagConstraints gbc_cbFechaLimiteInscripcionDiaCompeticion = new GridBagConstraints();
						gbc_cbFechaLimiteInscripcionDiaCompeticion.fill = GridBagConstraints.HORIZONTAL;
						gbc_cbFechaLimiteInscripcionDiaCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaLimiteInscripcionDiaCompeticion.gridx = 3;
						gbc_cbFechaLimiteInscripcionDiaCompeticion.gridy = 2;
						panelInscritosCompeticion.add(cbFechaLimiteInscripcionDiaCompeticion, gbc_cbFechaLimiteInscripcionDiaCompeticion);
					}
					{
						cbFechaLimiteInscripcionMesCompeticion = new JComboBox();
						cbFechaLimiteInscripcionMesCompeticion.setModel(new DefaultComboBoxModel(new String[] {" ","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$
						GridBagConstraints gbc_cbFechaLimiteInscripcionMesCompeticion = new GridBagConstraints();
						gbc_cbFechaLimiteInscripcionMesCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaLimiteInscripcionMesCompeticion.gridx = 4;
						gbc_cbFechaLimiteInscripcionMesCompeticion.gridy = 2;
						panelInscritosCompeticion.add(cbFechaLimiteInscripcionMesCompeticion, gbc_cbFechaLimiteInscripcionMesCompeticion);
					}
					{
						cbFechaLimiteInscripcionAnoCompeticion = new JComboBox();
						cbFechaLimiteInscripcionAnoCompeticion.setModel(new DefaultComboBoxModel(new String[] {" ","2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$
						GridBagConstraints gbc_cbFechaLimiteInscripcionAnoCompeticion = new GridBagConstraints();
						gbc_cbFechaLimiteInscripcionAnoCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaLimiteInscripcionAnoCompeticion.gridx = 5;
						gbc_cbFechaLimiteInscripcionAnoCompeticion.gridy = 2;
						panelInscritosCompeticion.add(cbFechaLimiteInscripcionAnoCompeticion, gbc_cbFechaLimiteInscripcionAnoCompeticion);
					}
				}
				{
					panelInformacionGeneralCompeticion = new JPanel();
					panelInformacionGeneralCompeticion.setBackground(SystemColor.controlHighlight);
					panelInformacionGeneralCompeticion.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					GridBagConstraints gbc_panelInformacionGeneralCompeticion = new GridBagConstraints();
					gbc_panelInformacionGeneralCompeticion.gridheight = 2;
					gbc_panelInformacionGeneralCompeticion.gridwidth = 3;
					gbc_panelInformacionGeneralCompeticion.insets = new Insets(0, 0, 5, 5);
					gbc_panelInformacionGeneralCompeticion.fill = GridBagConstraints.BOTH;
					gbc_panelInformacionGeneralCompeticion.gridx = 6;
					gbc_panelInformacionGeneralCompeticion.gridy = 4;
					panelCompeticion.add(panelInformacionGeneralCompeticion, gbc_panelInformacionGeneralCompeticion);
					GridBagLayout gbl_panelInformacionGeneralCompeticion = new GridBagLayout();
					gbl_panelInformacionGeneralCompeticion.columnWidths = new int[]{0, 342, 0};
					gbl_panelInformacionGeneralCompeticion.rowHeights = new int[]{0, 107, 0, 0};
					gbl_panelInformacionGeneralCompeticion.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
					gbl_panelInformacionGeneralCompeticion.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
					panelInformacionGeneralCompeticion.setLayout(gbl_panelInformacionGeneralCompeticion);
					{
						spInformacionGeneralCompeticion = new JScrollPane();
						GridBagConstraints gbc_spInformacionGeneralCompeticion = new GridBagConstraints();
						gbc_spInformacionGeneralCompeticion.fill = GridBagConstraints.BOTH;
						gbc_spInformacionGeneralCompeticion.insets = new Insets(0, 0, 5, 0);
						gbc_spInformacionGeneralCompeticion.gridx = 1;
						gbc_spInformacionGeneralCompeticion.gridy = 1;
						panelInformacionGeneralCompeticion.add(spInformacionGeneralCompeticion, gbc_spInformacionGeneralCompeticion);
						{
							taInformacionGeneralCompeticion = new JTextArea();
							spInformacionGeneralCompeticion.setViewportView(taInformacionGeneralCompeticion);
						}
					}
				}
				{
					panelEnlacesInteresCompeticion = new JPanel();
					panelEnlacesInteresCompeticion.setBackground(SystemColor.controlHighlight);
					panelEnlacesInteresCompeticion.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					GridBagConstraints gbc_panelEnlacesInteresCompeticion = new GridBagConstraints();
					gbc_panelEnlacesInteresCompeticion.fill = GridBagConstraints.BOTH;
					gbc_panelEnlacesInteresCompeticion.gridwidth = 3;
					gbc_panelEnlacesInteresCompeticion.gridheight = 3;
					gbc_panelEnlacesInteresCompeticion.insets = new Insets(0, 0, 5, 5);
					gbc_panelEnlacesInteresCompeticion.gridx = 1;
					gbc_panelEnlacesInteresCompeticion.gridy = 6;
					panelCompeticion.add(panelEnlacesInteresCompeticion, gbc_panelEnlacesInteresCompeticion);
					GridBagLayout gbl_panelEnlacesInteresCompeticion = new GridBagLayout();
					gbl_panelEnlacesInteresCompeticion.columnWidths = new int[]{0, 627, 0};
					gbl_panelEnlacesInteresCompeticion.rowHeights = new int[]{0, 0, 0, 0, 0};
					gbl_panelEnlacesInteresCompeticion.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
					gbl_panelEnlacesInteresCompeticion.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					panelEnlacesInteresCompeticion.setLayout(gbl_panelEnlacesInteresCompeticion);
					{
						spEnlacesInteresCompeticion = new JScrollPane();
						GridBagConstraints gbc_spEnlacesInteresCompeticion = new GridBagConstraints();
						gbc_spEnlacesInteresCompeticion.gridwidth = 2;
						gbc_spEnlacesInteresCompeticion.gridheight = 4;
						gbc_spEnlacesInteresCompeticion.fill = GridBagConstraints.BOTH;
						gbc_spEnlacesInteresCompeticion.gridx = 0;
						gbc_spEnlacesInteresCompeticion.gridy = 0;
						panelEnlacesInteresCompeticion.add(spEnlacesInteresCompeticion, gbc_spEnlacesInteresCompeticion);
						{
							taEnlacesInteresCompeticion = new JTextArea();
							spEnlacesInteresCompeticion.setViewportView(taEnlacesInteresCompeticion);
						}
					}
				}
				{
					panelMapaCompeticion = new JPanel();
					panelMapaCompeticion.setBackground(SystemColor.controlHighlight);
					panelMapaCompeticion.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					GridBagConstraints gbc_panelMapaCompeticion = new GridBagConstraints();
					gbc_panelMapaCompeticion.gridwidth = 3;
					gbc_panelMapaCompeticion.gridheight = 8;
					gbc_panelMapaCompeticion.insets = new Insets(0, 0, 5, 5);
					gbc_panelMapaCompeticion.fill = GridBagConstraints.BOTH;
					gbc_panelMapaCompeticion.gridx = 6;
					gbc_panelMapaCompeticion.gridy = 7;
					panelCompeticion.add(panelMapaCompeticion, gbc_panelMapaCompeticion);
					GridBagLayout gbl_panelMapaCompeticion = new GridBagLayout();
					gbl_panelMapaCompeticion.columnWidths = new int[]{0, 277, 70, 0};
					gbl_panelMapaCompeticion.rowHeights = new int[]{0, 196, 0, 0};
					gbl_panelMapaCompeticion.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
					gbl_panelMapaCompeticion.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
					panelMapaCompeticion.setLayout(gbl_panelMapaCompeticion);
					{
						spMapaCarreraCompeticion = new JScrollPane();
						GridBagConstraints gbc_spMapaCarreraCompeticion = new GridBagConstraints();
						gbc_spMapaCarreraCompeticion.gridheight = 2;
						gbc_spMapaCarreraCompeticion.gridwidth = 2;
						gbc_spMapaCarreraCompeticion.fill = GridBagConstraints.BOTH;
						gbc_spMapaCarreraCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_spMapaCarreraCompeticion.gridx = 1;
						gbc_spMapaCarreraCompeticion.gridy = 1;
						panelMapaCompeticion.add(spMapaCarreraCompeticion, gbc_spMapaCarreraCompeticion);
						{
							lblMapaImagenCompeticion = new JLabel(""); //$NON-NLS-1$
							lblMapaImagenCompeticion.setIcon(new ImageIcon(Index.class.getResource("/Recursos/mapa.png"))); //$NON-NLS-1$
							spMapaCarreraCompeticion.setViewportView(lblMapaImagenCompeticion);
						}
					}
				}
				{
					panelLogosCompeticion = new JPanel();
					panelLogosCompeticion.setBackground(SystemColor.controlHighlight);
					panelLogosCompeticion.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					GridBagConstraints gbc_panelLogosCompeticion = new GridBagConstraints();
					gbc_panelLogosCompeticion.gridwidth = 3;
					gbc_panelLogosCompeticion.gridheight = 7;
					gbc_panelLogosCompeticion.insets = new Insets(0, 0, 0, 5);
					gbc_panelLogosCompeticion.fill = GridBagConstraints.BOTH;
					gbc_panelLogosCompeticion.gridx = 1;
					gbc_panelLogosCompeticion.gridy = 9;
					panelCompeticion.add(panelLogosCompeticion, gbc_panelLogosCompeticion);
					GridBagLayout gbl_panelLogosCompeticion = new GridBagLayout();
					gbl_panelLogosCompeticion.columnWidths = new int[]{0, 207, 165, 254, 0, 0};
					gbl_panelLogosCompeticion.rowHeights = new int[]{0, 88, 0, 0, 0};
					gbl_panelLogosCompeticion.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
					gbl_panelLogosCompeticion.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
					panelLogosCompeticion.setLayout(gbl_panelLogosCompeticion);
					{
						spLogoEntidadCompeticion = new JScrollPane();
						GridBagConstraints gbc_spLogoEntidadCompeticion = new GridBagConstraints();
						gbc_spLogoEntidadCompeticion.gridheight = 3;
						gbc_spLogoEntidadCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_spLogoEntidadCompeticion.fill = GridBagConstraints.BOTH;
						gbc_spLogoEntidadCompeticion.gridx = 1;
						gbc_spLogoEntidadCompeticion.gridy = 1;
						panelLogosCompeticion.add(spLogoEntidadCompeticion, gbc_spLogoEntidadCompeticion);
						{
							lblLogoEntidadCompeticion = new JLabel(""); //$NON-NLS-1$
							lblLogoEntidadCompeticion.setHorizontalAlignment(SwingConstants.CENTER);
							lblLogoEntidadCompeticion.setIcon(new ImageIcon(Index.class.getResource("/Recursos/logoentidad.png"))); //$NON-NLS-1$
							spLogoEntidadCompeticion.setViewportView(lblLogoEntidadCompeticion);
						}
					}
					{
						spLogoEntidadesCooperativasCompeticion = new JScrollPane();
						GridBagConstraints gbc_spLogoEntidadesCooperativasCompeticion = new GridBagConstraints();
						gbc_spLogoEntidadesCooperativasCompeticion.gridheight = 3;
						gbc_spLogoEntidadesCooperativasCompeticion.fill = GridBagConstraints.BOTH;
						gbc_spLogoEntidadesCooperativasCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_spLogoEntidadesCooperativasCompeticion.gridx = 3;
						gbc_spLogoEntidadesCooperativasCompeticion.gridy = 1;
						panelLogosCompeticion.add(spLogoEntidadesCooperativasCompeticion, gbc_spLogoEntidadesCooperativasCompeticion);
						{
							lblLogoEntidadesCooperativas = new JLabel(""); //$NON-NLS-1$
							lblLogoEntidadesCooperativas.setHorizontalAlignment(SwingConstants.CENTER);
							lblLogoEntidadesCooperativas.setIcon(new ImageIcon(Index.class.getResource("/Recursos/logoentidadcoop.png"))); //$NON-NLS-1$
							spLogoEntidadesCooperativasCompeticion.setViewportView(lblLogoEntidadesCooperativas);
						}
					}
				}
				{
					btnEditarImagen = new JButton(MessagesIndex.getString("Index.btnEditarImagen.text")); //$NON-NLS-1$
					btnEditarImagen.addActionListener(new BtnEditarImagenActionListener());
					GridBagConstraints gbc_btnEditarImagen = new GridBagConstraints();
					gbc_btnEditarImagen.fill = GridBagConstraints.HORIZONTAL;
					gbc_btnEditarImagen.insets = new Insets(0, 0, 0, 5);
					gbc_btnEditarImagen.gridx = 6;
					gbc_btnEditarImagen.gridy = 15;
					panelCompeticion.add(btnEditarImagen, gbc_btnEditarImagen);
				}
			}
			{
				panelPodioCompeticion = new JPanel();
				panelContenedor.add(panelPodioCompeticion, "PodioCompeticion"); //$NON-NLS-1$
				panelPodioCompeticion.setLayout(null);
				{
					panelParticipantesPodioCompeticion = new JPanel();
					panelParticipantesPodioCompeticion.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					panelParticipantesPodioCompeticion.setBackground(SystemColor.controlHighlight);
					panelParticipantesPodioCompeticion.setBounds(888, 325, 323, 210);
					panelPodioCompeticion.add(panelParticipantesPodioCompeticion);
					GridBagLayout gbl_panelParticipantesPodioCompeticion = new GridBagLayout();
					gbl_panelParticipantesPodioCompeticion.columnWidths = new int[]{0, 0, 0, 0};
					gbl_panelParticipantesPodioCompeticion.rowHeights = new int[]{0, 0, 0, 0};
					gbl_panelParticipantesPodioCompeticion.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
					gbl_panelParticipantesPodioCompeticion.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
					panelParticipantesPodioCompeticion.setLayout(gbl_panelParticipantesPodioCompeticion);
					{
						spParticipantesPodioCompeticion = new JScrollPane();
						GridBagConstraints gbc_spParticipantesPodioCompeticion = new GridBagConstraints();
						gbc_spParticipantesPodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_spParticipantesPodioCompeticion.fill = GridBagConstraints.BOTH;
						gbc_spParticipantesPodioCompeticion.gridx = 1;
						gbc_spParticipantesPodioCompeticion.gridy = 1;
						panelParticipantesPodioCompeticion.add(spParticipantesPodioCompeticion, gbc_spParticipantesPodioCompeticion);
						{
							listParticipantesPodioCompeticion = new JList();
							listParticipantesPodioCompeticion.setBackground(Color.WHITE);
							listParticipantesPodioCompeticion.addListSelectionListener(new ListParticipantesPodioCompeticionListSelectionListener());
							listParticipantesPodioCompeticion.addMouseListener(new ListParticipantesPodioCompeticionMouseListener());
							modeloListaParticipantePodioCompeticion = new DefaultListModel();
							listParticipantesPodioCompeticion.setModel(modeloListaParticipantePodioCompeticion);
							listParticipantesPodioCompeticion.setSelectedIndex(0);
							listParticipantesPodioCompeticion.setCellRenderer(new ParticipantePodioCompeticionCellRenderer());
							spParticipantesPodioCompeticion.setViewportView(listParticipantesPodioCompeticion);
						}
					}
				}
				{
					panelTiemposPodioCompeticion = new JPanel();
					panelTiemposPodioCompeticion.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					panelTiemposPodioCompeticion.setBackground(SystemColor.controlHighlight);
					panelTiemposPodioCompeticion.setBounds(900, 99, 290, 159);
					panelPodioCompeticion.add(panelTiemposPodioCompeticion);
					GridBagLayout gbl_panelTiemposPodioCompeticion = new GridBagLayout();
					gbl_panelTiemposPodioCompeticion.columnWidths = new int[]{0, 106, 0, 0, 0};
					gbl_panelTiemposPodioCompeticion.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
					gbl_panelTiemposPodioCompeticion.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
					gbl_panelTiemposPodioCompeticion.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					panelTiemposPodioCompeticion.setLayout(gbl_panelTiemposPodioCompeticion);
					{
						lblTiempoTotalPodioCompeticion = new JLabel(MessagesIndex.getString("Index.lblTiempoTotalPodioCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblTiempoTotalPodioCompeticion = new GridBagConstraints();
						gbc_lblTiempoTotalPodioCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblTiempoTotalPodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblTiempoTotalPodioCompeticion.gridx = 1;
						gbc_lblTiempoTotalPodioCompeticion.gridy = 0;
						panelTiemposPodioCompeticion.add(lblTiempoTotalPodioCompeticion, gbc_lblTiempoTotalPodioCompeticion);
					}
					{
						txtTiempoTotalPodioCompeticion = new JTextField();
						txtTiempoTotalPodioCompeticion.setEditable(false);
						GridBagConstraints gbc_txtTiempoTotalPodioCompeticion = new GridBagConstraints();
						gbc_txtTiempoTotalPodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtTiempoTotalPodioCompeticion.fill = GridBagConstraints.BOTH;
						gbc_txtTiempoTotalPodioCompeticion.gridx = 2;
						gbc_txtTiempoTotalPodioCompeticion.gridy = 0;
						panelTiemposPodioCompeticion.add(txtTiempoTotalPodioCompeticion, gbc_txtTiempoTotalPodioCompeticion);
						txtTiempoTotalPodioCompeticion.setColumns(10);
					}
					{
						lblSeccion1PodioCompeticion = new JLabel(MessagesIndex.getString("Index.lblSeccion1PodioCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion1PodioCompeticion = new GridBagConstraints();
						gbc_lblSeccion1PodioCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblSeccion1PodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblSeccion1PodioCompeticion.gridx = 1;
						gbc_lblSeccion1PodioCompeticion.gridy = 1;
						panelTiemposPodioCompeticion.add(lblSeccion1PodioCompeticion, gbc_lblSeccion1PodioCompeticion);
					}
					{
						txtSeccion1PodioCompeticion = new JTextField();
						txtSeccion1PodioCompeticion.setEditable(false);
						GridBagConstraints gbc_txtSeccion1PodioCompeticion = new GridBagConstraints();
						gbc_txtSeccion1PodioCompeticion.fill = GridBagConstraints.BOTH;
						gbc_txtSeccion1PodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtSeccion1PodioCompeticion.gridx = 2;
						gbc_txtSeccion1PodioCompeticion.gridy = 1;
						panelTiemposPodioCompeticion.add(txtSeccion1PodioCompeticion, gbc_txtSeccion1PodioCompeticion);
						txtSeccion1PodioCompeticion.setColumns(10);
					}
					{
						lblSeccion2PodioCompeticion = new JLabel(MessagesIndex.getString("Index.lblSeccion2PodioCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion2PodioCompeticion = new GridBagConstraints();
						gbc_lblSeccion2PodioCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblSeccion2PodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblSeccion2PodioCompeticion.gridx = 1;
						gbc_lblSeccion2PodioCompeticion.gridy = 2;
						panelTiemposPodioCompeticion.add(lblSeccion2PodioCompeticion, gbc_lblSeccion2PodioCompeticion);
					}
					{
						txtSeccion2PodioCompeticion = new JTextField();
						txtSeccion2PodioCompeticion.setEditable(false);
						GridBagConstraints gbc_txtSeccion2PodioCompeticion = new GridBagConstraints();
						gbc_txtSeccion2PodioCompeticion.fill = GridBagConstraints.BOTH;
						gbc_txtSeccion2PodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtSeccion2PodioCompeticion.gridx = 2;
						gbc_txtSeccion2PodioCompeticion.gridy = 2;
						panelTiemposPodioCompeticion.add(txtSeccion2PodioCompeticion, gbc_txtSeccion2PodioCompeticion);
						txtSeccion2PodioCompeticion.setColumns(10);
					}
					{
						lblSeccion3PodioCompeticion = new JLabel(MessagesIndex.getString("Index.lblSeccion3PodioCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion3PodioCompeticion = new GridBagConstraints();
						gbc_lblSeccion3PodioCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblSeccion3PodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblSeccion3PodioCompeticion.gridx = 1;
						gbc_lblSeccion3PodioCompeticion.gridy = 3;
						panelTiemposPodioCompeticion.add(lblSeccion3PodioCompeticion, gbc_lblSeccion3PodioCompeticion);
					}
					{
						txtSeccion3PodioCompeticion = new JTextField();
						txtSeccion3PodioCompeticion.setEditable(false);
						GridBagConstraints gbc_txtSeccion3PodioCompeticion = new GridBagConstraints();
						gbc_txtSeccion3PodioCompeticion.fill = GridBagConstraints.BOTH;
						gbc_txtSeccion3PodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtSeccion3PodioCompeticion.gridx = 2;
						gbc_txtSeccion3PodioCompeticion.gridy = 3;
						panelTiemposPodioCompeticion.add(txtSeccion3PodioCompeticion, gbc_txtSeccion3PodioCompeticion);
						txtSeccion3PodioCompeticion.setColumns(10);
					}
					{
						lblSeccion4PodioCompeticion = new JLabel(MessagesIndex.getString("Index.lblSeccion4PodioCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion4PodioCompeticion = new GridBagConstraints();
						gbc_lblSeccion4PodioCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblSeccion4PodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_lblSeccion4PodioCompeticion.gridx = 1;
						gbc_lblSeccion4PodioCompeticion.gridy = 4;
						panelTiemposPodioCompeticion.add(lblSeccion4PodioCompeticion, gbc_lblSeccion4PodioCompeticion);
					}
					{
						txtSeccion4PodioCompeticion = new JTextField();
						txtSeccion4PodioCompeticion.setEditable(false);
						GridBagConstraints gbc_txtSeccion4PodioCompeticion = new GridBagConstraints();
						gbc_txtSeccion4PodioCompeticion.fill = GridBagConstraints.BOTH;
						gbc_txtSeccion4PodioCompeticion.insets = new Insets(0, 0, 5, 5);
						gbc_txtSeccion4PodioCompeticion.gridx = 2;
						gbc_txtSeccion4PodioCompeticion.gridy = 4;
						panelTiemposPodioCompeticion.add(txtSeccion4PodioCompeticion, gbc_txtSeccion4PodioCompeticion);
						txtSeccion4PodioCompeticion.setColumns(10);
					}
					{
						lblSeccion5PodioCompeticion = new JLabel(MessagesIndex.getString("Index.lblSeccion5PodioCompeticion.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion5PodioCompeticion = new GridBagConstraints();
						gbc_lblSeccion5PodioCompeticion.fill = GridBagConstraints.VERTICAL;
						gbc_lblSeccion5PodioCompeticion.insets = new Insets(0, 0, 0, 5);
						gbc_lblSeccion5PodioCompeticion.gridx = 1;
						gbc_lblSeccion5PodioCompeticion.gridy = 5;
						panelTiemposPodioCompeticion.add(lblSeccion5PodioCompeticion, gbc_lblSeccion5PodioCompeticion);
					}
					{
						txtSeccion5PodioCompeticion = new JTextField();
						txtSeccion5PodioCompeticion.setEditable(false);
						GridBagConstraints gbc_txtSeccion5PodioCompeticion = new GridBagConstraints();
						gbc_txtSeccion5PodioCompeticion.fill = GridBagConstraints.BOTH;
						gbc_txtSeccion5PodioCompeticion.insets = new Insets(0, 0, 0, 5);
						gbc_txtSeccion5PodioCompeticion.gridx = 2;
						gbc_txtSeccion5PodioCompeticion.gridy = 5;
						panelTiemposPodioCompeticion.add(txtSeccion5PodioCompeticion, gbc_txtSeccion5PodioCompeticion);
						txtSeccion5PodioCompeticion.setColumns(10);
					}
				}
				{
					panel = new JPanel();
					panel.setBackground(SystemColor.controlHighlight);
					panel.setBorder(UIManager.getBorder("Button.border")); //$NON-NLS-1$
					panel.setBounds(64, 62, 729, 507);
					panelPodioCompeticion.add(panel);
					panel.setLayout(null);
					
					lblPodio = new JLabel(""); //$NON-NLS-1$
					lblPodio.setHorizontalAlignment(SwingConstants.CENTER);
					lblPodio.setBounds(6, 113, 717, 342);
					panel.add(lblPodio);
					lblPodio.setIcon(new ImageIcon(Index.class.getResource("/Recursos/podio.png"))); //$NON-NLS-1$
					
					lblClasificacion = new JLabel(""); //$NON-NLS-1$
					lblClasificacion.setBounds(6, 16, 717, 100);
					panel.add(lblClasificacion);
					lblClasificacion.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.388")))); //$NON-NLS-1$
				}
				{
					lblTiempos = new JLabel(""); //$NON-NLS-1$
					lblTiempos.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.389")))); //$NON-NLS-1$
					lblTiempos.setBounds(949, 30, 200, 80);
					panelPodioCompeticion.add(lblTiempos);
				}
				{
					lblParticipantes = new JLabel(""); //$NON-NLS-1$
					lblParticipantes.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.390")))); //$NON-NLS-1$
					lblParticipantes.setBounds(949, 260, 200, 80);
					panelPodioCompeticion.add(lblParticipantes);
				}
				{
					btnAñadirPodioCompeticion = new JButton(MessagesIndex.getString("Index.btnAñadirPodioCompeticion.text")); //$NON-NLS-1$
					btnAñadirPodioCompeticion.addActionListener(new BtnAñadirpodiocompeticionActionListener());
					btnAñadirPodioCompeticion.setBounds(1023, 546, 89, 23);
					panelPodioCompeticion.add(btnAñadirPodioCompeticion);
				}
				{
					btnBorrarPodioCompeticion = new JButton(MessagesIndex.getString("Index.btnBorrarPodioCompeticion.text")); //$NON-NLS-1$
					btnBorrarPodioCompeticion.addActionListener(new BtnBorrarPodioCompeticionActionListener());
					btnBorrarPodioCompeticion.setBounds(1122, 546, 89, 23);
					panelPodioCompeticion.add(btnBorrarPodioCompeticion);
				}
				{
					btnGaleriaPodioCompeticion = new JButton(MessagesIndex.getString("Index.btnGaleriaPodioCompeticion.text")); //$NON-NLS-1$
					btnGaleriaPodioCompeticion.addActionListener(new BtnGaleriaPodioCompeticionActionListener());
					btnGaleriaPodioCompeticion.setBounds(799, 546, 89, 23);
					panelPodioCompeticion.add(btnGaleriaPodioCompeticion);
				}
			}
			{
				panelAdmin = new JPanel();
				panelAdmin.setBackground(SystemColor.inactiveCaptionBorder);
				panelContenedor.add(panelAdmin, "Admin"); //$NON-NLS-1$
				GridBagLayout gbl_panelAdmin = new GridBagLayout();
				gbl_panelAdmin.columnWidths = new int[]{56, 331, 211, 361, 49, 0};
				gbl_panelAdmin.rowHeights = new int[]{49, 0, 0, 0, 0, 0};
				gbl_panelAdmin.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				gbl_panelAdmin.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				panelAdmin.setLayout(gbl_panelAdmin);
				{
					panelDatosAdmin = new JPanel();
					panelDatosAdmin.setBorder(new LineBorder(new Color(0, 0, 0)));
					GridBagConstraints gbc_panelDatosAdmin = new GridBagConstraints();
					gbc_panelDatosAdmin.gridwidth = 3;
					gbc_panelDatosAdmin.insets = new Insets(0, 0, 5, 5);
					gbc_panelDatosAdmin.fill = GridBagConstraints.BOTH;
					gbc_panelDatosAdmin.gridx = 1;
					gbc_panelDatosAdmin.gridy = 1;
					panelAdmin.add(panelDatosAdmin, gbc_panelDatosAdmin);
					GridBagLayout gbl_panelDatosAdmin = new GridBagLayout();
					gbl_panelDatosAdmin.columnWidths = new int[]{44, 130, 128, 84, 0, 103, 125, 38, 58, 54, 35, 0};
					gbl_panelDatosAdmin.rowHeights = new int[]{56, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
					gbl_panelDatosAdmin.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					gbl_panelDatosAdmin.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
					panelDatosAdmin.setLayout(gbl_panelDatosAdmin);
					{
						spFotoAdmin = new JScrollPane();
						GridBagConstraints gbc_spFotoAdmin = new GridBagConstraints();
						gbc_spFotoAdmin.gridheight = 5;
						gbc_spFotoAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_spFotoAdmin.fill = GridBagConstraints.BOTH;
						gbc_spFotoAdmin.gridx = 1;
						gbc_spFotoAdmin.gridy = 1;
						panelDatosAdmin.add(spFotoAdmin, gbc_spFotoAdmin);
						{
							lblFotoAdmin = new JLabel(""); //$NON-NLS-1$
							lblFotoAdmin.setIcon(new ImageIcon(Index.class.getResource("/Recursos/av2.png"))); //$NON-NLS-1$
							spFotoAdmin.setViewportView(lblFotoAdmin);
						}
					}
					{
						lblNombreAdmin = new JLabel(MessagesIndex.getString("Index.lblNombreAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblNombreAdmin = new GridBagConstraints();
						gbc_lblNombreAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblNombreAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblNombreAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblNombreAdmin.gridx = 2;
						gbc_lblNombreAdmin.gridy = 1;
						panelDatosAdmin.add(lblNombreAdmin, gbc_lblNombreAdmin);
					}
					{
						txtNombreAdmin = new JTextField();
						GridBagConstraints gbc_txtNombreAdmin = new GridBagConstraints();
						gbc_txtNombreAdmin.fill = GridBagConstraints.BOTH;
						gbc_txtNombreAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtNombreAdmin.gridx = 3;
						gbc_txtNombreAdmin.gridy = 1;
						panelDatosAdmin.add(txtNombreAdmin, gbc_txtNombreAdmin);
					}
					{
						lblUsuarioAdmin = new JLabel(MessagesIndex.getString("Index.lblUsuarioAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblUsuarioAdmin = new GridBagConstraints();
						gbc_lblUsuarioAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblUsuarioAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblUsuarioAdmin.gridx = 6;
						gbc_lblUsuarioAdmin.gridy = 1;
						panelDatosAdmin.add(lblUsuarioAdmin, gbc_lblUsuarioAdmin);
					}
					{
						txtUsuarioAdmin = new JTextField();
						GridBagConstraints gbc_txtUsuarioAdmin = new GridBagConstraints();
						gbc_txtUsuarioAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtUsuarioAdmin.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtUsuarioAdmin.gridx = 7;
						gbc_txtUsuarioAdmin.gridy = 1;
						panelDatosAdmin.add(txtUsuarioAdmin, gbc_txtUsuarioAdmin);
						txtUsuarioAdmin.setColumns(10);
					}
					{
						lblApellidosAdmin = new JLabel(MessagesIndex.getString("Index.lblApellidosAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblApellidosAdmin = new GridBagConstraints();
						gbc_lblApellidosAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblApellidosAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblApellidosAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblApellidosAdmin.gridx = 2;
						gbc_lblApellidosAdmin.gridy = 2;
						panelDatosAdmin.add(lblApellidosAdmin, gbc_lblApellidosAdmin);
					}
					{
						txtApellidosAdmin = new JTextField();
						GridBagConstraints gbc_txtApellidosAdmin = new GridBagConstraints();
						gbc_txtApellidosAdmin.fill = GridBagConstraints.BOTH;
						gbc_txtApellidosAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtApellidosAdmin.gridx = 3;
						gbc_txtApellidosAdmin.gridy = 2;
						panelDatosAdmin.add(txtApellidosAdmin, gbc_txtApellidosAdmin);
					}
					{
						lblContraseñaAdmin = new JLabel(MessagesIndex.getString("Index.lblContraseñaAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblContraseñaAdmin = new GridBagConstraints();
						gbc_lblContraseñaAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblContraseñaAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblContraseñaAdmin.gridx = 6;
						gbc_lblContraseñaAdmin.gridy = 2;
						panelDatosAdmin.add(lblContraseñaAdmin, gbc_lblContraseñaAdmin);
					}
					{
						txtContraseñaAdmin = new JPasswordField();
						GridBagConstraints gbc_txtContraseñaAdmin = new GridBagConstraints();
						gbc_txtContraseñaAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtContraseñaAdmin.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtContraseñaAdmin.gridx = 7;
						gbc_txtContraseñaAdmin.gridy = 2;
						panelDatosAdmin.add(txtContraseñaAdmin, gbc_txtContraseñaAdmin);
						txtContraseñaAdmin.setColumns(10);
					}
					{
						lblDNIAdmin = new JLabel(MessagesIndex.getString("Index.lblDNIAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblDNIAdmin = new GridBagConstraints();
						gbc_lblDNIAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblDNIAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblDNIAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblDNIAdmin.gridx = 2;
						gbc_lblDNIAdmin.gridy = 3;
						panelDatosAdmin.add(lblDNIAdmin, gbc_lblDNIAdmin);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("########-U"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtDNIAdmin = new JFormattedTextField(mascara);
						
						GridBagConstraints gbc_txtDNIAdmin = new GridBagConstraints();
						gbc_txtDNIAdmin.fill = GridBagConstraints.BOTH;
						gbc_txtDNIAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtDNIAdmin.gridx = 3;
						gbc_txtDNIAdmin.gridy = 3;
						panelDatosAdmin.add(txtDNIAdmin, gbc_txtDNIAdmin);
						}catch (Exception e){
							
						}
					}
					{
						lblRepetirContraseñaAdmin = new JLabel(MessagesIndex.getString("Index.lblRepetirContraseñaAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblRepetirContraseñaAdmin = new GridBagConstraints();
						gbc_lblRepetirContraseñaAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblRepetirContraseñaAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblRepetirContraseñaAdmin.gridx = 6;
						gbc_lblRepetirContraseñaAdmin.gridy = 3;
						panelDatosAdmin.add(lblRepetirContraseñaAdmin, gbc_lblRepetirContraseñaAdmin);
					}
					{
						txtRepetirContraseñaAdmin = new JPasswordField();
						GridBagConstraints gbc_txtRepetirContraseñaAdmin = new GridBagConstraints();
						gbc_txtRepetirContraseñaAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtRepetirContraseñaAdmin.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtRepetirContraseñaAdmin.gridx = 7;
						gbc_txtRepetirContraseñaAdmin.gridy = 3;
						panelDatosAdmin.add(txtRepetirContraseñaAdmin, gbc_txtRepetirContraseñaAdmin);
						txtRepetirContraseñaAdmin.setColumns(10);
					}
					{
						lblFechaNacimientoAdmin = new JLabel(MessagesIndex.getString("Index.lblFechaNacimientoAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblFechaNacimientoAdmin = new GridBagConstraints();
						gbc_lblFechaNacimientoAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblFechaNacimientoAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblFechaNacimientoAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblFechaNacimientoAdmin.gridx = 2;
						gbc_lblFechaNacimientoAdmin.gridy = 4;
						panelDatosAdmin.add(lblFechaNacimientoAdmin, gbc_lblFechaNacimientoAdmin);
					}
					{
						cbFechaNacimientoDiaAdmin = new JComboBox();
						cbFechaNacimientoDiaAdmin.setModel(new DefaultComboBoxModel(new String[] {" ","01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$ //$NON-NLS-32$
						GridBagConstraints gbc_cbFechaNacimientoDiaAdmin = new GridBagConstraints();
						gbc_cbFechaNacimientoDiaAdmin.fill = GridBagConstraints.BOTH;
						gbc_cbFechaNacimientoDiaAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaNacimientoDiaAdmin.gridx = 3;
						gbc_cbFechaNacimientoDiaAdmin.gridy = 4;
						panelDatosAdmin.add(cbFechaNacimientoDiaAdmin, gbc_cbFechaNacimientoDiaAdmin);
					}
					{
						cbFechaNacimientoMesAdmin = new JComboBox();
						cbFechaNacimientoMesAdmin.setModel(new DefaultComboBoxModel(new String[] {" ","Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$
						GridBagConstraints gbc_cbFechaNacimientoMesAdmin = new GridBagConstraints();
						gbc_cbFechaNacimientoMesAdmin.fill = GridBagConstraints.BOTH;
						gbc_cbFechaNacimientoMesAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaNacimientoMesAdmin.gridx = 4;
						gbc_cbFechaNacimientoMesAdmin.gridy = 4;
						panelDatosAdmin.add(cbFechaNacimientoMesAdmin, gbc_cbFechaNacimientoMesAdmin);
					}
					{
						cbFechaNacimientoAnoAdmin = new JComboBox();
						cbFechaNacimientoAnoAdmin.setModel(new DefaultComboBoxModel(new String[] {" ","1920", "1921", "1922", "1923", "1924", "1925", "1926", "1927", "1928", "1929", "1930", "1931", "1932", "1933", "1934", "1935", "1936", "1937", "1938", "1939", "1940", "1941", "1942", "1943", "1944", "1945", "1946", "1947", "1948", "1949", "1950", "1951", "1952", "1953", "1954", "1955", "1956", "1957", "1958", "1959", "1960", "1961", "1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$ //$NON-NLS-32$ //$NON-NLS-33$ //$NON-NLS-34$ //$NON-NLS-35$ //$NON-NLS-36$ //$NON-NLS-37$ //$NON-NLS-38$ //$NON-NLS-39$ //$NON-NLS-40$ //$NON-NLS-41$ //$NON-NLS-42$ //$NON-NLS-43$ //$NON-NLS-44$ //$NON-NLS-45$ //$NON-NLS-46$ //$NON-NLS-47$ //$NON-NLS-48$ //$NON-NLS-49$ //$NON-NLS-50$ //$NON-NLS-51$ //$NON-NLS-52$ //$NON-NLS-53$ //$NON-NLS-54$ //$NON-NLS-55$ //$NON-NLS-56$ //$NON-NLS-57$ //$NON-NLS-58$ //$NON-NLS-59$ //$NON-NLS-60$ //$NON-NLS-61$ //$NON-NLS-62$ //$NON-NLS-63$ //$NON-NLS-64$ //$NON-NLS-65$ //$NON-NLS-66$ //$NON-NLS-67$ //$NON-NLS-68$ //$NON-NLS-69$ //$NON-NLS-70$ //$NON-NLS-71$ //$NON-NLS-72$ //$NON-NLS-73$ //$NON-NLS-74$ //$NON-NLS-75$ //$NON-NLS-76$ //$NON-NLS-77$ //$NON-NLS-78$ //$NON-NLS-79$ //$NON-NLS-80$ //$NON-NLS-81$ //$NON-NLS-82$ //$NON-NLS-83$ //$NON-NLS-84$ //$NON-NLS-85$ //$NON-NLS-86$ //$NON-NLS-87$ //$NON-NLS-88$ //$NON-NLS-89$ //$NON-NLS-90$ //$NON-NLS-91$ //$NON-NLS-92$ //$NON-NLS-93$ //$NON-NLS-94$ //$NON-NLS-95$ //$NON-NLS-96$
						GridBagConstraints gbc_cbFechaNacimientoAnoAdmin = new GridBagConstraints();
						gbc_cbFechaNacimientoAnoAdmin.fill = GridBagConstraints.BOTH;
						gbc_cbFechaNacimientoAnoAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_cbFechaNacimientoAnoAdmin.gridx = 5;
						gbc_cbFechaNacimientoAnoAdmin.gridy = 4;
						panelDatosAdmin.add(cbFechaNacimientoAnoAdmin, gbc_cbFechaNacimientoAnoAdmin);
					}
					{
						lblSexoAdmin = new JLabel(MessagesIndex.getString("Index.lblSexoAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblSexoAdmin = new GridBagConstraints();
						gbc_lblSexoAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblSexoAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblSexoAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblSexoAdmin.gridx = 2;
						gbc_lblSexoAdmin.gridy = 5;
						panelDatosAdmin.add(lblSexoAdmin, gbc_lblSexoAdmin);
					}
					{
						cbSexoAdmin = new JComboBox();
						cbSexoAdmin.setModel(new DefaultComboBoxModel(new String[] {" ","Masculino", "Femenino"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						GridBagConstraints gbc_cbSexoAdmin = new GridBagConstraints();
						gbc_cbSexoAdmin.fill = GridBagConstraints.BOTH;
						gbc_cbSexoAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_cbSexoAdmin.gridx = 3;
						gbc_cbSexoAdmin.gridy = 5;
						panelDatosAdmin.add(cbSexoAdmin, gbc_cbSexoAdmin);
					}
					{
						lblDireccionAdmin = new JLabel(MessagesIndex.getString("Index.lblDireccionAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblDireccionAdmin = new GridBagConstraints();
						gbc_lblDireccionAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblDireccionAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblDireccionAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblDireccionAdmin.gridx = 2;
						gbc_lblDireccionAdmin.gridy = 6;
						panelDatosAdmin.add(lblDireccionAdmin, gbc_lblDireccionAdmin);
					}
					{
						cbDireccionAdmin = new JComboBox();
						cbDireccionAdmin.setModel(new DefaultComboBoxModel(new String[] {" ","Calle", "Avenida", "Camino", "Cuesta", "Paseo", "Ronda", "Travesía", "Pasaje"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
						GridBagConstraints gbc_cbDireccionAdmin = new GridBagConstraints();
						gbc_cbDireccionAdmin.fill = GridBagConstraints.BOTH;
						gbc_cbDireccionAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_cbDireccionAdmin.gridx = 3;
						gbc_cbDireccionAdmin.gridy = 6;
						panelDatosAdmin.add(cbDireccionAdmin, gbc_cbDireccionAdmin);
					}
					{
						txtDireccionAdmin = new JTextField();
						GridBagConstraints gbc_txtDireccionAdmin = new GridBagConstraints();
						gbc_txtDireccionAdmin.fill = GridBagConstraints.BOTH;
						gbc_txtDireccionAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtDireccionAdmin.gridx = 4;
						gbc_txtDireccionAdmin.gridy = 6;
						panelDatosAdmin.add(txtDireccionAdmin, gbc_txtDireccionAdmin);
					}
					{
						lblNumeroAdmin = new JLabel(MessagesIndex.getString("Index.lblNumeroAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblNumeroAdmin = new GridBagConstraints();
						gbc_lblNumeroAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblNumeroAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblNumeroAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblNumeroAdmin.gridx = 6;
						gbc_lblNumeroAdmin.gridy = 6;
						panelDatosAdmin.add(lblNumeroAdmin, gbc_lblNumeroAdmin);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("##"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtNumeroAdmin = new JFormattedTextField(mascara);
							GridBagConstraints gbc_txtNumeroAdmin = new GridBagConstraints();
							gbc_txtNumeroAdmin.fill = GridBagConstraints.BOTH;
							gbc_txtNumeroAdmin.insets = new Insets(0, 0, 5, 5);
							gbc_txtNumeroAdmin.gridx = 7;
							gbc_txtNumeroAdmin.gridy = 6;
							panelDatosAdmin.add(txtNumeroAdmin, gbc_txtNumeroAdmin);
						}catch(Exception ex){

						}
					}
					{
						lblEscaleraAdmin = new JLabel(MessagesIndex.getString("Index.lblEscaleraAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblEscaleraAdmin = new GridBagConstraints();
						gbc_lblEscaleraAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblEscaleraAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblEscaleraAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblEscaleraAdmin.gridx = 2;
						gbc_lblEscaleraAdmin.gridy = 7;
						panelDatosAdmin.add(lblEscaleraAdmin, gbc_lblEscaleraAdmin);
					}
					{
						cbEscaleraAdmin = new JComboBox();
						cbEscaleraAdmin.setModel(new DefaultComboBoxModel(new String[] {" ","Derecha", "Izquierda"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						GridBagConstraints gbc_cbEscaleraAdmin = new GridBagConstraints();
						gbc_cbEscaleraAdmin.fill = GridBagConstraints.BOTH;
						gbc_cbEscaleraAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_cbEscaleraAdmin.gridx = 3;
						gbc_cbEscaleraAdmin.gridy = 7;
						panelDatosAdmin.add(cbEscaleraAdmin, gbc_cbEscaleraAdmin);
					}
					{
						lblPisoAdmin = new JLabel(MessagesIndex.getString("Index.lblPisoAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblPisoAdmin = new GridBagConstraints();
						gbc_lblPisoAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblPisoAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblPisoAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblPisoAdmin.gridx = 5;
						gbc_lblPisoAdmin.gridy = 7;
						panelDatosAdmin.add(lblPisoAdmin, gbc_lblPisoAdmin);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("U"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtPisoAdmin = new JFormattedTextField(mascara);
							GridBagConstraints gbc_txtPisoAdmin = new GridBagConstraints();
							gbc_txtPisoAdmin.fill = GridBagConstraints.BOTH;
							gbc_txtPisoAdmin.insets = new Insets(0, 0, 5, 5);
							gbc_txtPisoAdmin.gridx = 6;
							gbc_txtPisoAdmin.gridy = 7;
							panelDatosAdmin.add(txtPisoAdmin, gbc_txtPisoAdmin);
						}catch(Exception ex){

						}
					}
					{
						lblPuertaAdmin = new JLabel(MessagesIndex.getString("Index.lblPuertaAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblPuertaAdmin = new GridBagConstraints();
						gbc_lblPuertaAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblPuertaAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblPuertaAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblPuertaAdmin.gridx = 8;
						gbc_lblPuertaAdmin.gridy = 7;
						panelDatosAdmin.add(lblPuertaAdmin, gbc_lblPuertaAdmin);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("U"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtPuertaAdmin = new JFormattedTextField(mascara);
							GridBagConstraints gbc_txtPuertaAdmin = new GridBagConstraints();
							gbc_txtPuertaAdmin.fill = GridBagConstraints.BOTH;
							gbc_txtPuertaAdmin.insets = new Insets(0, 0, 5, 5);
							gbc_txtPuertaAdmin.gridx = 9;
							gbc_txtPuertaAdmin.gridy = 7;
							panelDatosAdmin.add(txtPuertaAdmin, gbc_txtPuertaAdmin);
						}catch(Exception e){

						}
					}
					{
						lblCodigoPostalAdmin = new JLabel(MessagesIndex.getString("Index.lblCodigoPostalAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblCodigoPostalAdmin = new GridBagConstraints();
						gbc_lblCodigoPostalAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblCodigoPostalAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblCodigoPostalAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblCodigoPostalAdmin.gridx = 2;
						gbc_lblCodigoPostalAdmin.gridy = 8;
						panelDatosAdmin.add(lblCodigoPostalAdmin, gbc_lblCodigoPostalAdmin);
					}
					{
						
						try{
							MaskFormatter mascara=new MaskFormatter("#####"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtCodigoPostalAdmin = new JFormattedTextField(mascara);
						
						GridBagConstraints gbc_txtCodigoPostalAdmin = new GridBagConstraints();
						gbc_txtCodigoPostalAdmin.fill = GridBagConstraints.BOTH;
						gbc_txtCodigoPostalAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtCodigoPostalAdmin.gridx = 3;
						gbc_txtCodigoPostalAdmin.gridy = 8;
						panelDatosAdmin.add(txtCodigoPostalAdmin, gbc_txtCodigoPostalAdmin);
						}catch(Exception e){
							
						}
					}
					{
						lblProvinciaAdmin = new JLabel(MessagesIndex.getString("Index.lblProvinciaAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblProvinciaAdmin = new GridBagConstraints();
						gbc_lblProvinciaAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblProvinciaAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblProvinciaAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblProvinciaAdmin.gridx = 5;
						gbc_lblProvinciaAdmin.gridy = 8;
						panelDatosAdmin.add(lblProvinciaAdmin, gbc_lblProvinciaAdmin);
					}
					{
						txtProvinciaAdmin = new JTextField();
						GridBagConstraints gbc_txtProvinciaAdmin = new GridBagConstraints();
						gbc_txtProvinciaAdmin.fill = GridBagConstraints.BOTH;
						gbc_txtProvinciaAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtProvinciaAdmin.gridx = 6;
						gbc_txtProvinciaAdmin.gridy = 8;
						panelDatosAdmin.add(txtProvinciaAdmin, gbc_txtProvinciaAdmin);
					}
					{
						lblLocalidadAdmin = new JLabel(MessagesIndex.getString("Index.lblLocalidadAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblLocalidadAdmin = new GridBagConstraints();
						gbc_lblLocalidadAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblLocalidadAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblLocalidadAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblLocalidadAdmin.gridx = 2;
						gbc_lblLocalidadAdmin.gridy = 9;
						panelDatosAdmin.add(lblLocalidadAdmin, gbc_lblLocalidadAdmin);
					}
					{
						txtLocalidadAdmin = new JTextField();
						GridBagConstraints gbc_txtLocalidadAdmin = new GridBagConstraints();
						gbc_txtLocalidadAdmin.fill = GridBagConstraints.BOTH;
						gbc_txtLocalidadAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtLocalidadAdmin.gridx = 3;
						gbc_txtLocalidadAdmin.gridy = 9;
						panelDatosAdmin.add(txtLocalidadAdmin, gbc_txtLocalidadAdmin);
					}
					{
						lblPaisAdmin = new JLabel(MessagesIndex.getString("Index.lblPaisAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblPaisAdmin = new GridBagConstraints();
						gbc_lblPaisAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblPaisAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblPaisAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblPaisAdmin.gridx = 2;
						gbc_lblPaisAdmin.gridy = 10;
						panelDatosAdmin.add(lblPaisAdmin, gbc_lblPaisAdmin);
					}
					{
						txtPaisAdmin = new JTextField();
						GridBagConstraints gbc_txtPaisAdmin = new GridBagConstraints();
						gbc_txtPaisAdmin.fill = GridBagConstraints.BOTH;
						gbc_txtPaisAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtPaisAdmin.gridx = 3;
						gbc_txtPaisAdmin.gridy = 10;
						panelDatosAdmin.add(txtPaisAdmin, gbc_txtPaisAdmin);
					}
					{
						lblTelefonoAdmin = new JLabel(MessagesIndex.getString("Index.lblTelefonoAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblTelefonoAdmin = new GridBagConstraints();
						gbc_lblTelefonoAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblTelefonoAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblTelefonoAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblTelefonoAdmin.gridx = 2;
						gbc_lblTelefonoAdmin.gridy = 11;
						panelDatosAdmin.add(lblTelefonoAdmin, gbc_lblTelefonoAdmin);
					}
					{
						try{
							MaskFormatter mascara=new MaskFormatter("#########"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtTelefonoAdmin = new JFormattedTextField(mascara);
							GridBagConstraints gbc_txtTelefonoAdmin = new GridBagConstraints();
							gbc_txtTelefonoAdmin.fill = GridBagConstraints.BOTH;
							gbc_txtTelefonoAdmin.insets = new Insets(0, 0, 5, 5);
							gbc_txtTelefonoAdmin.gridx = 3;
							gbc_txtTelefonoAdmin.gridy = 11;
							panelDatosAdmin.add(txtTelefonoAdmin, gbc_txtTelefonoAdmin);
						}catch(Exception e){
							
						}
					}
					{
						lblCorreoElectronicoAdmin = new JLabel(MessagesIndex.getString("Index.lblCorreoElectronicoAdmin.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblCorreoElectronicoAdmin = new GridBagConstraints();
						gbc_lblCorreoElectronicoAdmin.fill = GridBagConstraints.VERTICAL;
						gbc_lblCorreoElectronicoAdmin.anchor = GridBagConstraints.EAST;
						gbc_lblCorreoElectronicoAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_lblCorreoElectronicoAdmin.gridx = 5;
						gbc_lblCorreoElectronicoAdmin.gridy = 11;
						panelDatosAdmin.add(lblCorreoElectronicoAdmin, gbc_lblCorreoElectronicoAdmin);
					}
					{
						txtCorreoElectronicoAdmin = new JFormattedTextField();
						GridBagConstraints gbc_txtCorreoElectronicoAdmin = new GridBagConstraints();
						gbc_txtCorreoElectronicoAdmin.gridwidth = 4;
						gbc_txtCorreoElectronicoAdmin.fill = GridBagConstraints.BOTH;
						gbc_txtCorreoElectronicoAdmin.insets = new Insets(0, 0, 5, 5);
						gbc_txtCorreoElectronicoAdmin.gridx = 6;
						gbc_txtCorreoElectronicoAdmin.gridy = 11;
						panelDatosAdmin.add(txtCorreoElectronicoAdmin, gbc_txtCorreoElectronicoAdmin);
					}
					{
						{
							{
								btnGuardarAdminAdmin = new JButton(MessagesIndex.getString("Index.btnGuardarAdminAdmin.text")); //$NON-NLS-1$
								btnGuardarAdminAdmin.addActionListener(new BtnGuardarAdminActionListener());
								{
									btnCrearAdminAdmin = new JButton(MessagesIndex.getString("Index.btnCrearAdminAdmin.text")); //$NON-NLS-1$
									btnCrearAdminAdmin.addActionListener(new BtnCrearAdminAdminActionListener());
									GridBagConstraints gbc_btnCrearAdminAdmin = new GridBagConstraints();
									gbc_btnCrearAdminAdmin.insets = new Insets(0, 0, 0, 5);
									gbc_btnCrearAdminAdmin.gridx = 1;
									gbc_btnCrearAdminAdmin.gridy = 15;
									panelDatosAdmin.add(btnCrearAdminAdmin, gbc_btnCrearAdminAdmin);
								}
								GridBagConstraints gbc_btnGuardarAdminAdmin = new GridBagConstraints();
								gbc_btnGuardarAdminAdmin.insets = new Insets(0, 0, 0, 5);
								gbc_btnGuardarAdminAdmin.gridx = 2;
								gbc_btnGuardarAdminAdmin.gridy = 15;
								panelDatosAdmin.add(btnGuardarAdminAdmin, gbc_btnGuardarAdminAdmin);
							}
						}
					}
					btnEliminarAdminAdmin = new JButton(MessagesIndex.getString("Index.btnEliminarAdminAdmin.text")); //$NON-NLS-1$
					btnEliminarAdminAdmin.addActionListener(new BtnEliminarAdminActionListener());
					btnModificarAdminAdmin = new JButton(MessagesIndex.getString("Index.btnModificarAdminAdmin.text")); //$NON-NLS-1$
					btnModificarAdminAdmin.addActionListener(new BtnModificarAdminActionListener());
					GridBagConstraints gbc_btnModificarAdminAdmin = new GridBagConstraints();
					gbc_btnModificarAdminAdmin.insets = new Insets(0, 0, 0, 5);
					gbc_btnModificarAdminAdmin.gridx = 3;
					gbc_btnModificarAdminAdmin.gridy = 15;
					panelDatosAdmin.add(btnModificarAdminAdmin, gbc_btnModificarAdminAdmin);
					GridBagConstraints gbc_btnEliminarAdminAdmin = new GridBagConstraints();
					gbc_btnEliminarAdminAdmin.insets = new Insets(0, 0, 0, 5);
					gbc_btnEliminarAdminAdmin.gridx = 4;
					gbc_btnEliminarAdminAdmin.gridy = 15;
					panelDatosAdmin.add(btnEliminarAdminAdmin, gbc_btnEliminarAdminAdmin);
				}
				{
					lblFechaUltimoAccesoAdmin = new JLabel(MessagesIndex.getString("Index.lblFechaUltimoAccesoAdmin.text")); //$NON-NLS-1$
					GridBagConstraints gbc_lblFechaUltimoAccesoAdmin = new GridBagConstraints();
					gbc_lblFechaUltimoAccesoAdmin.anchor = GridBagConstraints.EAST;
					gbc_lblFechaUltimoAccesoAdmin.insets = new Insets(0, 0, 5, 5);
					gbc_lblFechaUltimoAccesoAdmin.gridx = 1;
					gbc_lblFechaUltimoAccesoAdmin.gridy = 3;
					panelAdmin.add(lblFechaUltimoAccesoAdmin, gbc_lblFechaUltimoAccesoAdmin);
				}
				{
					lblFechaUltimoAccesoMostrarAdmin = new JLabel(""); //$NON-NLS-1$
					GridBagConstraints gbc_lblFechaUltimoAccesoMostrarAdmin = new GridBagConstraints();
					gbc_lblFechaUltimoAccesoMostrarAdmin.insets = new Insets(0, 0, 5, 5);
					gbc_lblFechaUltimoAccesoMostrarAdmin.gridx = 2;
					gbc_lblFechaUltimoAccesoMostrarAdmin.gridy = 3;
					panelAdmin.add(lblFechaUltimoAccesoMostrarAdmin, gbc_lblFechaUltimoAccesoMostrarAdmin);
				}
			}
			{
				panelNuevaCompeticionParticipante = new JPanel();
				panelContenedor.add(panelNuevaCompeticionParticipante, "NuevaCompeticionParticipante"); //$NON-NLS-1$
				GridBagLayout gbl_panelNuevaCompeticionParticipante = new GridBagLayout();
				gbl_panelNuevaCompeticionParticipante.columnWidths = new int[]{18, 285, 0, 41, 289, 43, 0, 246, 18, 0};
				gbl_panelNuevaCompeticionParticipante.rowHeights = new int[]{60, 0, 0, 0, 0, 0, 28, 0, 0, 0, 30, 0, 45, 0};
				gbl_panelNuevaCompeticionParticipante.columnWeights = new double[]{1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
				gbl_panelNuevaCompeticionParticipante.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				panelNuevaCompeticionParticipante.setLayout(gbl_panelNuevaCompeticionParticipante);
				{
					{
						{
							//modeloListaNuevaCompeticionParticipante = new DefaultListModel();
						}
					}
				}
				{
					lblSeleccioneUnParticipante = new JLabel(MessagesIndex.getString("Index.lblSeleccioneUnParticipante.text")); //$NON-NLS-1$
					lblSeleccioneUnParticipante.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
					GridBagConstraints gbc_lblSeleccioneUnParticipante = new GridBagConstraints();
					gbc_lblSeleccioneUnParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_lblSeleccioneUnParticipante.gridx = 1;
					gbc_lblSeleccioneUnParticipante.gridy = 0;
					panelNuevaCompeticionParticipante.add(lblSeleccioneUnParticipante, gbc_lblSeleccioneUnParticipante);
				}
				{
					lblSeleccioneUnaCompeticin = new JLabel(MessagesIndex.getString("Index.lblSeleccioneUnaCompeticin.text")); //$NON-NLS-1$
					lblSeleccioneUnaCompeticin.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
					GridBagConstraints gbc_lblSeleccioneUnaCompeticin = new GridBagConstraints();
					gbc_lblSeleccioneUnaCompeticin.insets = new Insets(0, 0, 5, 5);
					gbc_lblSeleccioneUnaCompeticin.gridx = 7;
					gbc_lblSeleccioneUnaCompeticin.gridy = 0;
					panelNuevaCompeticionParticipante.add(lblSeleccioneUnaCompeticin, gbc_lblSeleccioneUnaCompeticin);
				}
				{
					panelParticipantesNuevaCompeticionParticipante = new JPanel();
					panelParticipantesNuevaCompeticionParticipante.setBorder(new LineBorder(new Color(0, 0, 0)));
					GridBagConstraints gbc_panelParticipantesNuevaCompeticionParticipante = new GridBagConstraints();
					gbc_panelParticipantesNuevaCompeticionParticipante.gridheight = 11;
					gbc_panelParticipantesNuevaCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_panelParticipantesNuevaCompeticionParticipante.fill = GridBagConstraints.BOTH;
					gbc_panelParticipantesNuevaCompeticionParticipante.gridx = 1;
					gbc_panelParticipantesNuevaCompeticionParticipante.gridy = 1;
					panelNuevaCompeticionParticipante.add(panelParticipantesNuevaCompeticionParticipante, gbc_panelParticipantesNuevaCompeticionParticipante);
					GridBagLayout gbl_panelParticipantesNuevaCompeticionParticipante = new GridBagLayout();
					gbl_panelParticipantesNuevaCompeticionParticipante.columnWidths = new int[]{0, 0, 0, 0};
					gbl_panelParticipantesNuevaCompeticionParticipante.rowHeights = new int[]{0, 0, 0, 0};
					gbl_panelParticipantesNuevaCompeticionParticipante.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
					gbl_panelParticipantesNuevaCompeticionParticipante.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
					panelParticipantesNuevaCompeticionParticipante.setLayout(gbl_panelParticipantesNuevaCompeticionParticipante);
					{
						lblImagenCompeticiones = new JLabel(""); //$NON-NLS-1$
						lblImagenCompeticiones.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.558")))); //$NON-NLS-1$
						GridBagConstraints gbc_lblImagenCompeticiones = new GridBagConstraints();
						gbc_lblImagenCompeticiones.insets = new Insets(0, 0, 5, 5);
						gbc_lblImagenCompeticiones.gridx = 1;
						gbc_lblImagenCompeticiones.gridy = 0;
						panelParticipantesNuevaCompeticionParticipante.add(lblImagenCompeticiones, gbc_lblImagenCompeticiones);
					}
					{
						spParticipanteNuevaCompeticionParticipante = new JScrollPane();
						GridBagConstraints gbc_spParticipanteNuevaCompeticionParticipante = new GridBagConstraints();
						gbc_spParticipanteNuevaCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
						gbc_spParticipanteNuevaCompeticionParticipante.fill = GridBagConstraints.BOTH;
						gbc_spParticipanteNuevaCompeticionParticipante.gridx = 1;
						gbc_spParticipanteNuevaCompeticionParticipante.gridy = 1;
						panelParticipantesNuevaCompeticionParticipante.add(spParticipanteNuevaCompeticionParticipante, gbc_spParticipanteNuevaCompeticionParticipante);
						{
							listParticipanteNuevaCompeticionParticipante = new JList();
							modeloListaParticipanteNuevaCompeticionParticipante = new DefaultListModel();
							listParticipanteNuevaCompeticionParticipante.setModel(modeloListaParticipanteNuevaCompeticionParticipante);
							listParticipanteNuevaCompeticionParticipante.setSelectedIndex(0);
							listParticipanteNuevaCompeticionParticipante.setCellRenderer(new ParticipanteCellRenderer());
							listParticipanteNuevaCompeticionParticipante.addListSelectionListener(new ListParticipanteNuevaCompeticionParticipanteListSelectionListener());
							spParticipanteNuevaCompeticionParticipante.setViewportView(listParticipanteNuevaCompeticionParticipante);
						}
					}
				}
				{
					panelTiemposNuevaCompeticionParticipante = new JPanel();
					panelTiemposNuevaCompeticionParticipante.setBorder(new LineBorder(new Color(0, 0, 0)));
					GridBagConstraints gbc_panelTiemposNuevaCompeticionParticipante = new GridBagConstraints();
					gbc_panelTiemposNuevaCompeticionParticipante.gridheight = 5;
					gbc_panelTiemposNuevaCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_panelTiemposNuevaCompeticionParticipante.fill = GridBagConstraints.BOTH;
					gbc_panelTiemposNuevaCompeticionParticipante.gridx = 4;
					gbc_panelTiemposNuevaCompeticionParticipante.gridy = 1;
					panelNuevaCompeticionParticipante.add(panelTiemposNuevaCompeticionParticipante, gbc_panelTiemposNuevaCompeticionParticipante);
					GridBagLayout gbl_panelTiemposNuevaCompeticionParticipante = new GridBagLayout();
					gbl_panelTiemposNuevaCompeticionParticipante.columnWidths = new int[]{0, 0, 0, 0, 0, 70, 0, 0, 0};
					gbl_panelTiemposNuevaCompeticionParticipante.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
					gbl_panelTiemposNuevaCompeticionParticipante.columnWeights = new double[]{0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					gbl_panelTiemposNuevaCompeticionParticipante.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
					panelTiemposNuevaCompeticionParticipante.setLayout(gbl_panelTiemposNuevaCompeticionParticipante);
					{
						lblDatosCP = new JLabel(""); //$NON-NLS-1$
						lblDatosCP.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.559")))); //$NON-NLS-1$
						GridBagConstraints gbc_lblDatosCP = new GridBagConstraints();
						gbc_lblDatosCP.gridwidth = 6;
						gbc_lblDatosCP.insets = new Insets(0, 0, 5, 5);
						gbc_lblDatosCP.gridx = 1;
						gbc_lblDatosCP.gridy = 1;
						panelTiemposNuevaCompeticionParticipante.add(lblDatosCP, gbc_lblDatosCP);
					}
					{
						lblTiempoTotalCP = new JLabel(MessagesIndex.getString("Index.lblTiempoTotalCP.text")); //$NON-NLS-1$
						lblTiempoTotalCP.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_lblTiempoTotalCP = new GridBagConstraints();
						gbc_lblTiempoTotalCP.anchor = GridBagConstraints.EAST;
						gbc_lblTiempoTotalCP.insets = new Insets(0, 0, 5, 5);
						gbc_lblTiempoTotalCP.gridx = 1;
						gbc_lblTiempoTotalCP.gridy = 2;
						panelTiemposNuevaCompeticionParticipante.add(lblTiempoTotalCP, gbc_lblTiempoTotalCP);
					}
					{
						
						try{
						
						MaskFormatter mascara=new MaskFormatter("##:##:##"); //$NON-NLS-1$
						mascara.setPlaceholderCharacter('X');
						
						txtTiempoTotalCP = new JFormattedTextField(mascara);
						
						
						GridBagConstraints gbc_txtTiempoTotalCP = new GridBagConstraints();
						gbc_txtTiempoTotalCP.gridwidth = 4;
						gbc_txtTiempoTotalCP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtTiempoTotalCP.insets = new Insets(0, 0, 5, 5);
						gbc_txtTiempoTotalCP.gridx = 2;
						gbc_txtTiempoTotalCP.gridy = 2;
						panelTiemposNuevaCompeticionParticipante.add(txtTiempoTotalCP, gbc_txtTiempoTotalCP);
						txtTiempoTotalCP.setColumns(10);
						}catch(Exception e){
							
						}
					}
					{
						lblSeccion1CP = new JLabel(MessagesIndex.getString("Index.lblSeccion1CP.text")); //$NON-NLS-1$
						lblSeccion1CP.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion1CP = new GridBagConstraints();
						gbc_lblSeccion1CP.anchor = GridBagConstraints.EAST;
						gbc_lblSeccion1CP.insets = new Insets(0, 0, 5, 5);
						gbc_lblSeccion1CP.gridx = 1;
						gbc_lblSeccion1CP.gridy = 3;
						panelTiemposNuevaCompeticionParticipante.add(lblSeccion1CP, gbc_lblSeccion1CP);
					}
					{
						
						try{
							
							MaskFormatter mascara = new MaskFormatter("##:##:##"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtSeccion1CP = new JFormattedTextField(mascara);
							
						GridBagConstraints gbc_txtSeccion1CP = new GridBagConstraints();
						gbc_txtSeccion1CP.gridwidth = 4;
						gbc_txtSeccion1CP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtSeccion1CP.insets = new Insets(0, 0, 5, 5);
						gbc_txtSeccion1CP.gridx = 2;
						gbc_txtSeccion1CP.gridy = 3;
						panelTiemposNuevaCompeticionParticipante.add(txtSeccion1CP, gbc_txtSeccion1CP);
						txtSeccion1CP.setColumns(10);
						}catch(Exception ex){
							
						}
					}
					{
						lblSeccion2CP = new JLabel(MessagesIndex.getString("Index.lblSeccion2CP.text")); //$NON-NLS-1$
						lblSeccion2CP.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion2CP = new GridBagConstraints();
						gbc_lblSeccion2CP.anchor = GridBagConstraints.EAST;
						gbc_lblSeccion2CP.insets = new Insets(0, 0, 5, 5);
						gbc_lblSeccion2CP.gridx = 1;
						gbc_lblSeccion2CP.gridy = 4;
						panelTiemposNuevaCompeticionParticipante.add(lblSeccion2CP, gbc_lblSeccion2CP);
					}
					{
						try{
							
							MaskFormatter mascara = new MaskFormatter("##:##:##"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtSeccion2CP = new JFormattedTextField(mascara);
						
						GridBagConstraints gbc_txtSeccion2CP = new GridBagConstraints();
						gbc_txtSeccion2CP.gridwidth = 4;
						gbc_txtSeccion2CP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtSeccion2CP.insets = new Insets(0, 0, 5, 5);
						gbc_txtSeccion2CP.gridx = 2;
						gbc_txtSeccion2CP.gridy = 4;
						panelTiemposNuevaCompeticionParticipante.add(txtSeccion2CP, gbc_txtSeccion2CP);
						txtSeccion2CP.setColumns(10);
						}catch(Exception e){
							
						}
					}
					{
						lblSeccion3CP = new JLabel(MessagesIndex.getString("Index.lblSeccion3CP.text")); //$NON-NLS-1$
						lblSeccion3CP.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion3CP = new GridBagConstraints();
						gbc_lblSeccion3CP.anchor = GridBagConstraints.EAST;
						gbc_lblSeccion3CP.insets = new Insets(0, 0, 5, 5);
						gbc_lblSeccion3CP.gridx = 1;
						gbc_lblSeccion3CP.gridy = 5;
						panelTiemposNuevaCompeticionParticipante.add(lblSeccion3CP, gbc_lblSeccion3CP);
					}
					{
						try{
							
							MaskFormatter mascara=new MaskFormatter("##:##:##"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtSeccion3CP = new JFormattedTextField(mascara);
						
						GridBagConstraints gbc_txtSeccion3CP = new GridBagConstraints();
						gbc_txtSeccion3CP.gridwidth = 4;
						gbc_txtSeccion3CP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtSeccion3CP.insets = new Insets(0, 0, 5, 5);
						gbc_txtSeccion3CP.gridx = 2;
						gbc_txtSeccion3CP.gridy = 5;
						panelTiemposNuevaCompeticionParticipante.add(txtSeccion3CP, gbc_txtSeccion3CP);
						txtSeccion3CP.setColumns(10);
						}catch(Exception e){
							
						}
					}
					{
						lblSeccion4CP = new JLabel(MessagesIndex.getString("Index.lblSeccion4CP.text")); //$NON-NLS-1$
						lblSeccion4CP.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion4CP = new GridBagConstraints();
						gbc_lblSeccion4CP.anchor = GridBagConstraints.EAST;
						gbc_lblSeccion4CP.insets = new Insets(0, 0, 5, 5);
						gbc_lblSeccion4CP.gridx = 1;
						gbc_lblSeccion4CP.gridy = 6;
						panelTiemposNuevaCompeticionParticipante.add(lblSeccion4CP, gbc_lblSeccion4CP);
					}
					{
						try{
							
							MaskFormatter mascara=new MaskFormatter("##:##:##"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtSeccion4CP = new JFormattedTextField(mascara);
						
						GridBagConstraints gbc_txtSeccion4CP = new GridBagConstraints();
						gbc_txtSeccion4CP.gridwidth = 4;
						gbc_txtSeccion4CP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtSeccion4CP.insets = new Insets(0, 0, 5, 5);
						gbc_txtSeccion4CP.gridx = 2;
						gbc_txtSeccion4CP.gridy = 6;
						panelTiemposNuevaCompeticionParticipante.add(txtSeccion4CP, gbc_txtSeccion4CP);
						txtSeccion4CP.setColumns(10);
						}catch(Exception e){
							
						}
					}
					{
						lblSeccion5CP = new JLabel(MessagesIndex.getString("Index.lblSeccion5CP.text")); //$NON-NLS-1$
						lblSeccion5CP.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_lblSeccion5CP = new GridBagConstraints();
						gbc_lblSeccion5CP.anchor = GridBagConstraints.EAST;
						gbc_lblSeccion5CP.insets = new Insets(0, 0, 5, 5);
						gbc_lblSeccion5CP.gridx = 1;
						gbc_lblSeccion5CP.gridy = 7;
						panelTiemposNuevaCompeticionParticipante.add(lblSeccion5CP, gbc_lblSeccion5CP);
					}
					{
						try{
							
							MaskFormatter mascara=new MaskFormatter("##:##:##"); //$NON-NLS-1$
							mascara.setPlaceholderCharacter('X');
							txtSeccion5CP = new JFormattedTextField(mascara);
							
						
						GridBagConstraints gbc_txtSeccion5CP = new GridBagConstraints();
						gbc_txtSeccion5CP.gridwidth = 4;
						gbc_txtSeccion5CP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtSeccion5CP.insets = new Insets(0, 0, 5, 5);
						gbc_txtSeccion5CP.gridx = 2;
						gbc_txtSeccion5CP.gridy = 7;
						panelTiemposNuevaCompeticionParticipante.add(txtSeccion5CP, gbc_txtSeccion5CP);
						txtSeccion5CP.setColumns(10);
						}catch(Exception e){
							
						}
					}
					{
						lblPosicionCP = new JLabel(MessagesIndex.getString("Index.lblPosicionCP.text")); //$NON-NLS-1$
						lblPosicionCP.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_lblPosicionCP = new GridBagConstraints();
						gbc_lblPosicionCP.anchor = GridBagConstraints.EAST;
						gbc_lblPosicionCP.insets = new Insets(0, 0, 5, 5);
						gbc_lblPosicionCP.gridx = 1;
						gbc_lblPosicionCP.gridy = 8;
						panelTiemposNuevaCompeticionParticipante.add(lblPosicionCP, gbc_lblPosicionCP);
					}
					{

						txtPosicionCP = new JFormattedTextField();
						txtPosicionCP.addKeyListener(new KeyAdapter() {
							@Override
							public void keyTyped(java.awt.event.KeyEvent evt) {
								char c=evt.getKeyChar();
								if(!(Character.isDigit(c) || (c==KeyEvent.VK_BACK_SPACE) || c==KeyEvent.VK_DELETE)){
									evt.consume();
								}
							}
							
						});
						
						GridBagConstraints gbc_txtPosicionCP = new GridBagConstraints();
						gbc_txtPosicionCP.gridwidth = 4;
						gbc_txtPosicionCP.insets = new Insets(0, 0, 5, 5);
						gbc_txtPosicionCP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtPosicionCP.gridx = 2;
						gbc_txtPosicionCP.gridy = 8;
						panelTiemposNuevaCompeticionParticipante.add(txtPosicionCP, gbc_txtPosicionCP);
						txtPosicionCP.setColumns(10);

					}
					{
						lblParticipanteCP = new JLabel(MessagesIndex.getString("Index.lblParticipanteCP.text")); //$NON-NLS-1$
						lblParticipanteCP.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_lblParticipanteCP = new GridBagConstraints();
						gbc_lblParticipanteCP.anchor = GridBagConstraints.EAST;
						gbc_lblParticipanteCP.insets = new Insets(0, 0, 5, 5);
						gbc_lblParticipanteCP.gridx = 1;
						gbc_lblParticipanteCP.gridy = 9;
						panelTiemposNuevaCompeticionParticipante.add(lblParticipanteCP, gbc_lblParticipanteCP);
					}
					{
						txtParticipanteCP = new JTextField();
						GridBagConstraints gbc_txtParticipanteCP = new GridBagConstraints();
						gbc_txtParticipanteCP.gridwidth = 4;
						gbc_txtParticipanteCP.insets = new Insets(0, 0, 5, 5);
						gbc_txtParticipanteCP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtParticipanteCP.gridx = 2;
						gbc_txtParticipanteCP.gridy = 9;
						panelTiemposNuevaCompeticionParticipante.add(txtParticipanteCP, gbc_txtParticipanteCP);
						txtParticipanteCP.setColumns(10);
					}
					{
						lblCompeticionCP = new JLabel(MessagesIndex.getString("Index.lblCompeticionCP.text")); //$NON-NLS-1$
						lblCompeticionCP.setFont(new Font("Tahoma", Font.PLAIN, 12)); //$NON-NLS-1$
						GridBagConstraints gbc_lblCompeticionCP = new GridBagConstraints();
						gbc_lblCompeticionCP.anchor = GridBagConstraints.EAST;
						gbc_lblCompeticionCP.insets = new Insets(0, 0, 5, 5);
						gbc_lblCompeticionCP.gridx = 1;
						gbc_lblCompeticionCP.gridy = 10;
						panelTiemposNuevaCompeticionParticipante.add(lblCompeticionCP, gbc_lblCompeticionCP);
					}
					{
						txtCompeticionCP = new JTextField();
						GridBagConstraints gbc_txtCompeticionCP = new GridBagConstraints();
						gbc_txtCompeticionCP.gridwidth = 4;
						gbc_txtCompeticionCP.insets = new Insets(0, 0, 5, 5);
						gbc_txtCompeticionCP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txtCompeticionCP.gridx = 2;
						gbc_txtCompeticionCP.gridy = 10;
						panelTiemposNuevaCompeticionParticipante.add(txtCompeticionCP, gbc_txtCompeticionCP);
						txtCompeticionCP.setColumns(10);
					}
				}
				panelCompeticionesNuevaCompeticionParticipante = new JPanel();
				panelCompeticionesNuevaCompeticionParticipante.setBorder(new LineBorder(new Color(0, 0, 0)));
				GridBagConstraints gbc_panelCompeticionesNuevaCompeticionParticipante = new GridBagConstraints();
				gbc_panelCompeticionesNuevaCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
				gbc_panelCompeticionesNuevaCompeticionParticipante.gridheight = 11;
				gbc_panelCompeticionesNuevaCompeticionParticipante.fill = GridBagConstraints.VERTICAL;
				gbc_panelCompeticionesNuevaCompeticionParticipante.gridx = 7;
				gbc_panelCompeticionesNuevaCompeticionParticipante.gridy = 1;
				panelNuevaCompeticionParticipante.add(panelCompeticionesNuevaCompeticionParticipante, gbc_panelCompeticionesNuevaCompeticionParticipante);
				GridBagLayout gbl_panelCompeticionesNuevaCompeticionParticipante = new GridBagLayout();
				gbl_panelCompeticionesNuevaCompeticionParticipante.columnWidths = new int[]{0, 97, 0, 0, 0, 18, 21, 92, 0, 0};
				gbl_panelCompeticionesNuevaCompeticionParticipante.rowHeights = new int[]{0, 0, 0, 31, 0, 0};
				gbl_panelCompeticionesNuevaCompeticionParticipante.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				gbl_panelCompeticionesNuevaCompeticionParticipante.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				panelCompeticionesNuevaCompeticionParticipante.setLayout(gbl_panelCompeticionesNuevaCompeticionParticipante);
				{
					label = new JLabel(""); //$NON-NLS-1$
					label.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.575")))); //$NON-NLS-1$
					GridBagConstraints gbc_label = new GridBagConstraints();
					gbc_label.gridwidth = 7;
					gbc_label.insets = new Insets(0, 0, 5, 5);
					gbc_label.gridx = 1;
					gbc_label.gridy = 0;
					panelCompeticionesNuevaCompeticionParticipante.add(label, gbc_label);
				}
				spCompeticionNuevaCompeticionParticipante = new JScrollPane();
				GridBagConstraints gbc_spCompeticionNuevaCompeticionParticipante = new GridBagConstraints();
				gbc_spCompeticionNuevaCompeticionParticipante.gridheight = 3;
				gbc_spCompeticionNuevaCompeticionParticipante.gridwidth = 7;
				gbc_spCompeticionNuevaCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
				gbc_spCompeticionNuevaCompeticionParticipante.fill = GridBagConstraints.BOTH;
				gbc_spCompeticionNuevaCompeticionParticipante.gridx = 1;
				gbc_spCompeticionNuevaCompeticionParticipante.gridy = 1;
				panelCompeticionesNuevaCompeticionParticipante.add(spCompeticionNuevaCompeticionParticipante, gbc_spCompeticionNuevaCompeticionParticipante);

				listCompeticionNuevaCompeticionParticipante = new JList();							
				listCompeticionNuevaCompeticionParticipante.addListSelectionListener(new ListCompeticionNuevaCompeticionParticipanteListSelectionListener());
				modeloListaCompeticionNuevaCompeticionParticipante = new DefaultListModel();
				listCompeticionNuevaCompeticionParticipante.setModel(modeloListaCompeticionNuevaCompeticionParticipante);
				listCompeticionNuevaCompeticionParticipante.setSelectedIndex(1);
				listCompeticionNuevaCompeticionParticipante.setCellRenderer(new CompeticionCellRenderer());
				spCompeticionNuevaCompeticionParticipante.setViewportView(listCompeticionNuevaCompeticionParticipante);
				{
					btnGuardarNuevaCompeticionParticipante = new JButton(""); //$NON-NLS-1$
					btnGuardarNuevaCompeticionParticipante.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.576")))); //$NON-NLS-1$
					btnGuardarNuevaCompeticionParticipante.addActionListener(new BtnGuardarActionListener());
					GridBagConstraints gbc_btnGuardarNuevaCompeticionParticipante = new GridBagConstraints();
					gbc_btnGuardarNuevaCompeticionParticipante.fill = GridBagConstraints.BOTH;
					gbc_btnGuardarNuevaCompeticionParticipante.gridheight = 3;
					gbc_btnGuardarNuevaCompeticionParticipante.insets = new Insets(0, 0, 5, 5);
					gbc_btnGuardarNuevaCompeticionParticipante.gridx = 4;
					gbc_btnGuardarNuevaCompeticionParticipante.gridy = 8;
					panelNuevaCompeticionParticipante.add(btnGuardarNuevaCompeticionParticipante, gbc_btnGuardarNuevaCompeticionParticipante);
					btnGuardarNuevaCompeticionParticipante.setFont(new Font("Tahoma", Font.PLAIN, 15)); //$NON-NLS-1$
				}
			}
			{
				{
					{
					}
				}
			}
		}
		{
			panelMenu = new JPanel();
			panelMenu.setBackground(SystemColor.textHighlightText);
			panelMenu.setBorder(UIManager.getBorder("DesktopIcon.border")); //$NON-NLS-1$
			frmIndex.getContentPane().add(panelMenu, BorderLayout.NORTH);
			GridBagLayout gbl_panelMenu = new GridBagLayout();
			gbl_panelMenu.columnWidths = new int[]{0, 0, 525, 70, 105, 109, 0};
			gbl_panelMenu.rowHeights = new int[]{0, 0};
			gbl_panelMenu.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panelMenu.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			panelMenu.setLayout(gbl_panelMenu);
			{
				panelAtras = new JPanel();
				GridBagConstraints gbc_panelAtras = new GridBagConstraints();
				gbc_panelAtras.fill = GridBagConstraints.BOTH;
				gbc_panelAtras.insets = new Insets(0, 0, 0, 5);
				gbc_panelAtras.gridx = 0;
				gbc_panelAtras.gridy = 0;
				panelMenu.add(panelAtras, gbc_panelAtras);
				GridBagLayout gbl_panelAtras = new GridBagLayout();
				gbl_panelAtras.columnWidths = new int[]{0, 0};
				gbl_panelAtras.rowHeights = new int[]{0, 0};
				gbl_panelAtras.columnWeights = new double[]{0.0, Double.MIN_VALUE};
				gbl_panelAtras.rowWeights = new double[]{0.0, Double.MIN_VALUE};
				panelAtras.setLayout(gbl_panelAtras);
				{
					btnAtras = new JButton(""); //$NON-NLS-1$
					btnAtras.addActionListener(new BtnAtrasActionListener());
					
					
					btnAtras.setAlignmentX(Component.CENTER_ALIGNMENT);
					GridBagConstraints gbc_btnAtras = new GridBagConstraints();
					gbc_btnAtras.gridx = 0;
					gbc_btnAtras.gridy = 0;
					panelAtras.add(btnAtras, gbc_btnAtras);
					btnAtras.setHorizontalTextPosition(SwingConstants.CENTER);
					btnAtras.setIcon(new ImageIcon(Index.class.getResource("/Recursos/left_circular-26.png"))); //$NON-NLS-1$
				}
			}
			{
				panelHome = new JPanel();
				GridBagConstraints gbc_panelHome = new GridBagConstraints();
				gbc_panelHome.fill = GridBagConstraints.BOTH;
				gbc_panelHome.insets = new Insets(0, 0, 0, 5);
				gbc_panelHome.gridx = 1;
				gbc_panelHome.gridy = 0;
				panelMenu.add(panelHome, gbc_panelHome);
				GridBagLayout gbl_panelHome = new GridBagLayout();
				gbl_panelHome.columnWidths = new int[]{0, 0};
				gbl_panelHome.rowHeights = new int[]{0, 0};
				gbl_panelHome.columnWeights = new double[]{0.0, Double.MIN_VALUE};
				gbl_panelHome.rowWeights = new double[]{0.0, Double.MIN_VALUE};
				panelHome.setLayout(gbl_panelHome);
				{
					btnHome = new JButton(""); //$NON-NLS-1$
					
					btnHome.addActionListener(new MiIndexActionListener());
					GridBagConstraints gbc_btnHome = new GridBagConstraints();
					gbc_btnHome.gridx = 0;
					gbc_btnHome.gridy = 0;
					panelHome.add(btnHome, gbc_btnHome);
					btnHome.setIcon(new ImageIcon(Index.class.getResource("/Recursos/home-26.png"))); //$NON-NLS-1$
				}
			}
			{
				lblLogotipo = new JLabel(""); //$NON-NLS-1$
				lblLogotipo.setIcon(new ImageIcon(Index.class.getResource("/Recursos/LogoPequeño.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_lblLogotipo = new GridBagConstraints();
				gbc_lblLogotipo.insets = new Insets(0, 0, 0, 5);
				gbc_lblLogotipo.gridx = 2;
				gbc_lblLogotipo.gridy = 0;
				panelMenu.add(lblLogotipo, gbc_lblLogotipo);
			}
			{
				{
					lblConectado = new JLabel(MessagesIndex.getString("Index.lblConectado.text")); //$NON-NLS-1$
					lblConectado.setAlignmentX(Component.RIGHT_ALIGNMENT);
					GridBagConstraints gbc_lblConectado = new GridBagConstraints();
					gbc_lblConectado.fill = GridBagConstraints.VERTICAL;
					gbc_lblConectado.insets = new Insets(0, 0, 0, 5);
					gbc_lblConectado.gridx = 3;
					gbc_lblConectado.gridy = 0;
					panelMenu.add(lblConectado, gbc_lblConectado);
				}
				{
					btnUsuario = new JButton(Login.admin.get(0).toUpperCase());
					btnUsuario.setFont(new Font("Tahoma", Font.BOLD, 12)); //$NON-NLS-1$
					btnUsuario.setBorder(null);
					btnUsuario.addActionListener(new MiAdminActionListener());
					GridBagConstraints gbc_btnUsuario = new GridBagConstraints();
					gbc_btnUsuario.fill = GridBagConstraints.BOTH;
					gbc_btnUsuario.insets = new Insets(0, 0, 0, 5);
					gbc_btnUsuario.gridx = 4;
					gbc_btnUsuario.gridy = 0;
					panelMenu.add(btnUsuario, gbc_btnUsuario);
				}
			}
			btnCerrarSesion = new JButton(MessagesIndex.getString("Index.btnCerrarSesion.text")); //$NON-NLS-1$
			btnCerrarSesion.addMouseListener(new BtnCerrarSesionMouseListener());
			btnCerrarSesion.setAlignmentX(Component.RIGHT_ALIGNMENT);
			GridBagConstraints gbc_btnCerrarSesion = new GridBagConstraints();
			gbc_btnCerrarSesion.gridx = 5;
			gbc_btnCerrarSesion.gridy = 0;
			panelMenu.add(btnCerrarSesion, gbc_btnCerrarSesion);
		}
		
	}

	public JFrame getFrameIndex() {
		return frmIndex;
	}
	
	public JPanel getPanelContenedor() {
		return panelContenedor;
	}
	
	public JFrame getFrame() {
		return frmIndex;
	}

	public void setFrame(JFrame frame) {
		this.frmIndex = frame;
	}
	
	////////////////
	// PANEL MENU //
	///////////////
	
	private class BtnAtrasActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
			cl.previous(getPanelContenedor());
			
		}
	}
	
	
		
	private class BtnCerrarSesionMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			try {
				actualizarUltimoAcceso();
				Persistencia.AgenteDB.desconectar();
				new Login().getFrame().setVisible(true);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			frmIndex.setVisible(false);
		}
	}
		
	////////////////////////////////
	// PANEL BUSCAR PARTICIPANTES //
	///////////////////////////////
	
	private class BtnCrearParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
			cl.show(getPanelContenedor(), "Participante"); //$NON-NLS-1$
					
			limpiarParticipante();
			editarParticipante(true);
			btnGuardarParticipanteParticipante.setEnabled(true);
			btnModificarParticipanteParticipante.setEnabled(false);
			btnEliminarParticipanteParticipante.setEnabled(false);
		}
	}
	
	private class BtnModificarParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (listParticipante.getSelectedIndex()==-1){
				JOptionPane.showMessageDialog(frmIndex,MessagesIndex.getString("Index.584"), "Error", JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			} else{
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "Participante"); //$NON-NLS-1$
				limpiarParticipante();
				int index =listParticipante.getSelectedIndex();
				modeloListaCompeticionParticipante.removeAllElements();
				actualizarCompeticionParticipante(index);
				mostrarParticipante(index);
				btnGuardarParticipanteParticipante.setEnabled(true);
				btnModificarParticipanteParticipante.setEnabled(false);
				btnEliminarParticipanteParticipante.setEnabled(false);
				
				editarParticipante(true);
			}
		}
	}
	
	private class BtnEliminarParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (listParticipante.getSelectedIndex()==-1){
				JOptionPane.showMessageDialog(frmIndex,MessagesIndex.getString("Index.587"), "Error", JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}else{
				int n = JOptionPane.showConfirmDialog(frmIndex, MessagesIndex.getString("Index.589"), MessagesIndex.getString("Index.590"), JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
				if (n==0){
					int index=listParticipante.getSelectedIndex();
					eliminarParticipante(index);
					modeloListaParticipante.removeAllElements();
					actualizarParticipantes();
				}
			}
		}
	}
	
	//Error al volver a pulsar un participante, es el ERROR 2 y 4
	private class BtnVerParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (listParticipante.getSelectedIndex()==-1){
				JOptionPane.showMessageDialog(frmIndex,MessagesIndex.getString("Index.591"), "Error", JOptionPane.INFORMATION_MESSAGE);  //$NON-NLS-1$ //$NON-NLS-2$
			} else{
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "Participante"); //$NON-NLS-1$
				limpiarParticipante();
				int index =listParticipante.getSelectedIndex();
				mostrarParticipante(index);
				
				btnGuardarParticipanteParticipante.setEnabled(false);
				btnModificarParticipanteParticipante.setEnabled(true);
				btnEliminarParticipanteParticipante.setEnabled(true);
				btnAnadirCompeticionParticipante.setEnabled(false);
				btnBorrarCompeticionParticipante.setEnabled(false);
				editarParticipante(false);
				modeloListaCompeticionParticipante.removeAllElements();
				actualizarCompeticionParticipante(index);
			}
		}
	}
	
	////////////////////////////////
	// PANEL BUSCAR COMPETICIONES //
	///////////////////////////////
	
	private class BtnCrearCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
			cl.show(getPanelContenedor(), "Competicion"); //$NON-NLS-1$
			limpiarCompeticion();
			editarCompeticion(true);
			btnGuardarCompeticionCompeticion.setEnabled(true);
			btnModificarCompeticionCompeticion.setEnabled(false);
			btnEliminarCompeticionCompeticion.setEnabled(false);
			btnVerInformacionCompeticion.setEnabled(false);
		}
	}
	
	private class BtnModificarCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (listCompeticion.getSelectedIndex()==-1){
				JOptionPane.showMessageDialog(frmIndex,MessagesIndex.getString("Index.595"), "Error", JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			} else{
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "Competicion"); //$NON-NLS-1$
				limpiarCompeticion();
				int index =listCompeticion.getSelectedIndex();
				mostrarCompeticion(index);
				btnEditarImagen.setEnabled(true);
				btnGuardarCompeticionCompeticion.setEnabled(true);
				btnModificarCompeticionCompeticion.setEnabled(false);
				btnEliminarCompeticionCompeticion.setEnabled(false);
				editarCompeticion(true);
			}
		}
	}
	
	private class BtnEliminarCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (listCompeticion.getSelectedIndex()==-1){
				JOptionPane.showMessageDialog(frmIndex,MessagesIndex.getString("Index.598"), "Error", JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}else{
				int n = JOptionPane.showConfirmDialog(frmIndex, MessagesIndex.getString("Index.600"), MessagesIndex.getString("Index.601"), JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
				if (n==0){
					int index=listCompeticion.getSelectedIndex();
					eliminarCompeticion(index);
					modeloListaCompeticion.removeAllElements();
					actualizarCompeticion();
				}
			}
		}
	}
	private class BtnVerCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (listCompeticion.getSelectedIndex()==-1){
				JOptionPane.showMessageDialog(frmIndex,MessagesIndex.getString("Index.602"), "Error", JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			} else{
				
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "Competicion"); //$NON-NLS-1$
				limpiarCompeticion();
				int index =listCompeticion.getSelectedIndex();
				mostrarCompeticion(index);
				btnVerInformacionCompeticion.setEnabled(true);
				btnGuardarCompeticionCompeticion.setEnabled(false);
				btnModificarCompeticionCompeticion.setEnabled(true);
				btnEliminarCompeticionCompeticion.setEnabled(true);
				editarCompeticion(false);
			}
		}
	}
	
	////////////////////////
	// PANEL PARTICIPANTE //
	///////////////////////
	
	private class BtnGuardarParticipanteParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			guardarParticipante();
			btnGuardarParticipanteParticipante.setEnabled(false);
			btnModificarParticipanteParticipante.setEnabled(true);
			btnEliminarParticipanteParticipante.setEnabled(true);
			btnAnadirCompeticionParticipante.setEnabled(false);
			btnBorrarCompeticionParticipante.setEnabled(false);
			editarParticipante(false);
			//JOptionPane.showMessageDialog(frmIndex,"Los datos se han guardado","Información", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	private class BtnModificarParticipanteParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			editarParticipante(true);
			btnGuardarParticipanteParticipante.setEnabled(true);
			btnAnadirCompeticionParticipante.setEnabled(true);
			btnBorrarCompeticionParticipante.setEnabled(true);
			btnEliminarParticipanteParticipante.setEnabled(false);
			btnModificarParticipanteParticipante.setEnabled(false);
			
			
		}
	}
	
	private class BtnEliminarParticipanteParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int n = JOptionPane.showConfirmDialog(frmIndex, MessagesIndex.getString("Index.605"), MessagesIndex.getString("Index.606"), JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
			if(n==0){
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "Home"); //$NON-NLS-1$
				eliminarParticipante(txtDNIParticipante.getText());	
				editarParticipante(false);
				limpiarParticipante();
				JOptionPane.showMessageDialog(frmIndex, MessagesIndex.getString("Index.608"), MessagesIndex.getString("Index.609"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}	
		}
	}
	
	private class BtnAnadirCompeticionParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
			cl.show(getPanelContenedor(), "NuevaCompeticionParticipante"); //$NON-NLS-1$
			dniParticipante=txtDNIParticipante.getText();
			modeloListaCompeticionNuevaCompeticionParticipante.removeAllElements();
			modeloListaParticipanteNuevaCompeticionParticipante.removeAllElements();
			actualizarCompeticionNuevaCompeticionParticipante();
			actualizarParticipanteNuevaCompeticionParticipante();
		}
	}
	
	private class BtnBorrarCompeticionParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int n = JOptionPane.showConfirmDialog(frmIndex, MessagesIndex.getString("Index.611"), MessagesIndex.getString("Index.612"), JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
			if(n==0){
				int index =listCompeticionParticipante.getSelectedIndex();
				eliminarCompeticionParticipante(index, txtDNIParticipante.getText());
				modeloListaCompeticionParticipante.removeAllElements();
				actualizarCompeticionParticipante(txtDNIParticipante.getText());
				JOptionPane.showMessageDialog(frmIndex, MessagesIndex.getString("Index.613"), MessagesIndex.getString("Index.614"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}	
		}
	}
	
	///////////////////////
	// PANEL COMPETICION //
	//////////////////////
	
	private class BtnGuardarCompeticionCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			guardarCompeticion();
			btnGuardarCompeticionCompeticion.setEnabled(false);
			btnModificarCompeticionCompeticion.setEnabled(true);
			btnEliminarCompeticionCompeticion.setEnabled(true);
			btnEditarImagen.setEnabled(false);
			editarCompeticion(false);

			//JOptionPane.showMessageDialog(frmIndex,"Los datos se han guardado","Información", JOptionPane.INFORMATION_MESSAGE);
			
		}
	}
	
	private class BtnModificarCompeticionCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			editarCompeticion(true);
			btnGuardarCompeticionCompeticion.setEnabled(true);
			btnModificarCompeticionCompeticion.setEnabled(false);
			btnEliminarCompeticionCompeticion.setEnabled(false);
			btnEditarImagen.setEnabled(true);

			
		}
	}
	
	private class BtnEliminarCompeticionCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int n = JOptionPane.showConfirmDialog(frmIndex, MessagesIndex.getString("Index.615"), MessagesIndex.getString("Index.616"), JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
			if(n==0){
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "Home"); //$NON-NLS-1$
				eliminarCompeticion(txtIdCompeticion.getText());	
				editarCompeticion(false);
				limpiarCompeticion();
				JOptionPane.showMessageDialog(frmIndex, MessagesIndex.getString("Index.618"), MessagesIndex.getString("Index.619"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}
			
		}
	}
	

	private class BtnVerInformacionCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try{
				
			Persistencia.AgenteDB.desconectar();
			btnGaleriaPodioCompeticion.setEnabled(false);
			String estado = (String) cbEstadoCompeticion.getSelectedItem();
			if(estado.compareTo("Finalizada") == 0){ //$NON-NLS-1$
				Persistencia.AgenteDB.conectar();
				btnGaleriaPodioCompeticion.setEnabled(true);
				lblClasificacion.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.621")))); //$NON-NLS-1$
				lblPodio.setIcon(new ImageIcon(Index.class.getResource("/Recursos/podio.png"))); //$NON-NLS-1$
			}else{
				Persistencia.AgenteDB.conectar();
				lblPodio.setIcon(new ImageIcon(Index.class.getResource("/Recursos/fondonopodio.png"))); //$NON-NLS-1$
				lblClasificacion.setIcon(new ImageIcon(Index.class.getResource(MessagesIndex.getString("Index.624")))); //$NON-NLS-1$
				//lblPodio.setIcon(null);
			}
			String index = txtIdCompeticion.getText();
			
			CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
			cl.show(getPanelContenedor(), "PodioCompeticion"); //$NON-NLS-1$
			modeloListaParticipantePodioCompeticion.removeAllElements();
			actualizarParticipantePodioCompeticion(index);
			
			
			
			}
			catch(Exception ex){
				
			}
		}
	}
	
	private class BtnEditarImagenActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			EditorGrafico graf=new EditorGrafico();
			graf.getFrame().setVisible(true);
			graf.getFrame().setLocationRelativeTo(frmIndex);
		}
	}
	
	
	//////////////////////////////
	// PANEL PODIO COMPETICION  //
	//////////////////////////////
	
	private class BtnAñadirpodiocompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
			cl.show(getPanelContenedor(), "NuevaCompeticionParticipante"); //$NON-NLS-1$
			dniParticipante=txtDNIParticipante.getText();
			modeloListaCompeticionNuevaCompeticionParticipante.removeAllElements();
			modeloListaParticipanteNuevaCompeticionParticipante.removeAllElements();
			actualizarCompeticionNuevaCompeticionParticipante();
			actualizarParticipanteNuevaCompeticionParticipante();
		}
	}
	private class BtnBorrarPodioCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int n = JOptionPane.showConfirmDialog(frmIndex, MessagesIndex.getString("Index.627"), MessagesIndex.getString("Index.628"), JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
			if(n==0){
				int index =listParticipantesPodioCompeticion.getSelectedIndex();
				eliminarParticipantePodioCompeticion(index);
				modeloListaParticipantePodioCompeticion.removeAllElements();
				actualizarCompeticionParticipante(idCompeticion);
				JOptionPane.showMessageDialog(frmIndex, MessagesIndex.getString("Index.629"), MessagesIndex.getString("Index.630"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}
	
	private class BtnGaleriaPodioCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Galeria galeria=new Galeria();
			galeria.getFrame().setVisible(true);
			galeria.getFrame().setLocationRelativeTo(frmIndex);
		}
	}
	
	//////////////////////////////////////////
	// PANEL NUEVA COMPETICION PARTICIPANTE //
	/////////////////////////////////////////
	
	private class ListCompeticionNuevaCompeticionParticipanteListSelectionListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent arg0) {
			int index = listCompeticionNuevaCompeticionParticipante.getSelectedIndex();
        	String competicion = (String) listCompeticionNuevaCompeticionParticipante.getModel().getElementAt(index);
    		String[] texto=competicion.split(" "); //$NON-NLS-1$
    		String id = texto[0];
    		txtCompeticionCP.setText(id);
    		
			
		}
	}
	private class ListParticipanteNuevaCompeticionParticipanteListSelectionListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			int index = listParticipanteNuevaCompeticionParticipante.getSelectedIndex();
        	String participante = (String) listParticipanteNuevaCompeticionParticipante.getModel().getElementAt(index);
    		String[] texto=participante.split(" "); //$NON-NLS-1$
    		String dni = texto[0];
    		txtParticipanteCP.setText(dni);
    		
			
		}
	}

	private class BtnGuardarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(txtPosicionCP.getText().compareTo("") == 0){ //$NON-NLS-1$
				txtPosicionCP.setText("X"); //$NON-NLS-1$
			}
			System.out.println(txtPosicionCP.getText());
			if( txtParticipanteCP.getText().compareTo("") == 0 || txtCompeticionCP.getText().compareTo("")== 0){ //$NON-NLS-1$ //$NON-NLS-2$
				JOptionPane.showMessageDialog(null, MessagesIndex.getString("Index.637")); //$NON-NLS-1$
			}else{
				try{
					try{
						String sql ="Insert into CompeticionesParticipantes (dniParticipante,idCompeticion,posicion,tiempoTotal,seccion1,seccion2,seccion3,seccion4,seccion5) values (?,?,?,?,?,?,?,?,?)";   //$NON-NLS-1$
						PreparedStatement pst = connection.prepareStatement(sql);
						pst.setString(1, txtParticipanteCP.getText());
						pst.setString(2, txtCompeticionCP.getText());
						pst.setString(3, txtPosicionCP.getText());
						pst.setString(4, txtTiempoTotalCP.getText());
						pst.setString(5, txtSeccion1CP.getText());
						pst.setString(6, txtSeccion2CP.getText());
						pst.setString(7, txtSeccion3CP.getText());
						pst.setString(8, txtSeccion4CP.getText());
						pst.setString(9, txtSeccion5CP.getText());
						pst.execute();
						JOptionPane.showMessageDialog(null, MessagesIndex.getString("Index.639")); //$NON-NLS-1$
						pst.close();
					}catch(Exception e1){
						String participanteCP = txtParticipanteCP.getText();
						String competicionCP =  txtCompeticionCP.getText();
						String tiempoTotalCP = txtTiempoTotalCP.getText();
						String posicionCP = txtPosicionCP.getText(); 
						String seccion1CP = txtSeccion1CP.getText();
						String seccion2CP = txtSeccion2CP.getText();
						String seccion3CP = txtSeccion3CP.getText();
						String seccion4CP = txtSeccion4CP.getText();
						String seccion5CP =  txtSeccion5CP.getText();
						String sql="update CompeticionesParticipantes set dniParticipante='"+participanteCP+"' ,idCompeticion ='"+competicionCP+"', posicion ='"+posicionCP+"', tiempoTotal ='"+tiempoTotalCP+"', seccion1 ='"+seccion1CP+"', seccion2 ='"+seccion2CP+"', seccion3 ='"+seccion3CP+"', seccion4 ='"+seccion4CP+"', seccion5 ='"+seccion5CP+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
						PreparedStatement pst = connection.prepareStatement(sql);
						pst.execute();
						pst.close();
						JOptionPane.showMessageDialog(null, MessagesIndex.getString("Index.650")); //$NON-NLS-1$
					}
				}catch (Exception e2) {
					JOptionPane.showMessageDialog(null, e2);
				}
			}
		}
	}
	
	
	//////////////////
	// PANEL ADMIN //
	////////////////
	
	private class BtnCrearAdminAdminActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			limpiarAdmin();
			editarAdmin(true);
			btnGuardarAdminAdmin.setEnabled(true);
			btnModificarAdminAdmin.setEnabled(false);
			btnEliminarAdminAdmin.setEnabled(false);
			btnCrearAdminAdmin.setEnabled(false);
		}
	}
	
	private class BtnGuardarAdminActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if((txtContraseñaAdmin.getText()).compareTo(txtRepetirContraseñaAdmin.getText()) !=0 ){
				JOptionPane.showMessageDialog(null, MessagesIndex.getString("Index.651"));	 //$NON-NLS-1$
			}else{
				guardarAdmin();
				btnGuardarAdminAdmin.setEnabled(false);
				btnModificarAdminAdmin.setEnabled(true);
				btnEliminarAdminAdmin.setEnabled(true);
				btnCrearAdminAdmin.setEnabled(true);
				//JOptionPane.showMessageDialog(frmIndex,"Los datos se han guardado","Información", JOptionPane.INFORMATION_MESSAGE);
			}
			
		}
	}
	
	private class BtnModificarAdminActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			editarAdmin(true);
			btnGuardarAdminAdmin.setEnabled(true);
			btnModificarAdminAdmin.setEnabled(false);
			btnEliminarAdminAdmin.setEnabled(false);
			btnCrearAdminAdmin.setEnabled(false);
		}
	}
	
	private class BtnEliminarAdminActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int n = JOptionPane.showConfirmDialog(frmIndex, MessagesIndex.getString("Index.652"), MessagesIndex.getString("Index.653"), JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
			if(n==0){
				eliminarAdmin();	
				editarAdmin(false);
				JOptionPane.showMessageDialog(frmIndex, MessagesIndex.getString("Index.654"), MessagesIndex.getString("Index.655"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
				try {
					new Login().getFrame().setVisible(true);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1);
				}
			}
			frmIndex.setVisible(false);
		}
	}
	
	//////////////
	// MENU BAR //
	/////////////
	
	private class MntmCerrarSesionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				actualizarUltimoAcceso();
				Persistencia.AgenteDB.desconectar();
				new Login().getFrame().setVisible(true);
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, e1);
			}
			frmIndex.setVisible(false);
		}
	}
	private class MntmSalirActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			actualizarUltimoAcceso();
			JOptionPane.showMessageDialog(frmIndex, MessagesIndex.getString("Index.656")); //$NON-NLS-1$
			System.exit(0);
		}
	}
	private class MntmIdiomaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			SeleccionIdioma select = new SeleccionIdioma();
			select.main(null);
		    frmIndex.dispose();
		}
	}
	private class RdbtnmntmPequenaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("Tahoma", Font.BOLD, 8));
			setUIFont (new javax.swing.plaf.FontUIResource(new Font("Tahoma",Font.PLAIN, 10))); //$NON-NLS-1$
			SwingUtilities.updateComponentTreeUI(frmIndex);  // update components
			frmIndex.pack();
		}
	}
	private class RdbtnmntmNormalActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("Times New Roman", Font.PLAIN, 10));
			setUIFont (new javax.swing.plaf.FontUIResource(new Font("Tahoma",Font.PLAIN, 11))); //$NON-NLS-1$
			SwingUtilities.updateComponentTreeUI(frmIndex);  // update components
			frmIndex.pack();
		}
	}
	private class RdbtnmntmGrandeActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("Verdana", Font.ITALIC, 12));
			setUIFont (new javax.swing.plaf.FontUIResource(new Font("Tahoma",Font.PLAIN, 12))); //$NON-NLS-1$
			SwingUtilities.updateComponentTreeUI(frmIndex);  // update components
			frmIndex.pack();
		}
	}
	private class MntmManualUsuarioActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (Desktop.isDesktopSupported()) {
			    try {
			        File myFile = new File("Manual.pdf");  //$NON-NLS-1$
			        Desktop.getDesktop().open(myFile);
			    } catch (IOException ex) {
			    	JOptionPane.showMessageDialog(frmIndex,MessagesIndex.getString("Index.661"), MessagesIndex.getString("Index.662"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			    }
			}
		
		}
	}

	private class MntmAcercaDeActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(null,MessagesIndex.getString("Index.663"), MessagesIndex.getString("Index.664"),JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			
		}
	}

	//////////////////
	//   OYENTES   //
	////////////////
		
	//Inicio
	private class MiIndexActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			//Posible solucion
			//idCompeticion = null;
			//dniParticipante = null;
			//Posible Solucion
			try {
								
				Persistencia.AgenteDB.conectar();
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "Index"); //$NON-NLS-1$
				
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);
			}
			
		}
	}
	
	//Participantes
	private class MiParticipanteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Persistencia.AgenteDB.conectar();
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "BuscarParticipante"); //$NON-NLS-1$
				modeloListaParticipante.removeAllElements();
				actualizarParticipantes();
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, e1);
			}
			
		}
	}
	
	//Competiciones
	private class MiCompeticionActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Persistencia.AgenteDB.conectar();
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "BuscarCompeticion"); //$NON-NLS-1$
				modeloListaCompeticion.removeAllElements();
				actualizarCompeticion();
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, e1);
			}
			
		}
	}
	
	//Administrador
	private class MiAdminActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
			cl.show(getPanelContenedor(), "Admin"); //$NON-NLS-1$
	        try{
	        	String query = "select * from Admin where usuario ='"+Login.admin.get(0)+"' and password='"+Login.admin.get(1)+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		         PreparedStatement pst = connection.prepareStatement(query);
		         ResultSet rs=pst.executeQuery();
		         if(rs.next()){
		        	 txtNombreAdmin.setText(rs.getString("nombre")); //$NON-NLS-1$
		        	 txtApellidosAdmin.setText(rs.getString("apellidos")); //$NON-NLS-1$
		        	 txtDNIAdmin.setText(rs.getString("dni"));  //$NON-NLS-1$
		        	 String fechaNacimiento = rs.getString("fechaNacimiento"); //$NON-NLS-1$
		        	 String[] fecha=fechaNacimiento.split("/"); //$NON-NLS-1$
		        	 cbFechaNacimientoDiaAdmin.setSelectedItem(fecha[0]);
		        	 cbFechaNacimientoMesAdmin.setSelectedItem(fecha[1]);
		        	 cbFechaNacimientoAnoAdmin.setSelectedItem(fecha[2]);
		        	 cbSexoAdmin.setSelectedItem(rs.getString("sexo")); //$NON-NLS-1$
		        	 cbDireccionAdmin.setSelectedItem(rs.getString("tipoVia")); //$NON-NLS-1$
		        	 txtDireccionAdmin.setText(rs.getString("direccion")); //$NON-NLS-1$
		        	 txtNumeroAdmin.setText(rs.getString("nCalle")); //$NON-NLS-1$
		        	 cbEscaleraAdmin.setSelectedItem(rs.getString("escalera")); //$NON-NLS-1$
		        	 txtPisoAdmin.setText(rs.getString("piso")); //$NON-NLS-1$
		        	 txtPuertaAdmin.setText(rs.getString("puerta")); //$NON-NLS-1$
		        	 txtCodigoPostalAdmin.setText(rs.getString("codigoPostal")); //$NON-NLS-1$
		        	 txtProvinciaAdmin.setText(rs.getString("provincia")); //$NON-NLS-1$
		        	 txtLocalidadAdmin.setText(rs.getString("localidad")); //$NON-NLS-1$
		        	 txtPaisAdmin.setText(rs.getString("pais")); //$NON-NLS-1$
		        	 txtTelefonoAdmin.setValue(rs.getString("telefono")); //$NON-NLS-1$
		        	 txtCorreoElectronicoAdmin.setText(rs.getString("correoElectronico")); 	 //$NON-NLS-1$
		        	 txtUsuarioAdmin.setText(rs.getString("usuario"));  //$NON-NLS-1$
		        	 txtContraseñaAdmin.setText(rs.getString("password"));  //$NON-NLS-1$
		        	 lblFechaUltimoAccesoMostrarAdmin.setText(rs.getString("fechaUltimoAcceso")); //$NON-NLS-1$
		        	 
		         }
		         rs.close();
	        	 pst.close();
	        }catch(Exception e1){
		        JOptionPane.showMessageDialog(null, e1);
	        }
	        editarAdmin(false);
	        btnGuardarAdminAdmin.setEnabled(false);
			btnModificarAdminAdmin.setEnabled(true);
			btnEliminarAdminAdmin.setEnabled(true);
			btnCrearAdminAdmin.setEnabled(true);
		}
	}
	
	
	/////////////////
	//   METODOS  //
	////////////////
	
	//Actualizadores
	
	public void actualizarParticipantes(){
		try{
		   String sql ="select * from Participantes  "; //$NON-NLS-1$
		   PreparedStatement pst = connection.prepareStatement(sql);
		   ResultSet rs=pst.executeQuery();
		   while(rs.next())
	            modeloListaParticipante.addElement(rs.getString("dni")+ " " +rs.getString("nombre")+ " " +rs.getString("apellidos")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
	        listParticipante.setModel(modeloListaParticipante);
	        rs.close();
			pst.close();
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, e);
			}
	}

	public void actualizarCompeticion(){
		try{
			String sql ="select * from Competiciones  "; //$NON-NLS-1$
			PreparedStatement pst = connection.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();  
			while(rs.next())
				modeloListaCompeticion.addElement(rs.getString("id")+ " " +rs.getString("nombre")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		    listCompeticion.setModel(modeloListaCompeticion);
		    rs.close();
			pst.close();
			}catch(Exception e){
			    JOptionPane.showMessageDialog(null, e);
			}
	}
	
	public void actualizarCompeticionParticipante(int index){
		try{
			Persistencia.AgenteDB.conectar();
			String participante = (String) listParticipante.getModel().getElementAt(index);
			String[] texto=participante.split(" "); //$NON-NLS-1$
			String dni = texto[0];
			String sql ="select * from CompeticionesParticipantes cp, Competiciones c where cp.idCompeticion=c.id and cp.dniParticipante='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$
			PreparedStatement pst = connection.prepareStatement(sql);
			ResultSet rs=pst.executeQuery(); 
			while(rs.next())
				modeloListaCompeticionParticipante.addElement(rs.getString("id")+ " " +rs.getString("nombre")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		    listCompeticionParticipante.setModel(modeloListaCompeticionParticipante);
		    rs.close();
			pst.close();
			Persistencia.AgenteDB.desconectar();
			}catch(Exception e){
			    JOptionPane.showMessageDialog(null, e);
			}
	}
	
	public void actualizarCompeticionParticipante(String dni){
		try{
			Persistencia.AgenteDB.conectar();
			String sql ="select * from CompeticionesParticipantes cp, Competiciones c where cp.idCompeticion=c.id and cp.dniParticipante='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$
			PreparedStatement pst = connection.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();  
			while(rs.next())
				modeloListaCompeticionParticipante.addElement(rs.getString("id")+ " " +rs.getString("nombre")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		    listCompeticionParticipante.setModel(modeloListaCompeticionParticipante);
		    rs.close();
			pst.close();
			Persistencia.AgenteDB.desconectar();
			}catch(Exception e){
			    JOptionPane.showMessageDialog(null, e);
			}
	}
	
	public void actualizarParticipantePodioCompeticion(String index){
		try{
			Persistencia.AgenteDB.conectar();
			String sql ="select * from CompeticionesParticipantes cp, Participantes p where cp.dniParticipante=p.dni and cp.idCompeticion='"+index+"' "; //$NON-NLS-1$ //$NON-NLS-2$
			PreparedStatement pst = connection.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			while(rs.next()){
						        
	        	modeloListaParticipantePodioCompeticion.addElement(rs.getString("posicion")+ " " +rs.getString("dni")+ " " +rs.getString("nombre")+ " " +rs.getString("apellidos")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
			}
			listParticipantesPodioCompeticion.setModel(modeloListaParticipantePodioCompeticion);
		    rs.close();
			pst.close();
			Persistencia.AgenteDB.desconectar();
			}catch(Exception e){
			    JOptionPane.showMessageDialog(null, e);
			}
	}
	
	public void actualizarCompeticionNuevaCompeticionParticipante(){
		try{
			String sql ="select * from Competiciones  "; //$NON-NLS-1$
			PreparedStatement pst = connection.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
				modeloListaCompeticionNuevaCompeticionParticipante.addElement(rs.getString("id")+ " " +rs.getString("nombre")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			listCompeticionNuevaCompeticionParticipante.setModel(modeloListaCompeticionNuevaCompeticionParticipante);
			rs.close();
			pst.close();
			}catch(Exception e){
			    JOptionPane.showMessageDialog(null, e);
			}
	}
	
	public void actualizarParticipanteNuevaCompeticionParticipante(){
		try{
			String sql ="select * from Participantes  "; //$NON-NLS-1$
			PreparedStatement pst = connection.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
				modeloListaParticipanteNuevaCompeticionParticipante.addElement(rs.getString("dni")+ " " +rs.getString("nombre")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			listParticipanteNuevaCompeticionParticipante.setModel(modeloListaParticipanteNuevaCompeticionParticipante);
			rs.close();
			pst.close();
			}catch(Exception e){
			    JOptionPane.showMessageDialog(null, e);
			}
	}
	
	
	//Participantes
	
	public void eliminarParticipante(int index){
		String participante = (String) listParticipante.getModel().getElementAt(index);
		String[] texto=participante.split(" "); //$NON-NLS-1$
		String dni = texto[0];
        try{
        	String sql ="delete from Participantes where dni='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$
        	PreparedStatement pst = connection.prepareStatement(sql);
        	pst.execute();
			pst.close();
        }catch(Exception e){
        	JOptionPane.showMessageDialog(null, e);
        }
	}
	
	public void eliminarParticipante(String dni){
        try{
        	String sql ="delete from Participantes where dni='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$
        	PreparedStatement pst = connection.prepareStatement(sql);
        	pst.execute();
			pst.close();
        }catch(Exception e){
        	JOptionPane.showMessageDialog(null, e);
        }
	}
	
	public void mostrarParticipante(int index){
		String participante = (String) listParticipante.getModel().getElementAt(index);
		String[] texto=participante.split(" "); //$NON-NLS-1$
		String dni = texto[0];
		
        try{
        	String sql ="select * from Participantes where dni='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$
	         PreparedStatement pst = connection.prepareStatement(sql);
	         ResultSet rs=pst.executeQuery();
	         if(rs.next()){
	        	 txtNombreParticipante.setText(rs.getString("nombre")); //$NON-NLS-1$
	        	 txtApellidosParticipante.setText(rs.getString("apellidos")); //$NON-NLS-1$
	        	 txtDNIParticipante.setText(rs.getString("dni"));   //$NON-NLS-1$
	        	 String fechaNacimiento = rs.getString("fechaNacimiento"); //$NON-NLS-1$
	        	 String[] fecha=fechaNacimiento.split("/"); //$NON-NLS-1$
	        	 cbFechaNacimientoDiaParticipante.setSelectedItem(fecha[0]);
	        	 cbFechaNacimientoMesParticipante.setSelectedItem(fecha[1]);
	        	 cbFechaNacimientoAnoParticipante.setSelectedItem(fecha[2]);
	        	 cbSexoParticipante.setSelectedItem(rs.getString("sexo")); //$NON-NLS-1$
	        	 cbDireccionParticipante.setSelectedItem(rs.getString("tipoVia")); //$NON-NLS-1$
	        	 txtDireccionParticipante.setText(rs.getString("direccion")); //$NON-NLS-1$
	        	 txtNumeroParticipante.setText(rs.getString("nCalle")); //$NON-NLS-1$
	        	 cbEscaleraParticipante.setSelectedItem(rs.getString("escalera")); //$NON-NLS-1$
	        	 txtPisoParticipante.setText(rs.getString("piso")); //$NON-NLS-1$
	        	 txtPuertaParticipante.setText(rs.getString("puerta")); //$NON-NLS-1$
	        	 txtCodigoPostalParticipante.setText(rs.getString("codigoPostal")); //$NON-NLS-1$
	        	 txtProvinciaParticipante.setText(rs.getString("provincia")); //$NON-NLS-1$
	        	 txtLocalidadParticipante.setText(rs.getString("localidad")); //$NON-NLS-1$
	        	 txtPaisParticipante.setText(rs.getString("pais")); //$NON-NLS-1$
	        	 txtTelefonoParticipante.setValue(rs.getString("telefono")); //$NON-NLS-1$
	        	 txtCorreoElectronicoParticipante.setText(rs.getString("correoElectronico")); //$NON-NLS-1$
	        	 cbTallaParticipante.setSelectedItem(rs.getString("talla")); //$NON-NLS-1$
	        	 cbTipoParticipante.setSelectedItem(rs.getString("tipo"));  //$NON-NLS-1$
	        	
	          }
	         rs.close();
				pst.close();
        }catch(Exception e){
	        JOptionPane.showMessageDialog(null, e);
        }	
	}
	
	//Completar las sentencias sql
	public void guardarParticipante(){
		try{
			try{
				String sql ="Insert into Participantes (nombre,apellidos,dni,fechaNacimiento,sexo,tipoVia,direccion,nCalle,escalera,piso,puerta,codigoPostal,provincia,localidad,pais,telefono,correoElectronico,talla,tipo) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";   //$NON-NLS-1$
				PreparedStatement pst = connection.prepareStatement(sql);
				pst.setString(1, txtNombreParticipante.getText());
				pst.setString(2, txtApellidosParticipante.getText());
				pst.setString(3, txtDNIParticipante.getText());
				String diaparticipante= cbFechaNacimientoDiaParticipante.getSelectedItem().toString();
				String mesparticipante= cbFechaNacimientoMesParticipante.getSelectedItem().toString();
				String anoparticipante= cbFechaNacimientoAnoParticipante.getSelectedItem().toString();
				String fecha = diaparticipante+"/"+mesparticipante+"/"+anoparticipante; //$NON-NLS-1$ //$NON-NLS-2$
				pst.setString(4, fecha);
				pst.setString(5, cbSexoParticipante.getSelectedItem().toString());
				pst.setString(6, cbDireccionParticipante.getSelectedItem().toString());
				pst.setString(7, txtDireccionParticipante.getText());
				pst.setString(8, txtNumeroParticipante.getText());
				pst.setString(9, cbEscaleraParticipante.getSelectedItem().toString());
				pst.setString(10, txtPisoParticipante.getText());
				pst.setString(11, txtPuertaParticipante.getText());
				pst.setString(12, txtCodigoPostalParticipante.getText());
				pst.setString(13, txtProvinciaParticipante.getText());
				pst.setString(14, txtLocalidadParticipante.getText());
				pst.setString(15, txtPaisParticipante.getText());
				pst.setString(16, txtTelefonoParticipante.getText());
				pst.setString(17, txtCorreoElectronicoParticipante.getText());
				pst.setString(18, cbTallaParticipante.getSelectedItem().toString());
				pst.setString(19, cbTipoParticipante.getSelectedItem().toString());
				pst.execute();
				pst.close();
				JOptionPane.showMessageDialog(null, MessagesIndex.getString("Index.762"));	 //$NON-NLS-1$
				
			}catch(Exception e){
				String nombre= txtNombreParticipante.getText();
				String apellidos= txtApellidosParticipante.getText();
				String dni= txtDNIParticipante.getText();
				String diaparticipante= cbFechaNacimientoDiaParticipante.getSelectedItem().toString();
				String mesparticipante= cbFechaNacimientoMesParticipante.getSelectedItem().toString();
				String anoparticipante= cbFechaNacimientoAnoParticipante.getSelectedItem().toString();
				String fecha = diaparticipante+"/"+mesparticipante+"/"+anoparticipante; //$NON-NLS-1$ //$NON-NLS-2$
				String sexo= cbSexoParticipante.getSelectedItem().toString();
				String cbdireccionparticipante= cbDireccionParticipante.getSelectedItem().toString();
				String txtdireccionparticipante= txtDireccionParticipante.getText();
				String numeroparticipante= txtNumeroParticipante.getText();
				String escaleraparticipante= cbEscaleraParticipante.getSelectedItem().toString();
				String pisoparticipante= txtPisoParticipante.getText();
				String puertaparticipante= txtPuertaParticipante.getText();
				String codigopostalparticipante= txtCodigoPostalParticipante.getText();
				String localidadparticipante= txtLocalidadParticipante.getText();
				String paisparticipante= txtPaisParticipante.getText();
				String provinciaparticipante= txtProvinciaParticipante.getText();
				String tlfparticipante= txtTelefonoParticipante.getText();
				String emailparticipante= txtCorreoElectronicoParticipante.getText();
				String tallaParticipante= cbTallaParticipante.getSelectedItem().toString();
				String tipoParticipante= cbTipoParticipante.getSelectedItem().toString();
				
				String sql="update Participantes set nombre='"+nombre+"' ,apellidos ='"+apellidos+"',dni ='"+dni+"',fechaNacimiento='"+fecha+"',fechaNacimiento='"+fecha+"',fechaNacimiento='"+fecha+"',sexo='"+sexo+"',tipoVia='"+cbdireccionparticipante+"',direccion='"+txtdireccionparticipante+"',nCalle='"+numeroparticipante+"',escalera='"+escaleraparticipante+"',piso='"+pisoparticipante+"',puerta='"+puertaparticipante+"',codigoPostal='"+codigopostalparticipante+"',localidad='"+localidadparticipante+"',pais='"+paisparticipante+"',provincia='"+provinciaparticipante+"',telefono='"+tlfparticipante+"',correoElectronico='"+emailparticipante+"',talla='"+tallaParticipante+"',tipo='"+tipoParticipante+"' where dni='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$
				PreparedStatement pst = connection.prepareStatement(sql);
				pst.execute();
				pst.close();
				JOptionPane.showMessageDialog(null, MessagesIndex.getString("Index.788")); //$NON-NLS-1$
			}	
		}catch(Exception e1){
	        JOptionPane.showMessageDialog(null, e1);
        }
	}
	
	
	public void editarParticipante (boolean editable){
		txtNombreParticipante.setEditable(editable);
		txtApellidosParticipante.setEditable(editable);
		txtDNIParticipante.setEditable(editable);
		cbFechaNacimientoDiaParticipante.setEditable(false);
		cbFechaNacimientoDiaParticipante.setEnabled(editable);
		cbFechaNacimientoMesParticipante.setEditable(false);
		cbFechaNacimientoMesParticipante.setEnabled(editable);
		cbFechaNacimientoAnoParticipante.setEditable(false);
		cbFechaNacimientoAnoParticipante.setEnabled(editable);
		cbSexoParticipante.setEditable(false);
		cbSexoParticipante.setEnabled(editable);
		cbDireccionParticipante.setEditable(false);
		cbDireccionParticipante.setEnabled(editable);
		txtDireccionParticipante.setEditable(editable);
		txtNumeroParticipante.setEditable(editable);
		cbEscaleraParticipante.setEditable(false);
		cbEscaleraParticipante.setEnabled(editable);
		cbTallaParticipante.setEditable(false);
		cbTallaParticipante.setEnabled(editable);
		cbTipoParticipante.setEnabled(editable);
		cbTipoParticipante.setEditable(false);
		txtPisoParticipante.setEditable(editable);
		txtPuertaParticipante.setEditable(editable);
		txtCodigoPostalParticipante.setEditable(editable);
		txtLocalidadParticipante.setEditable(editable);
		txtPaisParticipante.setEditable(editable);
		txtProvinciaParticipante.setEditable(editable);
		txtTelefonoParticipante.setEditable(editable);
		txtCorreoElectronicoParticipante.setEditable(editable);
		cbTallaParticipante.setEditable(false);
		cbTallaParticipante.setEditable(editable);
		cbTipoParticipante.setEditable(false);
		cbTipoParticipante.setEditable(editable);
	}
	
	//Añadir los atributos que falten
	public void limpiarParticipante(){
		txtNombreParticipante.setText("");  //$NON-NLS-1$
		txtApellidosParticipante.setText("");  //$NON-NLS-1$
		txtDNIParticipante.setText("");  //$NON-NLS-1$
		txtDireccionParticipante.setText("");  //$NON-NLS-1$
		txtNumeroParticipante.setText("");  //$NON-NLS-1$
		txtPisoParticipante.setText("");  //$NON-NLS-1$
		txtPuertaParticipante.setText("");  //$NON-NLS-1$
		txtCodigoPostalParticipante.setText("");  //$NON-NLS-1$
		txtLocalidadParticipante.setText("");  //$NON-NLS-1$
		txtPaisParticipante.setText("");  //$NON-NLS-1$
		txtProvinciaParticipante.setText("");  //$NON-NLS-1$
		txtTelefonoParticipante.setText("");  //$NON-NLS-1$
		txtCorreoElectronicoParticipante.setText(""); //$NON-NLS-1$
		taResumenCompeticionParticipante.setText(""); //$NON-NLS-1$
	}
	
	public void eliminarCompeticionParticipante(int index, String dni){
		String competicion = (String) listCompeticionParticipante.getModel().getElementAt(index);
		String[] texto=competicion.split(" "); //$NON-NLS-1$
		String id = texto[0];
		try{
        	String sql ="delete from CompeticionesParticipantes where idCompeticion='"+id+"' and dniParticipante='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        	PreparedStatement pst = connection.prepareStatement(sql);
        	pst.execute();
			pst.close();
        }catch(Exception e){
        	JOptionPane.showMessageDialog(null, e);
        }
		
	}
	
	//Competicion
	
	public void eliminarCompeticion(int index){
		String competicion = (String) listCompeticion.getModel().getElementAt(index);
		String[] texto=competicion.split(" "); //$NON-NLS-1$
		String id = texto[0];
        try{
        	String sql ="delete from Competiciones where id='"+id+"' "; //$NON-NLS-1$ //$NON-NLS-2$
        	PreparedStatement pst = connection.prepareStatement(sql);
        	pst.execute();
			pst.close();
        }catch(Exception e){
        	JOptionPane.showMessageDialog(null, e);
        }
	}
	
	public void eliminarCompeticion(String id){
        try{
        	String sql ="delete from Competiciones where id='"+id+"' "; //$NON-NLS-1$ //$NON-NLS-2$
        	PreparedStatement pst = connection.prepareStatement(sql);
        	pst.execute();
			pst.close();
        }catch(Exception e){
        	JOptionPane.showMessageDialog(null, e);
        }
	}
	
	public void mostrarCompeticion(int index){
		String competicion = (String) listCompeticion.getModel().getElementAt(index);
		String[] texto=competicion.split(" "); //$NON-NLS-1$
		String id = texto[0];
        try{
        	String sql ="select * from Competiciones where id='"+id+"' "; //$NON-NLS-1$ //$NON-NLS-2$
	         PreparedStatement pst = connection.prepareStatement(sql);
	         ResultSet rs=pst.executeQuery();
	         if(rs.next()){
	        	 txtNombreCompeticion.setText(rs.getString("nombre")); //$NON-NLS-1$
	        	 txtLocalidadCompeticion.setText(rs.getString("localidad")); //$NON-NLS-1$
	        	 txtIdCompeticion.setText(rs.getString("id"));   //$NON-NLS-1$
	           	 String add4 = rs.getString("fecha"); //$NON-NLS-1$
	        	 String[] fecha=add4.split("/"); //$NON-NLS-1$
	        	 cbFechaDiaCompeticion.setSelectedItem(fecha[0]);
	        	 cbFechaMesCompeticion.setSelectedItem(fecha[1]);
	        	 cbFechaAnoCompeticion.setSelectedItem(fecha[2]);
	        	 txtLocalidadCompeticion.setText(rs.getString("localidad")); //$NON-NLS-1$
	        	 txtTelefonoCompeticion.setText(rs.getString("telefono")); //$NON-NLS-1$
	        	 txtCorreoElectronicoCompeticion.setText(rs.getString("email")); //$NON-NLS-1$
	        	 txtWebCompeticion.setText(rs.getString("web")); //$NON-NLS-1$
	           	 cbModalidadCompeticion.setSelectedItem(rs.getString("modalidad")); //$NON-NLS-1$
	        	 cbDificultadCompeticion.setSelectedItem(rs.getString("dificultad")); //$NON-NLS-1$
	        	 txtDistanciaCompeticion.setText(rs.getString("distancia")); //$NON-NLS-1$
	        	 txtDescripcionCompeticion.setText(rs.getString("descripcion")); //$NON-NLS-1$
	        	 txtLimiteInscritosCompeticion.setText(rs.getString("limitedeinscritos")); //$NON-NLS-1$
	        	 String add15 =rs.getString("fechalimiteinscripcion"); //$NON-NLS-1$
	        	 String[] fechaInscripcion=add15.split("/"); //$NON-NLS-1$
	        	 cbFechaLimiteInscripcionDiaCompeticion.setSelectedItem(fechaInscripcion[0]);
	        	 cbFechaLimiteInscripcionMesCompeticion.setSelectedItem(fechaInscripcion[1]);
	        	 cbFechaLimiteInscripcionAnoCompeticion.setSelectedItem(fechaInscripcion[2]);
	        	 txtPrecioInscripcionCompeticion.setText(rs.getString("precio")); //$NON-NLS-1$
	        	
	        	 String add18 =rs.getString("mapa");	   //$NON-NLS-1$
	        	 String add19 =rs.getString("logo"); //$NON-NLS-1$
	        	 String add20 =rs.getString("sponsor"); //$NON-NLS-1$
	        	 String add21 =rs.getString("sponsorsecundario"); //$NON-NLS-1$
	        	 taEnlacesInteresCompeticion.setText(rs.getString("enlacesinteres")); //$NON-NLS-1$
	        	 taInformacionGeneralCompeticion.setText(rs.getString("informacionadicional")); //$NON-NLS-1$
	        	 cbEstadoCompeticion.setSelectedItem(rs.getString("estado")); //$NON-NLS-1$
	        	
	          }
	         rs.close();
				pst.close();
        }catch(Exception e){
	        JOptionPane.showMessageDialog(null, e);
        }
        //Para Mostrar Los Tiempos
        idCompeticion=txtIdCompeticion.getText();
	}
	
	
	
	public void guardarCompeticion(){
		try{
			try{
				String sql ="Insert into Competiciones(nombre,fecha,localidad,telefono,email,web,id,modalidad,dificultad,distancia,descripcion,limitedeinscritos,fechalimiteinscripcion,precio,enlacesinteres,informacionadicional,estado) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";  	 //$NON-NLS-1$
				PreparedStatement pst = connection.prepareStatement(sql);
				pst.setString(1, txtNombreCompeticion.getText());
				String fechaDiaCompeticion= cbFechaDiaCompeticion.getSelectedItem().toString();
				String fechaMesCompeticion= cbFechaMesCompeticion.getSelectedItem().toString();
				String fechaAnoCompeticion= cbFechaAnoCompeticion.getSelectedItem().toString();
				String fecha = fechaDiaCompeticion+"/"+fechaMesCompeticion+"/"+fechaAnoCompeticion; //$NON-NLS-1$ //$NON-NLS-2$
				pst.setString(2, fecha);
				pst.setString(3, txtLocalidadCompeticion.getText());			
				pst.setString(4, txtTelefonoCompeticion.getText());
				pst.setString(5, txtCorreoElectronicoCompeticion.getText());
				pst.setString(6, txtWebCompeticion.getText());
				pst.setString(7, txtIdCompeticion.getText());
				pst.setString(8, cbModalidadCompeticion.getSelectedItem().toString());
				pst.setString(9, cbDificultadCompeticion.getSelectedItem().toString());
				pst.setString(10, txtDistanciaCompeticion.getText());
				pst.setString(11, txtDescripcionCompeticion.getText());
				pst.setString(12, txtLimiteInscritosCompeticion.getText());
				String fechaLimiteInscripcionDiaCompeticion= cbFechaLimiteInscripcionDiaCompeticion.getSelectedItem().toString();
				String fechaLimiteInscripcionMesCompeticion= cbFechaLimiteInscripcionMesCompeticion.getSelectedItem().toString();
				String fechaLimiteInscripcionAnoCompeticion= cbFechaLimiteInscripcionAnoCompeticion.getSelectedItem().toString();
				String fechaLimiteInscripcion = fechaLimiteInscripcionDiaCompeticion+"/"+fechaLimiteInscripcionMesCompeticion+"/"+fechaLimiteInscripcionAnoCompeticion; //$NON-NLS-1$ //$NON-NLS-2$
				pst.setString(13, fechaLimiteInscripcion);
				pst.setString(14, txtPrecioInscripcionCompeticion.getText());
				pst.setString(15, taEnlacesInteresCompeticion.getText());
				pst.setString(16, taInformacionGeneralCompeticion.getText());
				pst.setString(17, cbEstadoCompeticion.getSelectedItem().toString());
				
				pst.execute();
				pst.close();
				JOptionPane.showMessageDialog(null, MessagesIndex.getString("Index.844"));	 //$NON-NLS-1$
			}catch(Exception e){
				String nombre= txtNombreCompeticion.getText();
				String localidadcompeticion= txtLocalidadCompeticion.getText();
				String id= txtIdCompeticion.getText();
				String diaCompeticion= cbFechaDiaCompeticion.getSelectedItem().toString();
				String mesCompeticion= cbFechaMesCompeticion.getSelectedItem().toString();
				String anoCompeticion= cbFechaAnoCompeticion.getSelectedItem().toString();
				String fechaCompeticion = diaCompeticion+"/"+mesCompeticion+"/"+anoCompeticion; //$NON-NLS-1$ //$NON-NLS-2$
				String telefonoCompeticion = txtTelefonoCompeticion.getText();
				String correoElectronicoCompeticion = txtCorreoElectronicoCompeticion.getText();
				String webCompeticion = txtWebCompeticion.getText();
				String modalidadCompeticion = cbModalidadCompeticion.getSelectedItem().toString();
				String dificultadCompeticion = cbDificultadCompeticion.getSelectedItem().toString();
				String distanciaCompeticion = txtDistanciaCompeticion.getText();
				String descripcionCompeticion = txtDescripcionCompeticion.getText();
				String limiteInscritosCompeticion = txtLimiteInscritosCompeticion.getText();
				String diaInscritosCompeticion= cbFechaLimiteInscripcionDiaCompeticion.getSelectedItem().toString();
				String mesInscritosCompeticion= cbFechaLimiteInscripcionMesCompeticion.getSelectedItem().toString();
				String anoInscritosCompeticion= cbFechaLimiteInscripcionAnoCompeticion.getSelectedItem().toString();
				String fechaInscritosCompeticion = diaInscritosCompeticion+"/"+mesInscritosCompeticion+"/"+anoInscritosCompeticion; //$NON-NLS-1$ //$NON-NLS-2$
				String precioCompeticion = txtPrecioInscripcionCompeticion.getText();
				String enlacesCompeticion = taEnlacesInteresCompeticion.getText();
				String informacionCompeticion = taInformacionGeneralCompeticion.getText();
				String estadoCompeticion= cbEstadoCompeticion.getSelectedItem().toString();
							
				
				String sql="update Competiciones set nombre='"+nombre+"' ,localidad ='"+localidadcompeticion+"',id ='"+id+"',fecha='"+fechaCompeticion+"',telefono='"+telefonoCompeticion+"',email='"+correoElectronicoCompeticion+"',web='"+webCompeticion+"',modalidad='"+modalidadCompeticion+"',dificultad='"+dificultadCompeticion+"',distancia='"+distanciaCompeticion+"',descripcion='"+descripcionCompeticion+"',limitedeinscritos='"+limiteInscritosCompeticion+"',fechalimiteinscripcion='"+fechaInscritosCompeticion+"',precio='"+precioCompeticion+"',enlacesinteres='"+enlacesCompeticion+"',informacionadicional='"+informacionCompeticion+"',estado='"+estadoCompeticion+"' where id='"+id+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$
				PreparedStatement pst = connection.prepareStatement(sql);
				pst.execute();

				pst.close();
				JOptionPane.showMessageDialog(null, MessagesIndex.getString("Index.868")); //$NON-NLS-1$
			}	
		}catch(Exception e1){
			JOptionPane.showMessageDialog(null, e1);
		}
	}
	
	
	public void editarCompeticion (boolean editable){
		txtNombreCompeticion.setEditable(editable);
		txtLocalidadCompeticion.setEditable(editable);
		txtIdCompeticion.setEditable(editable);
		cbFechaDiaCompeticion.setEditable(false);
		cbFechaDiaCompeticion.setEnabled(editable);
		cbFechaMesCompeticion.setEditable(false);
		cbFechaMesCompeticion.setEnabled(editable);
		cbFechaAnoCompeticion.setEditable(false);
		cbFechaAnoCompeticion.setEnabled(editable);
		cbModalidadCompeticion.setEditable(false);
		cbModalidadCompeticion.setEnabled(editable);
		cbDificultadCompeticion.setEditable(false);
		cbDificultadCompeticion.setEnabled(editable);
		txtDescripcionCompeticion.setEditable(editable);
		cbFechaLimiteInscripcionDiaCompeticion.setEditable(false);
		cbFechaLimiteInscripcionDiaCompeticion.setEnabled(editable);
		cbFechaLimiteInscripcionMesCompeticion.setEditable(false);
		cbFechaLimiteInscripcionMesCompeticion.setEnabled(editable);
		cbFechaLimiteInscripcionAnoCompeticion.setEditable(false);
		cbFechaLimiteInscripcionAnoCompeticion.setEnabled(editable);
		txtPrecioInscripcionCompeticion.setEditable(editable);
		txtLimiteInscritosCompeticion.setEditable(editable);
		txtTelefonoCompeticion.setEditable(editable);
		txtWebCompeticion.setEditable(editable);
		txtCorreoElectronicoCompeticion.setEditable(editable);
		txtDistanciaCompeticion.setEditable(editable);	
		taEnlacesInteresCompeticion.setEditable(editable);
		taInformacionGeneralCompeticion.setEditable(editable);
		cbEstadoCompeticion.setEditable(false);
		cbEstadoCompeticion.setEnabled(editable);
		txtPrecioInscripcionCompeticion.setEditable(editable);
	}
	
	//Añadir los atributos que falten
	public void limpiarCompeticion(){
		txtNombreCompeticion.setText("");  //$NON-NLS-1$
		txtLocalidadCompeticion.setText("");  //$NON-NLS-1$
		txtIdCompeticion.setText("");  //$NON-NLS-1$
		txtDescripcionCompeticion.setText("");  //$NON-NLS-1$
		txtPrecioInscripcionCompeticion.setText("");  //$NON-NLS-1$
		txtLimiteInscritosCompeticion.setText("");  //$NON-NLS-1$
		txtTelefonoCompeticion.setText("");  //$NON-NLS-1$
		txtWebCompeticion.setText("");  //$NON-NLS-1$
		txtCorreoElectronicoCompeticion.setText("");  //$NON-NLS-1$
		txtDistanciaCompeticion.setText(""); 	 //$NON-NLS-1$
		taEnlacesInteresCompeticion.setText("");  //$NON-NLS-1$
		taInformacionGeneralCompeticion.setText("");  //$NON-NLS-1$
		txtPrecioInscripcionCompeticion.setText(""); //$NON-NLS-1$
		
	}
	
	public void eliminarParticipantePodioCompeticion(int index){
		String participante = (String) listCompeticionParticipante.getModel().getElementAt(index);
		String[] texto=participante.split(" "); //$NON-NLS-1$
		String dni = texto[0];
		try{
        	String sql ="delete from CompeticionesParticipantes where idCompeticion='"+idCompeticion+"' and dniParticipante='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        	PreparedStatement pst = connection.prepareStatement(sql);
        	pst.execute();

			pst.close();
        }catch(Exception e){
        	JOptionPane.showMessageDialog(null, e);
        }
		
	}
	
	
	///////////
	// ADMIN //
	///////////
	
	public void eliminarAdmin(){
		String dni = txtDNIAdmin.getText();
        try{
        	String sql ="delete from Admin where dni='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$
        	PreparedStatement pst = connection.prepareStatement(sql);
        	pst.execute();

			pst.close();
        }catch(Exception e){
        	JOptionPane.showMessageDialog(null, e);
        }
	}
	
	public void guardarAdmin(){
		try{
			try{
				String sql ="Insert into Admin (nombre,apellidos,dni,fechaNacimiento,sexo,tipoVia,direccion,nCalle,escalera,piso,puerta,codigoPostal,provincia,localidad,pais,telefono,correoElectronico,usuario,password)  values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";   //$NON-NLS-1$
				PreparedStatement pst = connection.prepareStatement(sql);
				pst.setString(1, txtNombreAdmin.getText());
				pst.setString(2, txtApellidosAdmin.getText());
				pst.setString(3, txtDNIAdmin.getText());
				String fechaNacimientoDia= cbFechaNacimientoDiaAdmin.getSelectedItem().toString();
				String fechaNacimientoMes= cbFechaNacimientoMesAdmin.getSelectedItem().toString();
				String fechaNacimientoAno= cbFechaNacimientoAnoAdmin.getSelectedItem().toString();
				String fechaNacimiento = fechaNacimientoDia+"/"+fechaNacimientoMes+"/"+fechaNacimientoAno; //$NON-NLS-1$ //$NON-NLS-2$
				pst.setString(4, fechaNacimiento);
				pst.setString(5, cbSexoAdmin.getSelectedItem().toString());
				pst.setString(6, cbDireccionAdmin.getSelectedItem().toString());
				pst.setString(7, txtDireccionAdmin.getText());
				pst.setString(8, txtNumeroAdmin.getText());
				pst.setString(9, cbEscaleraAdmin.getSelectedItem().toString());
				pst.setString(10, txtPisoAdmin.getText());
				pst.setString(11, txtPuertaAdmin.getText());
				pst.setString(12, txtCodigoPostalAdmin.getText());
				pst.setString(13, txtProvinciaAdmin.getText());
				pst.setString(14, txtLocalidadAdmin.getText());
				pst.setString(15, txtPaisAdmin.getText());
				pst.setString(16, txtTelefonoAdmin.getText());
				pst.setString(17, txtCorreoElectronicoAdmin.getText());
				pst.setString(17, txtPisoAdmin.getText());
				pst.setString(17, txtCorreoElectronicoAdmin.getText());
				pst.setString(18, txtUsuarioAdmin.getText());
				pst.setString(19, txtContraseñaAdmin.getText());
				pst.execute();

				pst.close();
				JOptionPane.showMessageDialog(null, MessagesIndex.getString("Index.891"));	 //$NON-NLS-1$
			}catch(Exception e){
				String nombre= txtNombreAdmin.getText();
				String apellidos= txtApellidosAdmin.getText();
				String dni= txtDNIAdmin.getText();
				String fechaNacimientoDia= cbFechaNacimientoDiaAdmin.getSelectedItem().toString();
				String fechaNacimientoMes= cbFechaNacimientoMesAdmin.getSelectedItem().toString();
				String fechaNacimientoAno= cbFechaNacimientoAnoAdmin.getSelectedItem().toString();
				String fecha = fechaNacimientoDia+"/"+fechaNacimientoMes+"/"+fechaNacimientoAno; //$NON-NLS-1$ //$NON-NLS-2$
				String sexo= cbSexoAdmin.getSelectedItem().toString();
				String tipoVia= cbDireccionAdmin.getSelectedItem().toString();
				String direccion= txtDireccionAdmin.getText();
				String nCalle= txtNumeroAdmin.getText();
				String escalera= cbEscaleraAdmin.getSelectedItem().toString();
				String piso= txtPisoAdmin.getText();
				String puerta= txtPuertaAdmin.getText();
				String codigoPostal= txtCodigoPostalAdmin.getText();
				String localidad= txtLocalidadAdmin.getText();
				String pais= txtPaisAdmin.getText();
				String provincia= txtProvinciaAdmin.getText();
				String telefono= txtTelefonoAdmin.getText();
				String correoElectronico= txtCorreoElectronicoAdmin.getText();
				String usuario= txtUsuarioAdmin.getText();
				String contraseña= txtContraseñaAdmin.getText();

				String sql="update Admin set nombre='"+nombre+"' ,apellidos ='"+apellidos+"',dni ='"+dni+"',fechaNacimiento='"+fecha+"',sexo='"+sexo+"',tipoVia='"+tipoVia+"',direccion='"+direccion+"',nCalle='"+nCalle+"',escalera='"+escalera+"',piso='"+piso+"',puerta='"+puerta+"',codigoPostal='"+codigoPostal+"',localidad='"+localidad+"',pais='"+pais+"',provincia='"+provincia+"',telefono='"+telefono+"',correoElectronico='"+correoElectronico+"',usuario='"+usuario+"',password='"+contraseña+"' where dni='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$
				PreparedStatement pst = connection.prepareStatement(sql);
				pst.execute();

				pst.close();
				JOptionPane.showMessageDialog(null, "Administrador Guardado"); //$NON-NLS-1$
			}	
		}catch(Exception e1){
			JOptionPane.showMessageDialog(null, e1);
		}

	}
	
	//Añadir los atributos que falten
	public void editarAdmin (boolean editable){
		txtNombreAdmin.setEditable(editable);
		txtApellidosAdmin.setEditable(editable);
		txtDNIAdmin.setEditable(editable);
		cbFechaNacimientoDiaAdmin.setEditable(false);
		cbFechaNacimientoDiaAdmin.setEnabled(editable);
		cbFechaNacimientoMesAdmin.setEditable(false);
		cbFechaNacimientoMesAdmin.setEnabled(editable);
		cbFechaNacimientoAnoAdmin.setEditable(false);
		cbFechaNacimientoAnoAdmin.setEnabled(editable);
		cbSexoAdmin.setEditable(false);
		cbSexoAdmin.setEnabled(editable);
		cbDireccionAdmin.setEditable(false);
		cbDireccionAdmin.setEnabled(editable);
		txtDireccionAdmin.setEditable(editable);
		txtNumeroAdmin.setEditable(editable);
		cbEscaleraAdmin.setEditable(false);
		cbEscaleraAdmin.setEnabled(editable);
		txtPisoAdmin.setEditable(editable);
		txtPuertaAdmin.setEditable(editable);
		txtCodigoPostalAdmin.setEditable(editable);
		txtLocalidadAdmin.setEditable(editable);
		txtPaisAdmin.setEditable(editable);
		txtProvinciaAdmin.setEditable(editable);
		txtTelefonoAdmin.setEditable(editable);
		txtCorreoElectronicoAdmin.setEditable(editable);
		txtUsuarioAdmin.setEditable(editable);
		txtContraseñaAdmin.setEditable(editable);
		txtRepetirContraseñaAdmin.setEditable(editable);
	}
	
	//Añadir los atributos que falten
	public void limpiarAdmin(){
		txtNombreAdmin.setText("");  //$NON-NLS-1$
		txtApellidosAdmin.setText("");  //$NON-NLS-1$
		txtDNIAdmin.setText("");  //$NON-NLS-1$
		txtDireccionAdmin.setText("");  //$NON-NLS-1$
		txtNumeroAdmin.setText("");  //$NON-NLS-1$
		txtPisoAdmin.setText("");  //$NON-NLS-1$
		txtPuertaAdmin.setText("");  //$NON-NLS-1$
		txtPuertaAdmin.setText("");  //$NON-NLS-1$
		txtCodigoPostalAdmin.setText("");  //$NON-NLS-1$
		txtLocalidadAdmin.setText(""); 	 //$NON-NLS-1$
		txtPaisAdmin.setText("");  //$NON-NLS-1$
		txtProvinciaAdmin.setText("");  //$NON-NLS-1$
		txtTelefonoAdmin.setText(""); //$NON-NLS-1$
		txtCorreoElectronicoAdmin.setText(""); //$NON-NLS-1$
		txtUsuarioAdmin.setText(""); //$NON-NLS-1$
		txtContraseñaAdmin.setText(""); //$NON-NLS-1$
		txtRepetirContraseñaAdmin.setText(""); //$NON-NLS-1$

	}
	
	
	//resumen Competicion en Panel Participante
	private class mostrarCompeticionParticipanteListSelectionListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			
				try{
					String competicionParticipante = (String) listCompeticionParticipante.getSelectedValue();
					String[] texto=competicionParticipante.split(" "); //$NON-NLS-1$
					String id = texto[0];
		        	String sql ="select * from Competiciones where id='"+id+"' "; //$NON-NLS-1$ //$NON-NLS-2$
			        PreparedStatement pst = connection.prepareStatement(sql);
			        ResultSet rs=pst.executeQuery();
			        if(rs.next()){
			        	taResumenCompeticionParticipante.setText(rs.getString("nombre")+"\n"+rs.getString("localidad")+"\n"+rs.getString("id")+"\n"+rs.getString("fecha")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
			        	
			        }
			        rs.close();
					pst.close();
		        }catch(Exception e2){
			        //JOptionPane.showMessageDialog(null, e2);
		        }
			 
		}
	}
	
	//Doble Click lista Competiciones en Panel Participantes
	private class ListCompeticionParticipanteMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
	        	int index = listCompeticionParticipante.getSelectedIndex();
	        	
	    		try{
	    			String competicion = (String) listCompeticionParticipante.getModel().getElementAt(index);
		    		String[] texto=competicion.split(" "); //$NON-NLS-1$
		    		String id = texto[0];
		    		
	            	String sql ="select * from Competiciones where id='"+id+"' "; //$NON-NLS-1$ //$NON-NLS-2$
	    	         PreparedStatement pst = connection.prepareStatement(sql);
	    	         ResultSet rs=pst.executeQuery();
	    	         if(rs.next()){
	    	        	 txtNombreCompeticion.setText(rs.getString("nombre")); //$NON-NLS-1$
	    	        	 txtLocalidadCompeticion.setText(rs.getString("localidad")); //$NON-NLS-1$
	    	        	 txtIdCompeticion.setText(rs.getString("id"));   //$NON-NLS-1$
	    	           	 String add4 = rs.getString("fecha"); //$NON-NLS-1$
	    	        	 String[] fecha=add4.split("/"); //$NON-NLS-1$
	    	        	 cbFechaDiaCompeticion.setSelectedItem(fecha[0]);
	    	        	 cbFechaMesCompeticion.setSelectedItem(fecha[1]);
	    	        	 cbFechaAnoCompeticion.setSelectedItem(fecha[2]);
	    	        	 txtLocalidadCompeticion.setText(rs.getString("localidad")); //$NON-NLS-1$
	    	        	 txtTelefonoCompeticion.setText(rs.getString("telefono")); //$NON-NLS-1$
	    	        	 txtCorreoElectronicoCompeticion.setText(rs.getString("email")); //$NON-NLS-1$
	    	        	 txtWebCompeticion.setText(rs.getString("web")); //$NON-NLS-1$
	    	           	 cbModalidadCompeticion.setSelectedItem(rs.getString("modalidad")); //$NON-NLS-1$
	    	        	 cbDificultadCompeticion.setSelectedItem(rs.getString("dificultad")); //$NON-NLS-1$
	    	        	 txtDistanciaCompeticion.setText(rs.getString("distancia")); //$NON-NLS-1$
	    	        	 txtDescripcionCompeticion.setText(rs.getString("descripcion")); //$NON-NLS-1$
	    	        	 txtLimiteInscritosCompeticion.setText(rs.getString("limitedeinscritos")); //$NON-NLS-1$
	    	        	 String add15 =rs.getString("fechalimiteinscripcion"); //$NON-NLS-1$
	    	        	 String[] fechaInscripcion=add15.split("/"); //$NON-NLS-1$
	    	        	 cbFechaLimiteInscripcionDiaCompeticion.setSelectedItem(fechaInscripcion[0]);
	    	        	 cbFechaLimiteInscripcionMesCompeticion.setSelectedItem(fechaInscripcion[1]);
	    	        	 cbFechaLimiteInscripcionAnoCompeticion.setSelectedItem(fechaInscripcion[2]);
	    	        	 txtPrecioInscripcionCompeticion.setText(rs.getString("precio")); //$NON-NLS-1$
	    	        	 
	    	        	 String add18 =rs.getString("mapa");	   //$NON-NLS-1$
	    	        	 String add19 =rs.getString("logo"); //$NON-NLS-1$
	    	        	 String add20 =rs.getString("sponsor"); //$NON-NLS-1$
	    	        	 String add21 =rs.getString("sponsorsecundario"); //$NON-NLS-1$
	    	        	 taEnlacesInteresCompeticion.setText(rs.getString("enlacesinteres")); //$NON-NLS-1$
	    	        	 taInformacionGeneralCompeticion.setText(rs.getString("informacionadicional")); //$NON-NLS-1$
	    	        	 cbEstadoCompeticion.setSelectedItem(rs.getString("estado")); //$NON-NLS-1$
	    	        	 
	    	          }
	    	         rs.close();
 					pst.close();
	            }catch(Exception e2){
	    	        //JOptionPane.showMessageDialog(null, e2);
	            }
	    		btnGuardarCompeticionCompeticion.setEnabled(false);
				btnModificarCompeticionCompeticion.setEnabled(true);
				btnEliminarCompeticionCompeticion.setEnabled(true);
				btnEditarImagen.setEnabled(false);
				CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
				cl.show(getPanelContenedor(), "Competicion"); //$NON-NLS-1$
				editarCompeticion(false);
			}
		}
	}
	

	//Doble Click lista ParticipantesPodioCompeticiones en Panel Podio Competiciones
	private class ListParticipantesPodioCompeticionMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			
			
			if (e.getClickCount() == 2) {
				limpiarParticipante();
				editarParticipante(true);
				
	        	
	    		try{
	    			Persistencia.AgenteDB.desconectar();
	    			Persistencia.AgenteDB.conectar();
	    			int index = listParticipantesPodioCompeticion.getSelectedIndex();
		        	String participantePodioCompeticion = (String) listParticipantesPodioCompeticion.getModel().getElementAt(index);
		        	
		    		String[] texto=participantePodioCompeticion.split(" "); //$NON-NLS-1$
		    		String dni = texto[1];
		    		
	    			String sql ="select * from Participantes where dni='"+dni+"'"; //$NON-NLS-1$ //$NON-NLS-2$
	    			PreparedStatement pst = connection.prepareStatement(sql);
	    			ResultSet rs=pst.executeQuery();
	    			if(rs.next()){
	    				String nombre = rs.getString("nombre");  //$NON-NLS-1$
	    				txtNombreParticipante.setText(rs.getString("nombre")); //$NON-NLS-1$
	    				txtApellidosParticipante.setText(rs.getString("apellidos")); //$NON-NLS-1$
	    				txtDNIParticipante.setText(rs.getString("dni"));   //$NON-NLS-1$
	    				String fechaNacimiento = rs.getString("fechaNacimiento"); //$NON-NLS-1$
	    				String[] fecha=fechaNacimiento.split("/"); //$NON-NLS-1$
	    				cbFechaNacimientoDiaParticipante.setSelectedItem(fecha[0]);
	    				cbFechaNacimientoMesParticipante.setSelectedItem(fecha[1]);
	    				cbFechaNacimientoAnoParticipante.setSelectedItem(fecha[2]);
	    				cbSexoParticipante.setSelectedItem(rs.getString("sexo")); //$NON-NLS-1$
	    				cbDireccionParticipante.setSelectedItem(rs.getString("tipoVia")); //$NON-NLS-1$
	    				txtDireccionParticipante.setText(rs.getString("direccion")); //$NON-NLS-1$
	    				txtNumeroParticipante.setText(rs.getString("nCalle")); //$NON-NLS-1$
	    				cbEscaleraParticipante.setSelectedItem(rs.getString("escalera")); //$NON-NLS-1$
	    				txtPisoParticipante.setText(rs.getString("piso")); //$NON-NLS-1$
	    				txtPuertaParticipante.setText(rs.getString("puerta")); //$NON-NLS-1$
	    				txtCodigoPostalParticipante.setText(rs.getString("codigoPostal")); //$NON-NLS-1$
	    				txtProvinciaParticipante.setText(rs.getString("provincia")); //$NON-NLS-1$
	    				txtLocalidadParticipante.setText(rs.getString("localidad")); //$NON-NLS-1$
	    				txtPaisParticipante.setText(rs.getString("pais")); //$NON-NLS-1$
	    				txtTelefonoParticipante.setValue(rs.getString("telefono")); //$NON-NLS-1$
	    				txtCorreoElectronicoParticipante.setText(rs.getString("correoElectronico")); //$NON-NLS-1$
	    				cbTallaParticipante.setSelectedItem(rs.getString("talla")); //$NON-NLS-1$
	    				cbTipoParticipante.setSelectedItem(rs.getString("tipo"));  //$NON-NLS-1$
	    				
	    			}
	    			Persistencia.AgenteDB.desconectar();
	    			
	    			//rs.close();
    				//pst.close();
	    		}catch(Exception e2){
	    			//JOptionPane.showMessageDialog(null, e2);
	    		}
	    		
	    		btnGuardarParticipanteParticipante.setEnabled(false);
	    		btnModificarParticipanteParticipante.setEnabled(true);
	    		btnEliminarParticipanteParticipante.setEnabled(true);
	    		
	    		CardLayout cl = (CardLayout)(getPanelContenedor().getLayout());
	    		cl.show(getPanelContenedor(), "Participante"); //$NON-NLS-1$
	    		
	    		try{
	    			
					String competicionParticipante = (String) listCompeticionParticipante.getSelectedValue();
					String[] texto=competicionParticipante.split(" "); //$NON-NLS-1$
					String id = texto[0];
		        	String sql ="select * from Competiciones where id='"+id+"' "; //$NON-NLS-1$ //$NON-NLS-2$
			        PreparedStatement pst = connection.prepareStatement(sql);
			        ResultSet rs=pst.executeQuery();
			        if(rs.next()){
			        	taResumenCompeticionParticipante.setText(rs.getString("nombre")+"\n"+rs.getString("localidad")+"\n"+rs.getString("id")+"\n"+rs.getString("fecha")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
			        	
			        }
			        rs.close();
					pst.close();
		        }catch(Exception e2){
			        //JOptionPane.showMessageDialog(null, e2);
		        }
	    		editarParticipante(false);
			}
		}
	}
	
	//mostrar tiempos //No hace el split
	private class ListParticipantesPodioCompeticionListSelectionListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			String participantePodioCompeticion = (String) listParticipantesPodioCompeticion.getSelectedValue();
			String[] texto=participantePodioCompeticion.split(" "); //$NON-NLS-1$
			String dni = texto[1];
				try{
					String sql ="select * from CompeticionesParticipantes where dniParticipante='"+dni+"' and idCompeticion='"+idCompeticion+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			        PreparedStatement pst = connection.prepareStatement(sql);
			        ResultSet rs=pst.executeQuery();
			        if(rs.next()){
			        	txtTiempoTotalPodioCompeticion.setText(rs.getString("tiempoTotal")); //$NON-NLS-1$
			        	txtSeccion1PodioCompeticion.setText(rs.getString("seccion1")); //$NON-NLS-1$
			        	txtSeccion2PodioCompeticion.setText(rs.getString("seccion2")); //$NON-NLS-1$
			        	txtSeccion3PodioCompeticion.setText(rs.getString("seccion3")); //$NON-NLS-1$
			        	txtSeccion4PodioCompeticion.setText(rs.getString("seccion4")); //$NON-NLS-1$
			        	txtSeccion5PodioCompeticion.setText(rs.getString("seccion5"));	 //$NON-NLS-1$
			        }
			        rs.close();
					pst.close();
		        }catch(Exception e2){
			        JOptionPane.showMessageDialog(null, e2);
		        }
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	//Actualizar Ultimo Acceso Admin
	public void actualizarUltimoAcceso(){
		java.util.Date fechaUltimoAcceso = new java.util.Date();
		String dni =null;
		try{
			String query = "select * from Admin where usuario ='"+Login.admin.get(0)+"' and password='"+Login.admin.get(1)+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			PreparedStatement pst = connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			if(rs.next()){
				dni =rs.getString("dni"); 	 //$NON-NLS-1$
			}
			rs.close();
			pst.close();
		}catch(Exception e1){
			JOptionPane.showMessageDialog(null, e1);
		}
		try{
			String sql="update Admin set fechaUltimoAcceso='"+fechaUltimoAcceso+"' where dni='"+dni+"' "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			PreparedStatement pst = connection.prepareStatement(sql);
			pst.execute();
			pst.close();
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, e);
		}

	}
	
	//Cambiar fuente global
		public static void setUIFont (javax.swing.plaf.FontUIResource f){
			java.util.Enumeration keys = UIManager.getDefaults().keys();
			while (keys.hasMoreElements()) {
				Object key = keys.nextElement();
				Object value = UIManager.get (key);
				if (value != null && value instanceof javax.swing.plaf.FontUIResource)
					UIManager.put (key, f);
			}
		} 
}