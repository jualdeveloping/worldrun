package Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridBagLayout;

import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.border.TitledBorder;

public class Galeria {

	private JFrame frame;
	private JPanel panelFoto;
	private JPanel panelMenu;
	private JButton btnAtrasFotos;
	private JButton btnProximaFoto;
	private JLabel lblFotoGrande;
	private JPanel panelMiniatura1;
	private JPanel panelMiniatura2;
	private JPanel panelMiniatura3;
	private JLabel lblMiniatura1;
	private JLabel lblMiniatura2;
	private JLabel lblMiniatura3;
	
	
    private int miniatura1 = 0;
    private int miniatura2 = 1;
    private int miniatura3 = 2;
    
    private ArrayList<ImageIcon> fotos = new ArrayList<ImageIcon>();
    private ImageIcon nofoto;
    
    
    
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Galeria window = new Galeria();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Galeria() {
		nofoto = new ImageIcon(Galeria.class.getResource("/Galeria/nofoto.gif"));
	    fotos.add(new ImageIcon(Galeria.class.getResource("/Galeria/Galeria01.jpg")));
	    fotos.add(new ImageIcon(Galeria.class.getResource("/Galeria/Galeria02.jpg")));
	    fotos.add(new ImageIcon(Galeria.class.getResource("/Galeria/Galeria03.jpg")));
	    fotos.add(new ImageIcon(Galeria.class.getResource("/Galeria/Galeria04.jpg")));
	    fotos.add(new ImageIcon(Galeria.class.getResource("/Galeria/Galeria05.jpg")));
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.controlHighlight);
		frame.setBounds(100, 100, 839, 492);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setResizable(false);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		{
			panelFoto = new JPanel();
			GridBagConstraints gbc_panelFoto = new GridBagConstraints();
			gbc_panelFoto.insets = new Insets(0, 0, 5, 5);
			gbc_panelFoto.fill = GridBagConstraints.BOTH;
			gbc_panelFoto.gridx = 1;
			gbc_panelFoto.gridy = 1;
			frame.getContentPane().add(panelFoto, gbc_panelFoto);
			GridBagLayout gbl_panelFoto = new GridBagLayout();
			gbl_panelFoto.columnWidths = new int[]{0, 33, 0, 0};
			gbl_panelFoto.rowHeights = new int[]{0, 35, 0, 0};
			gbl_panelFoto.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			gbl_panelFoto.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			panelFoto.setLayout(gbl_panelFoto);
			{
				lblFotoGrande = new JLabel("");
				GridBagConstraints gbc_lblFotoGrande = new GridBagConstraints();
				gbc_lblFotoGrande.insets = new Insets(0, 0, 5, 5);
				gbc_lblFotoGrande.gridx = 1;
				gbc_lblFotoGrande.gridy = 1;
				panelFoto.add(lblFotoGrande, gbc_lblFotoGrande);
			}
		}
		{
			panelMenu = new JPanel();
			GridBagConstraints gbc_panelMenu = new GridBagConstraints();
			gbc_panelMenu.insets = new Insets(0, 0, 5, 5);
			gbc_panelMenu.fill = GridBagConstraints.BOTH;
			gbc_panelMenu.gridx = 1;
			gbc_panelMenu.gridy = 2;
			frame.getContentPane().add(panelMenu, gbc_panelMenu);
			GridBagLayout gbl_panelMenu = new GridBagLayout();
			gbl_panelMenu.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
			gbl_panelMenu.rowHeights = new int[]{0, 44, 0, 0};
			gbl_panelMenu.columnWeights = new double[]{0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panelMenu.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			panelMenu.setLayout(gbl_panelMenu);
			{
				btnAtrasFotos = new JButton("");
				btnAtrasFotos.setIcon(new ImageIcon(Galeria.class.getResource("/Recursos/left_circular-26.png")));
				btnAtrasFotos.addActionListener(new BtnAtrasFotosActionListener());
				GridBagConstraints gbc_btnAtrasFotos = new GridBagConstraints();
				gbc_btnAtrasFotos.insets = new Insets(0, 0, 5, 5);
				gbc_btnAtrasFotos.gridx = 1;
				gbc_btnAtrasFotos.gridy = 1;
				panelMenu.add(btnAtrasFotos, gbc_btnAtrasFotos);
			}
			{
				panelMiniatura1 = new JPanel();
				panelMiniatura1.setBorder(new LineBorder(new Color(0, 0, 0)));
				panelMiniatura1.addMouseListener(new PanelMiniatura1MouseListener());
				GridBagConstraints gbc_panelMiniatura1 = new GridBagConstraints();
				gbc_panelMiniatura1.insets = new Insets(0, 0, 5, 5);
				gbc_panelMiniatura1.fill = GridBagConstraints.BOTH;
				gbc_panelMiniatura1.gridx = 2;
				gbc_panelMiniatura1.gridy = 1;
				panelMenu.add(panelMiniatura1, gbc_panelMiniatura1);
				GridBagLayout gbl_panelMiniatura1 = new GridBagLayout();
				gbl_panelMiniatura1.columnWidths = new int[]{0, 29, 0, 0};
				gbl_panelMiniatura1.rowHeights = new int[]{0, 28, 0, 0};
				gbl_panelMiniatura1.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
				gbl_panelMiniatura1.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
				panelMiniatura1.setLayout(gbl_panelMiniatura1);
				{
					lblMiniatura1 = new JLabel("");
					lblMiniatura1.setPreferredSize(new java.awt.Dimension(100, 100));
					lblMiniatura1.setIcon(new ImageIcon(Galeria.class.getResource("/Galeria/Galeria01.jpg")));
					GridBagConstraints gbc_lblMiniatura1 = new GridBagConstraints();
					gbc_lblMiniatura1.insets = new Insets(0, 0, 5, 5);
					gbc_lblMiniatura1.gridx = 1;
					gbc_lblMiniatura1.gridy = 1;
					panelMiniatura1.add(lblMiniatura1, gbc_lblMiniatura1);
				}
			}
			{
				panelMiniatura2 = new JPanel();
				panelMiniatura2.setBorder(new LineBorder(new Color(0, 0, 0)));
				panelMiniatura2.addMouseListener(new PanelMiniatura2MouseListener());
				GridBagConstraints gbc_panelMiniatura2 = new GridBagConstraints();
				gbc_panelMiniatura2.insets = new Insets(0, 0, 5, 5);
				gbc_panelMiniatura2.fill = GridBagConstraints.BOTH;
				gbc_panelMiniatura2.gridx = 3;
				gbc_panelMiniatura2.gridy = 1;
				panelMenu.add(panelMiniatura2, gbc_panelMiniatura2);
				GridBagLayout gbl_panelMiniatura2 = new GridBagLayout();
				gbl_panelMiniatura2.columnWidths = new int[]{0, 30, 0, 0};
				gbl_panelMiniatura2.rowHeights = new int[]{0, 35, 0, 0};
				gbl_panelMiniatura2.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
				gbl_panelMiniatura2.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
				panelMiniatura2.setLayout(gbl_panelMiniatura2);
				{
					lblMiniatura2 = new JLabel("");
					lblMiniatura2.setPreferredSize(new java.awt.Dimension(100, 100));
					GridBagConstraints gbc_lblMiniatura2 = new GridBagConstraints();
					gbc_lblMiniatura2.insets = new Insets(0, 0, 5, 5);
					gbc_lblMiniatura2.gridx = 1;
					gbc_lblMiniatura2.gridy = 1;
					panelMiniatura2.add(lblMiniatura2, gbc_lblMiniatura2);
				}
			}
			{
				panelMiniatura3 = new JPanel();
				panelMiniatura3.setBorder(new LineBorder(new Color(0, 0, 0)));
				panelMiniatura3.addMouseListener(new PanelMiniatura3MouseListener());
				GridBagConstraints gbc_panelMiniatura3 = new GridBagConstraints();
				gbc_panelMiniatura3.insets = new Insets(0, 0, 5, 5);
				gbc_panelMiniatura3.fill = GridBagConstraints.BOTH;
				gbc_panelMiniatura3.gridx = 4;
				gbc_panelMiniatura3.gridy = 1;
				panelMenu.add(panelMiniatura3, gbc_panelMiniatura3);
				GridBagLayout gbl_panelMiniatura3 = new GridBagLayout();
				gbl_panelMiniatura3.columnWidths = new int[]{0, 38, 0, 0};
				gbl_panelMiniatura3.rowHeights = new int[]{0, 47, 0, 0};
				gbl_panelMiniatura3.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
				gbl_panelMiniatura3.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
				panelMiniatura3.setLayout(gbl_panelMiniatura3);
				{
					lblMiniatura3 = new JLabel("");
					lblMiniatura3.setPreferredSize(new java.awt.Dimension(100, 100));
					GridBagConstraints gbc_lblMiniatura3 = new GridBagConstraints();
					gbc_lblMiniatura3.insets = new Insets(0, 0, 5, 5);
					gbc_lblMiniatura3.gridx = 1;
					gbc_lblMiniatura3.gridy = 1;
					panelMiniatura3.add(lblMiniatura3, gbc_lblMiniatura3);
				}
			}
			{
				btnProximaFoto = new JButton("");
				btnProximaFoto.setIcon(new ImageIcon(Galeria.class.getResource("/Recursos/right_circular-26.png")));
				btnProximaFoto.addActionListener(new BtnProximaFotoActionListener());
				GridBagConstraints gbc_btnProximaFoto = new GridBagConstraints();
				gbc_btnProximaFoto.insets = new Insets(0, 0, 5, 5);
				gbc_btnProximaFoto.gridx = 5;
				gbc_btnProximaFoto.gridy = 1;
				panelMenu.add(btnProximaFoto, gbc_btnProximaFoto);
			}
		}
	}

	private class BtnAtrasFotosActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			miniatura1-=1;
			miniatura2-=1;
			miniatura3-=1;
	        lblMiniatura1.setIcon(getPreview(miniatura1));
	        lblMiniatura2.setIcon(getPreview(miniatura2));
	        lblMiniatura3.setIcon(getPreview(miniatura3));
		}
	}
	
	private class BtnProximaFotoActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			miniatura1+=1;
			miniatura2+=1;
			miniatura3+=1;
			lblMiniatura1.setIcon(getPreview(miniatura1));
			lblMiniatura2.setIcon(getPreview(miniatura2));
			lblMiniatura3.setIcon(getPreview(miniatura3));
		}
	}
	
	private class PanelMiniatura1MouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			 lblFotoGrande.setIcon(getFoto(miniatura1,lblFotoGrande.getSize()));
		}
	}
	private class PanelMiniatura2MouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			lblFotoGrande.setIcon(getFoto(miniatura2,lblFotoGrande.getSize()));
		}
	}
	private class PanelMiniatura3MouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			lblFotoGrande.setIcon(getFoto(miniatura3,lblFotoGrande.getSize()));
		}
	}
	
	/* devuelve una imagen de tamaño 100x100 VISTA PREVIA */
    public Icon getPreview(int num){
        if( num>=0 & num<fotos.size() )
        {
            Image mini = fotos.get(num).getImage().getScaledInstance(100, 100, Image.SCALE_AREA_AVERAGING);
            return new ImageIcon(mini);
        }
        else
        {
            Image mini = nofoto.getImage().getScaledInstance(100, 100, Image.SCALE_AREA_AVERAGING);
            return new ImageIcon(mini);
        }
    }

   /* devuelve la foto original, pero si el tamaño es mayor al contenedor, lo redimensiona */
   public Icon getFoto(int num, Dimension d){
        if( num>=0 & num<fotos.size() )
        {
            //si la foto es mas grande que el contendor -> redimensionar
            if(fotos.get(num).getIconWidth()>d.getWidth()){
                float v = getValor(fotos.get(num).getIconWidth(),d.width);
                return Disminuir(fotos.get(num),v);
            }else if(fotos.get(num).getIconHeight()>d.getHeight()){
                float v = getValor(fotos.get(num).getIconHeight(),d.height);               
                return Disminuir(fotos.get(num),v);
            }else{
                return fotos.get(num);
            }
        }
        else
        {
            Image mini = nofoto.getImage().getScaledInstance(400, 400, Image.SCALE_AREA_AVERAGING);
            return new ImageIcon(mini);
        }
    }

   /* redimensiona la imagen en para que ingrese al contendor pero manteniendo sus proporciones*/
    private ImageIcon Disminuir(ImageIcon i, float v){
        int valEscalaX =  (int) (i.getIconWidth() * v );
        int valEscalaY =  (int) (i.getIconHeight() * v );
        Image mini = i.getImage().getScaledInstance(valEscalaX, valEscalaY, Image.SCALE_AREA_AVERAGING);
        return new ImageIcon(mini);
     }

    /* devuelve el valor de escala para redimensionar la imagen*/
    private float getValor(int a, int b){                
        return Math.abs((a/new Float(b))-2f);
    }
    
    public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
}
