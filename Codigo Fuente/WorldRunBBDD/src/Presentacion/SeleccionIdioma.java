package Presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;

import javax.swing.JButton;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

import java.awt.Dimension;

import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.ButtonGroup;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class SeleccionIdioma {

	private JFrame frame;
	private JPanel panelGeneral;
	private JPanel panelSeleccion;
	private JLabel lblTextoSeleccionIdioma;
	private JRadioButton rdbtnEspanol;
	private JRadioButton rdbtnEnglish;
	private JPanel panelBoton;
	private JButton btnOkSeleccionIdioma;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					SeleccionIdioma window = new SeleccionIdioma();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SeleccionIdioma() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(SeleccionIdioma.class.getResource("/Recursos/FavIcon.png")));
		frame.setBounds(100, 100, 503, 379);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		{
			panelGeneral = new JPanel();
			frame.getContentPane().add(panelGeneral, BorderLayout.CENTER);
			panelGeneral.setLayout(null);
			{
				panelSeleccion = new JPanel();
				panelSeleccion.setBounds(117, 31, 247, 186);
				panelSeleccion.setBorder(new LineBorder(new Color(0, 0, 0)));
				panelGeneral.add(panelSeleccion);
				GridBagLayout gbl_panelSeleccion = new GridBagLayout();
				gbl_panelSeleccion.columnWidths = new int[]{0, 128, 0, 0};
				gbl_panelSeleccion.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
				gbl_panelSeleccion.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
				gbl_panelSeleccion.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				panelSeleccion.setLayout(gbl_panelSeleccion);
				{
					lblTextoSeleccionIdioma = new JLabel(MessagesSeleccionIdioma.getString("SeleccionIdioma.lblTextoSeleccionIdioma.text")); //$NON-NLS-1$
					GridBagConstraints gbc_lblTextoSeleccionIdioma = new GridBagConstraints();
					gbc_lblTextoSeleccionIdioma.insets = new Insets(0, 0, 5, 5);
					gbc_lblTextoSeleccionIdioma.gridx = 1;
					gbc_lblTextoSeleccionIdioma.gridy = 1;
					panelSeleccion.add(lblTextoSeleccionIdioma, gbc_lblTextoSeleccionIdioma);
				}
				{
					rdbtnEspanol = new JRadioButton(MessagesSeleccionIdioma.getString("SeleccionIdioma.rdbtnEspanol.text")); //$NON-NLS-1$
					buttonGroup.add(rdbtnEspanol);
					rdbtnEspanol.setIcon(new ImageIcon(SeleccionIdioma.class.getResource("/Recursos/Spain-icon.png")));
					GridBagConstraints gbc_rdbtnEspanol = new GridBagConstraints();
					gbc_rdbtnEspanol.insets = new Insets(0, 0, 5, 5);
					gbc_rdbtnEspanol.gridx = 1;
					gbc_rdbtnEspanol.gridy = 2;
					panelSeleccion.add(rdbtnEspanol, gbc_rdbtnEspanol);
				}
				{
					rdbtnEnglish = new JRadioButton(MessagesSeleccionIdioma.getString("SeleccionIdioma.rdbtnEnglish.text")); //$NON-NLS-1$
					buttonGroup.add(rdbtnEnglish);
					rdbtnEnglish.setIcon(new ImageIcon(SeleccionIdioma.class.getResource("/Recursos/United-States-of-Americ-icon.png")));
					GridBagConstraints gbc_rdbtnEnglish = new GridBagConstraints();
					gbc_rdbtnEnglish.insets = new Insets(0, 0, 5, 5);
					gbc_rdbtnEnglish.gridx = 1;
					gbc_rdbtnEnglish.gridy = 3;
					panelSeleccion.add(rdbtnEnglish, gbc_rdbtnEnglish);
				}
			}
			{
				panelBoton = new JPanel();
				panelBoton.setBounds(117, 252, 247, 41);
				panelBoton.setBorder(new LineBorder(new Color(0, 0, 0)));
				panelGeneral.add(panelBoton);
				GridBagLayout gbl_panelBoton = new GridBagLayout();
				gbl_panelBoton.columnWidths = new int[]{0, 0, 0, 0};
				gbl_panelBoton.rowHeights = new int[]{0, 0, 0, 0};
				gbl_panelBoton.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
				gbl_panelBoton.rowWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
				panelBoton.setLayout(gbl_panelBoton);
				{
					btnOkSeleccionIdioma = new JButton(MessagesSeleccionIdioma.getString("SeleccionIdioma.btnOkSeleccionIdioma.text")); //$NON-NLS-1$
					btnOkSeleccionIdioma.addActionListener(new BtnOkSeleccionIdiomaActionListener());
					GridBagConstraints gbc_btnOkSeleccionIdioma = new GridBagConstraints();
					gbc_btnOkSeleccionIdioma.fill = GridBagConstraints.BOTH;
					gbc_btnOkSeleccionIdioma.insets = new Insets(0, 0, 5, 5);
					gbc_btnOkSeleccionIdioma.gridx = 1;
					gbc_btnOkSeleccionIdioma.gridy = 1;
					panelBoton.add(btnOkSeleccionIdioma, gbc_btnOkSeleccionIdioma);
				}
			}
			
			JLabel lblNewLabel = new JLabel(MessagesSeleccionIdioma.getString("SeleccionIdioma.lblNewLabel.text")); //$NON-NLS-1$
			lblNewLabel.setIcon(new ImageIcon(SeleccionIdioma.class.getResource("/Recursos/mapamundi.png")));
			lblNewLabel.setBounds(0, 0, 487, 341);
			panelGeneral.add(lblNewLabel);
		}
	}

	private class BtnOkSeleccionIdiomaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			if (rdbtnEnglish.isSelected()){
				MessagesIndex.setIdioma("inglés"); 
				MessagesLogin.setIdioma("inglés");
				MessagesSeleccionIdioma.setIdioma("inglés");
				MessagesLeyendaEditorGrafico.setIdioma("inglés");
				}
			    else{
			    	MessagesIndex.setIdioma("Español"); 
					MessagesLogin.setIdioma("Español");
					MessagesSeleccionIdioma.setIdioma("Español");
					MessagesLeyendaEditorGrafico.setIdioma("Español");
				}
				try {
					Index index = new Index();
					index.getFrameIndex().setVisible(true);
					frame.dispose();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		}
	}
}
