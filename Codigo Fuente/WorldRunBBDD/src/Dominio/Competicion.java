package Dominio;

import java.util.ArrayList;

public class Competicion {

	protected String id;
	protected String nombre;
	protected String localidad;
	protected String fecha;
	protected String modalidad;
	protected String dificultad;
	protected String descripcion;
	protected String fechaLimite;
	protected double precio;
	protected double distancia;
	protected String entidad;
	protected int limite;
	protected String cartel;
	protected String informacionGeneral;
	protected ArrayList <String> entidadesColaboradoras;
	protected ArrayList <String> enlacesInteres;
	protected int telefono;
	protected String web;
	protected String correoElectronico;
	protected String mapa;
	private ArrayList<Participante> participante;
	
	public Competicion(String id, String nombre, String localidad, String fecha,
			String modalidad, String dificultad, String descripcion,
			String fechaLimite, double precio, double distancia,
			String entidad, int limite, String cartel,
			String informacionGeneral,
			ArrayList<String> entidadesColaboradoras,
			ArrayList<String> enlacesInteres, int telefono, String web,
			String correoElectronico, String mapa,
			ArrayList<Participante> participante) {
		this.id = id;
		this.nombre = nombre;
		this.localidad = localidad;
		this.fecha = fecha;
		this.modalidad = modalidad;
		this.dificultad = dificultad;
		this.descripcion = descripcion;
		this.fechaLimite = fechaLimite;
		this.precio = precio;
		this.distancia = distancia;
		this.entidad = entidad;
		this.limite = limite;
		this.cartel = cartel;
		this.informacionGeneral = informacionGeneral;
		this.entidadesColaboradoras = entidadesColaboradoras;
		this.enlacesInteres = enlacesInteres;
		this.telefono = telefono;
		this.web = web;
		this.correoElectronico = correoElectronico;
		this.mapa = mapa;
		this.participante = participante;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getModalidad() {
		return modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	public String getDificultad() {
		return dificultad;
	}

	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFechaLimite() {
		return fechaLimite;
	}

	public void setFechaLimite(String fechaLimite) {
		this.fechaLimite = fechaLimite;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public int getLimite() {
		return limite;
	}

	public void setLimite(int limite) {
		this.limite = limite;
	}

	public String getCartel() {
		return cartel;
	}

	public void setCartel(String cartel) {
		this.cartel = cartel;
	}

	public String getInformacionGeneral() {
		return informacionGeneral;
	}

	public void setInformacionGeneral(String informacionGeneral) {
		this.informacionGeneral = informacionGeneral;
	}

	public ArrayList<String> getEntidadesColaboradoras() {
		return entidadesColaboradoras;
	}

	public void setEntidadesColaboradoras(ArrayList<String> entidadesColaboradoras) {
		this.entidadesColaboradoras = entidadesColaboradoras;
	}

	public ArrayList<String> getEnlacesInteres() {
		return enlacesInteres;
	}

	public void setEnlacesInteres(ArrayList<String> enlacesInteres) {
		this.enlacesInteres = enlacesInteres;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getMapa() {
		return mapa;
	}

	public void setMapa(String mapa) {
		this.mapa = mapa;
	}

	public ArrayList<Participante> getParticipante() {
		return participante;
	}

	public void setParticipante(ArrayList<Participante> participante) {
		this.participante = participante;
	}

	
	
	

}
