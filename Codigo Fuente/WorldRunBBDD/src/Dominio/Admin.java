package Dominio;

import java.util.Date;
import java.util.LinkedList;

public class Admin extends Persona {
	
	private String usuario;
	private String password;
	private Date fechaUltimoAcceso;
	
	
	
	public Admin(String nombre, String apellidos, String dni,
			String fechaNacimiento, String sexo, String tipoVia,
			String direccion, int nCalle, String escalera, int piso,
			String puerta, int codigoPostal, String provincia,
			String localidad, String pais, int telefono,
			String correoElectronico, String foto, String usuario,
			String password, Date fechaUltimoAcceso) {
		super(nombre, apellidos, dni, fechaNacimiento, sexo, tipoVia,
				direccion, nCalle, escalera, piso, puerta, codigoPostal,
				provincia, localidad, pais, telefono, correoElectronico, foto);
		this.usuario = usuario;
		this.password = password;
		this.fechaUltimoAcceso = fechaUltimoAcceso;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getFechaUltimoAcceso() {
		return fechaUltimoAcceso;
	}

	public void setFechaUltimoAcceso(Date fechaUltimoAcceso) {
		this.fechaUltimoAcceso = fechaUltimoAcceso;
	}
	
}
