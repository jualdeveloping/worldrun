package Dominio;

import java.util.ArrayList;

public class Participante extends Persona{
	
	protected String talla;
	protected String tipo;
	protected String sponsor;
	protected String clubDeportivo;
	private ArrayList<Competicion> competicion;
	
	public Participante(String nombre, String apellidos, String dni,
			String fechaNacimiento, String sexo, String tipoVia,
			String direccion, int nCalle, String escalera, int piso,
			String puerta, int codigoPostal, String provincia,
			String localidad, String pais, int telefono,
			String correoElectronico, String foto, String talla, String tipo,
			String sponsor, String clubDeportivo,
			ArrayList<Competicion> competicion) {
		super(nombre, apellidos, dni, fechaNacimiento, sexo, tipoVia,
				direccion, nCalle, escalera, piso, puerta, codigoPostal,
				provincia, localidad, pais, telefono, correoElectronico, foto);
		this.talla = talla;
		this.tipo = tipo;
		this.sponsor = sponsor;
		this.clubDeportivo = clubDeportivo;
		this.competicion = competicion;
	}

	public String getTalla() {
		return talla;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getClubDeportivo() {
		return clubDeportivo;
	}

	public void setClubDeportivo(String clubDeportivo) {
		this.clubDeportivo = clubDeportivo;
	}

	public ArrayList<Competicion> getCompeticion() {
		return competicion;
	}

	public void setCompeticion(ArrayList<Competicion> competicion) {
		this.competicion = competicion;
	}
	

}
